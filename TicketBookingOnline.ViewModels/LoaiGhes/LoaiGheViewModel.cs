﻿using System.ComponentModel;

namespace TicketBookingOnline.ViewModels.LoaiGhes
{
    public class LoaiGheViewModel
    {
        public int LoaiGheId { get; set; }

        [DisplayName("Loại ghế")]
        public string  TenLoaiGhe { get; set; }
    }
}

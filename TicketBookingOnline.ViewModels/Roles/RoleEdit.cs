﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using TicketBookingOnline.Core.Models;
namespace TicketBookingOnline.ViewModels.Roles
{
    public class RoleEdit
    {
        public IdentityRole Role { get; set; }
        public IEnumerable<CGVManager> Members { get; set; }
        public IEnumerable<CGVManager> NonMembers { get; set; }
    }
}

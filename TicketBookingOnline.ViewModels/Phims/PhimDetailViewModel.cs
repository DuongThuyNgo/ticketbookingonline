﻿using System;
using System.ComponentModel;
using TicketBookingOnline.ViewModels.CanhBaos;

namespace TicketBookingOnline.ViewModels.Phims
{
    public class PhimDetailViewModel
    {
        public int PhimId { get; set; }

        [DisplayName("Tên phim")]
        public string TenPhim { get; set; }

        [DisplayName("Thời lượng")]
        public int ThoiLuong { get; set; }

        [DisplayName("Ảnh")]
        public string Anh { get; set; }

        [DisplayName("Banner")]
        public string Banner { get; set; }

        [DisplayName("Trailer")]
        public string Trailer { get; set; }

        [DisplayName("Mô tả")]
        public string MoTa { get; set; }

        [DisplayName("Khởi chiếu")]
        public DateTime KhoiChieu { get; set; }

        [DisplayName("Ngôn ngữ")]
        public string NgonNgu { get; set; }

        [DisplayName("Đạo diễn")]
        public string DaoDien { get; set; }

        [DisplayName("Diễn viên")]
        public string DienVien { get; set; }

        [DisplayName("Trạng thái")]
        public string TrangThai { get; set; }

        [DisplayName("Total Rate")]
        public float TotalRate { get; set; }

        [DisplayName("Giá vé")]
        public decimal GiaVe { get; set; }

        [DisplayName("Khuyến mãi")]
        public float KhuyenMai { get; set; }

        [DisplayName("Thể loại")]
        public string TheLoais { get; set; }

        [DisplayName("Cảnh báo")]
        public CanhBaoViewModel CanhBao {get;set;}
    }
}

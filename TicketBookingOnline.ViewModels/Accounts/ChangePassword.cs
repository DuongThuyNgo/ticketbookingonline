﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace TicketBookingOnline.ViewModels.Accounts
{
    public class ChangePassword
    {
        [Required(ErrorMessage = "Mật khẩu hiện tại không được để trống")]
        [DisplayName("Mật khẩu hiện tại")]
        public string CurrentPassword { get; set; }

        [DisplayName("Mật khẩu mới")]
        [Required(ErrorMessage = "Mật khẩu mới không được để trống")]
        public string NewPassword { get; set; }

        [DisplayName("Xác nhận mật khẩu mới")]
        [Required(ErrorMessage = "Xác nhận mật khẩu mới không được đê trống")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string Email { get; set; }

    }
}

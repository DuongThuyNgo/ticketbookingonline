﻿using System.ComponentModel;
namespace TicketBookingOnline.ViewModels.KhachHangs
{
    public class KhachHangViewModel
    {
        public int UserId { get; set; }

        [DisplayName("Họ tên")]
        public string HoTen { get; set; }

        [DisplayName("Địa chỉ")]
        public string DiaChi { get; set; }

        [DisplayName("Số điện thoại")]
        public string SoDienThoai { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Điểm thưởng")]
        public int DiemThuong { get; set; }       
    }
}

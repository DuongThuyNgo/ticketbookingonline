﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace TicketBookingOnline.ViewModels.KhachHangs
{
    public class KhachHangLoginViewModel
    {
        [DisplayName("Email")]
        [Required(ErrorMessage = "Email không được để trống")]
        public string Email { get; set; }

        [DisplayName("Mật khẩu")]
        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        public string Password { get; set; }

    }
}

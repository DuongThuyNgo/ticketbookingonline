﻿using System;
using TicketBookingOnline.ViewModels.CumRaps;

namespace TicketBookingOnline.ViewModels.ThongKes
{
    public class DoanhThuTheoNgay
    {
        public int? CumRapId { get; set; }

        public DateTime Ngay { get; set; }

        public decimal DoanhThu {get; set;}

        public  CumRapViewModel CumRap { get; set; }
    }
}

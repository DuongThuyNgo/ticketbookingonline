﻿using TicketBookingOnline.ViewModels.CumRaps;

namespace TicketBookingOnline.ViewModels.ThongKes
{
    public class DoanhThuTheoNam
    {
        public int ? CumRapId { get; set; }

        public int Nam { get; set; }

        public decimal DoanhThu { get; set; }

        public CumRapViewModel CumRap { get; set; }
    }
}

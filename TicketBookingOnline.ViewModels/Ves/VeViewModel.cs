﻿using System;
using System.ComponentModel;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.LichChieus;
using TicketBookingOnline.ViewModels.RapGhes;


namespace TicketBookingOnline.ViewModels.Ves
{
    public class VeViewModel
    {
        public int VeId { get; set; }

        public int RapId { get; set; }

        public int LoaiGheId { get; set; }

        public int LichChieuId { get; set; }

        [DisplayName("Số lượng ghế")]
        public int SoLuongGheNgoi { get; set; }

        [DisplayName("Vị trí")]
        public string ViTri { get; set; }

        [DisplayName("Ngày đặt vé")]
        public DateTime NgayDatVe { get; set; }

        [DisplayName("Tổng giá vé")]
        public decimal TongGiaVe { get; set; }

        [DisplayName("Trạng thái")]
        public string TrangThai { get; set; }

        public int UserId { get; set; }

        public RapGheViewModel RapGhe { get; set; }

        public LichChieuViewModel LichChieu { get; set; }

        public KhachHangViewModel User { get; set; }
    }
}

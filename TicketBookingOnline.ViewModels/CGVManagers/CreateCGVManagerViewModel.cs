﻿using System.ComponentModel.DataAnnotations;


namespace TicketBookingOnline.ViewModels.CGVManagers
{
    public class CreateCGVManagerViewModel
    {
        [Display(Name = "Tên tài khoản")]
        [Required(ErrorMessage = "Tên tài khoản không được để trống")]
        public string UserName { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email không được để trống")]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        public string Password { get; set; }

        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Số điện thoại không được để trống")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Loại tài khoản")]
        [Required(ErrorMessage = "Loại tài khoản không được để trống")]
        public string LoaiTaiKhoan { get; set; }

        [Display(Name = "Cum Rap")]
        [Required(ErrorMessage = "Cụm rạp không được để trống")]
        public int CumRapId { get; set; }

        public string RoleName { get; set; }
    }
}

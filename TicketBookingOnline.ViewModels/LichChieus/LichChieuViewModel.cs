﻿using System;
using System.ComponentModel;
using TicketBookingOnline.ViewModels.Phims;
using TicketBookingOnline.ViewModels.Raps;

namespace TicketBookingOnline.ViewModels.LichChieus
{
    public class LichChieuViewModel
    {
        public int LichChieuId { get; set; }

        public int PhimId { get; set; }

        public int RapId { get; set; }

        [DisplayName("Xuất chiếu")]
        public DateTime XuatChieu { get; set; }

        [DisplayName("Khuyến mãi (%)")]
        public int PhanTramKM { get; set; }

        public RapViewModel Rap {get; set;}

        public PhimViewModel Phim { get; set; }

    }
}

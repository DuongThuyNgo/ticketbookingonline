﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace TicketBookingOnline.ViewModels.Sliders
{
    public class SliderViewModel
    {
        public int SlideId { get; set; }

        [DisplayName("Tên")]
        [Required(ErrorMessage= "Tên không được để trống")]
        public string Ten { get; set; }

        [DisplayName("Ảnh")]
        public string Anh { get; set; }
    }
}

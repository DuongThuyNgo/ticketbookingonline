﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketBookingOnline.ViewModels.PhimTheLoais
{
    public class PhimTheLoaiViewModel
    {
        public int PhimId { get; set; }

        public int TheLoaiId { get; set; }
    }
}

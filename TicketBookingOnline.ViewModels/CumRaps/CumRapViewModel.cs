﻿using System.ComponentModel;
using TicketBookingOnline.ViewModels.ThanhPhos;

namespace TicketBookingOnline.ViewModels.CumRaps
{
    public class CumRapViewModel
    {
        public int CumRapId { get; set; }

        public int ThanhPhoId { get; set; }

        [DisplayName("Tên rạp")]
        public string TenRap { get; set; }

        [DisplayName("Địa chỉ")]
        public string DiaChi { get; set; }

        [DisplayName("Số điện thoại")]
        public string SoDienThoai { get; set; }

        [DisplayName("Thành phố")]
        public ThanhPhoViewModel ThanhPho { get; set; }
    }
}

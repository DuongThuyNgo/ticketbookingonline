﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.ThanhPhos;

namespace TicketBookingOnline.Services.Services
{
    public class ThanhPhoServices : IThanhPhoServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ThanhPhoServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<ThanhPhoViewModel> GetAll()
        {
            try
            {
                var thanhPhos = _unitOfWork.ThanhPhoRepository.GetAll();
                return _mapper.Map<IEnumerable<ThanhPhoViewModel>>(thanhPhos);

            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

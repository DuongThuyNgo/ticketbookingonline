﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Phims;
using TicketBookingOnline.ViewModels.PhimTheLoais;

namespace TicketBookingOnline.Services.Services
{
    public class PhimServices:IPhimServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PhimServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<PhimViewModel> GetPhimByTrangThai(string trangThai)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhimByTrangThai(trangThai);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<PhimViewModel> GetPhimByTheLoai(int maTheLoai)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhimByTheLoai(maTheLoai);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public IEnumerable<PhimViewModel> GetPhimByTheLoais(List<int> maTheLoai)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhimByTheLoais(maTheLoai);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }



        public IEnumerable<PhimViewModel> Filter(int? thanhPhoId, int? cumRapId, DateTime? ngayChieu)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.Filter(thanhPhoId, cumRapId, ngayChieu);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public PhimDetailViewModel GetPhim(int phimId)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhim(phimId);
                var phimVM = _mapper.Map<PhimDetailViewModel>(phims);
                phimVM.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(phimVM.PhimId);
                return phimVM;

            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public ResponseResult<CreatePhimViewModel> Create(CreatePhimViewModel phimVM)
        {
            try
            {
                var phim = _mapper.Map<Phim>(phimVM);
                _unitOfWork.PhimRepository.Create(phim);
                _unitOfWork.SaveChanges();

                var latestPhim = _unitOfWork.PhimRepository.GetLastestPhim();

                var phimTheLoaiVMs = new List<PhimTheLoaiViewModel>();
                string[] phimTheLoaiIds = phimVM.TheLoais.Split(";");
                for (int i = 0; i < phimTheLoaiIds.Length - 1; i++)
                {
                    var item = new PhimTheLoaiViewModel();
                    item.TheLoaiId = int.Parse(phimTheLoaiIds[i]);
                    item.PhimId = latestPhim.PhimId;
                    phimTheLoaiVMs.Add(item);
                }
                var phimTheLoais = _mapper.Map<IEnumerable<PhimTheLoai>>(phimTheLoaiVMs);
                
                _unitOfWork.PhimTheLoaiRepository.CreateRange(phimTheLoais);
                _unitOfWork.SaveChanges();

                return new ResponseResult<CreatePhimViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreatePhimViewModel>(ex.Message);
            }
        }

        public ResponseResult<CreatePhimViewModel> Update(CreatePhimViewModel phimVM)
        {
            try
            {
                var oldPhimTheLoais = _unitOfWork.PhimTheLoaiRepository.GetPhimTheLoaiByPhimId(phimVM.PhimId).ToList();
                _unitOfWork.PhimTheLoaiRepository.DeleteRange(oldPhimTheLoais);
                _unitOfWork.SaveChanges();
                var phim = _mapper.Map<Phim>(phimVM);
                phim.IsActive = true;
                _unitOfWork.PhimRepository.Update(phim);
                _unitOfWork.SaveChanges();
                var phimTheLoaiVMs = new List<PhimTheLoaiViewModel>();
                string[] phimTheLoais = phimVM.TheLoais.Split(";");
                var phimTheLoaiIds = new int[phimTheLoais.Length-1];
                for (int i = 0; i < phimTheLoais.Length -1; i++)
                {
                    var item = new PhimTheLoaiViewModel();
                    item.TheLoaiId = int.Parse(phimTheLoais[i]);
                    item.PhimId = phimVM.PhimId;
                    phimTheLoaiVMs.Add(item);
                }

                _unitOfWork.PhimTheLoaiRepository.CreateRange(_mapper.Map<List<PhimTheLoai>>(phimTheLoaiVMs));
                _unitOfWork.SaveChanges();
                return new ResponseResult<CreatePhimViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreatePhimViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int phimId)
        {
            try
            {
                _unitOfWork.PhimRepository.DeletePhim(phimId);
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public IEnumerable<PhimViewModel> GetAll()
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetAll();
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

        }

        public PhimDetailViewModel GetPhimByAnh(string anh)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhimByAnh(anh);
                var phimVM= _mapper.Map<PhimDetailViewModel>(phims);
                phimVM.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(phimVM.PhimId);
                return phimVM;

            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public PhimDetailViewModel GetLastestPhim()
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetLastestPhim();
                var phimVMs = _mapper.Map<PhimDetailViewModel>(phims);
                phimVMs.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(phimVMs.PhimId);               
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public CreatePhimViewModel GetPhimUpdate(int phimId)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhim(phimId);
                var phimVM = _mapper.Map<CreatePhimViewModel>(phims);
                phimVM.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(phimVM.PhimId);
                return phimVM;

            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<PhimViewModel> GetPhimByTenPhim(string tenPhim)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.Find(x => x.TenPhim.Contains(tenPhim));
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetTheLoaiOfPhim(int PhimId)
        {
            try
            {
                var theloai= _unitOfWork.PhimRepository.TheLoaiOfPhim(PhimId);
                return theloai;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<PhimViewModel> GetAllPhimIsActive()
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetAllPhimIsActive();
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public ResponseResult<int> UpdateTrangThaiPhim(int phimId, string trangThai)
        {
            try
            {
                var phimUpdate = _unitOfWork.PhimRepository.GetById(phimId);
                phimUpdate.TrangThai = trangThai;
                _unitOfWork.PhimRepository.Update(phimUpdate);
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public IEnumerable<PhimViewModel> GetAllPhimIsActiveWithRowPage(int index)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetAllPhimIsActiveWithRowperPage(index);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<PhimViewModel> GetPhimByTrangThaiWithRowPage(string trangThai, int index)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhimByTrangThaiWithRowperPage(trangThai, index);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<PhimViewModel> GetPhimByTenPhimWithRowPage(string tenPhim, int index)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.SearchPhimWithRowperPage(tenPhim, index);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<PhimViewModel> GetPhimByTheLoaiRowPerPage(int maTheLoai, int index)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhimByTheLoaiRowPerPage(maTheLoai, index);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);


                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CountPhimByTheLoai(int maTheLoai)
        {
            try
            {
                return _unitOfWork.PhimRepository.CountPhimByTheLoai(maTheLoai);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<PhimViewModel> PhimHot()
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.PhimHot();
                return _mapper.Map<IEnumerable<PhimViewModel>>(phims);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<PhimViewModel> PhimCungTheLoai(int phimId)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.PhimCungTheLoai(phimId);
                return _mapper.Map<IEnumerable<PhimViewModel>>(phims);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<PhimViewModel> GetPhimByTheLoaisWithRowPerPage(List<int> maTheLoai, int index, int rowPerPage)
        {
            try
            {
                var phims = _unitOfWork.PhimRepository.GetPhimByTheLoaisWithRowPerPage(maTheLoai, index, rowPerPage);
                var phimVMs = _mapper.Map<IEnumerable<PhimViewModel>>(phims);
                foreach (var item in phimVMs)
                {
                    item.TheLoais = _unitOfWork.PhimRepository.TheLoaiOfPhim(item.PhimId);
                }
                return phimVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public int CountPhimByTheLoais(List<int> maTheLoai)
        {
            try
            {
                return _unitOfWork.PhimRepository.CountPhimByTheLoais(maTheLoai);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.LichChieus;
using TicketBookingOnline.ViewModels.Raps;

namespace TicketBookingOnline.Services.Services
{
    public class LichChieuServices : ILichChieuServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LichChieuServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<CreateLichChieuViewModel> Create(CreateLichChieuViewModel lichChieuVM)
        {
            try
            {
                if (_unitOfWork.LichChieuRepository.IsLichChieuExists(lichChieuVM.PhimId, lichChieuVM.XuatChieu, lichChieuVM.RapId))
                    return new ResponseResult<CreateLichChieuViewModel>("Lich Chieu is exists !");
                var lichChieu = _mapper.Map<LichChieu>(lichChieuVM);
                _unitOfWork.LichChieuRepository.Create(lichChieu);
                var phim = _unitOfWork.PhimRepository.GetById(lichChieu.PhimId);
                if(phim.SoLuongLichChieu == 0)
                {
                    phim.TrangThai = "DangChieu";
                }
                phim.SoLuongLichChieu = phim.SoLuongLichChieu + 1;
                _unitOfWork.PhimRepository.Update(phim);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CreateLichChieuViewModel>();
            }
            catch(Exception ex)
            {
                return new ResponseResult<CreateLichChieuViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int lichChieuId)
        {
            try
            {
                _unitOfWork.LichChieuRepository.Delete(lichChieuId);
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public IEnumerable<LichChieuViewModel> Filter(int phimId, int? cumRapId, int? rapId, DateTime? ngayChieu)
        {
            try
            {
                var lichChieus = _unitOfWork.LichChieuRepository.Filter(phimId, cumRapId, rapId, ngayChieu);
                var lichChieuVMs = _mapper.Map<IEnumerable<LichChieuViewModel>>(lichChieus);
                foreach (var item in lichChieuVMs)
                {
                    var rap = _unitOfWork.RapRepository.GetById(item.RapId);
                    item.Rap = _mapper.Map<RapViewModel>(rap);
                }
                return lichChieuVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<LichChieuViewModel> GetLichChieuByPhimId(int phimId)
        {
            try
            {
                var lichChieus = _unitOfWork.LichChieuRepository.GetLichChieuByPhimId(phimId);
                var lichChieuVMs = _mapper.Map<IEnumerable<LichChieuViewModel>>(lichChieus);
                foreach (var item in lichChieuVMs)
                {
                    var rap = _unitOfWork.RapRepository.GetById(item.RapId);
                    item.Rap = _mapper.Map<RapViewModel>(rap);
                }
                return lichChieuVMs;
            }catch(Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<string> GetLichChieuInCumRap(int cumRapId)
        {
            try
            {
                var lichChieus = _unitOfWork.LichChieuRepository.GetLichChieuInCumRap(cumRapId);
                var lichChieuVMs = _mapper.Map<IEnumerable<string>>(lichChieus);

                return lichChieuVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<LichChieuViewModel> GetLichChieuOfPhimInCumRap(int phimId, int cumRapId)
        {
            try
            {
                var lichChieus = _unitOfWork.LichChieuRepository.GetLichChieuOfPhimInCumRap(phimId,cumRapId);
                var lichChieuVMs = _mapper.Map<IEnumerable<LichChieuViewModel>>(lichChieus);
                foreach(var item in lichChieuVMs)
                {
                    var rap = _unitOfWork.RapRepository.GetById(item.RapId);
                    item.Rap = _mapper.Map<RapViewModel>(rap);
                }
                return lichChieuVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }
    }
}

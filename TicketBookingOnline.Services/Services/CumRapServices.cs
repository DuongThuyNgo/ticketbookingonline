﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.CumRaps;

namespace TicketBookingOnline.Services.Services
{
    public class CumRapServices : ICumRapServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CumRapServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public int CountAll(string tenCumRap, int? thanhPhoId)
        {
            try
            {
                var cumRaps = _unitOfWork.CumRapRepository.GetAllDetails().OrderBy(x => x.ThanhPho.TenThanhPho).AsQueryable();
                if (tenCumRap != null)
                {
                    cumRaps = cumRaps.Where(x => x.TenRap.ToLower().Contains(tenCumRap.ToLower()));
                }
                if (thanhPhoId != null && thanhPhoId != 0)
                {
                    cumRaps = cumRaps.Where(x => x.ThanhPhoId == thanhPhoId);
                }
                return cumRaps.Count();
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public ResponseResult<CreateCumRapViewModel> Create(CreateCumRapViewModel cumRapVM)
        {
            try
            {
                var cumRap = _mapper.Map<CumRap>(cumRapVM);
                _unitOfWork.CumRapRepository.Create(cumRap);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CreateCumRapViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateCumRapViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int cumRapId)
        {
            try
            {
                _unitOfWork.CumRapRepository.DeleteCumRap(cumRapId);
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public IEnumerable<CumRapViewModel> GetAll(string tenCumRap , int ? thanhPhoId)
        {
            try
            {
                var cumRaps = _unitOfWork.CumRapRepository.GetAllDetails().OrderBy(x=>x.ThanhPho.TenThanhPho).AsQueryable();
                if(tenCumRap != null)
                {
                    cumRaps = cumRaps.Where(x => x.TenRap.ToLower().Contains(tenCumRap.ToLower()));
                }
                if(thanhPhoId != null && thanhPhoId != 0)
                {
                    cumRaps = cumRaps.Where(x => x.ThanhPhoId == thanhPhoId);
                }
                var cumRapVMs = _mapper.Map<IEnumerable<CumRapViewModel>>(cumRaps);
                return cumRapVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public CumRapViewModel GetById(int cumRapId)
        {
            try
            {
                var cumRap = _unitOfWork.CumRapRepository.GetById(cumRapId);
                return _mapper.Map<CumRapViewModel>(cumRap);
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<CumRapViewModel> GetCumRapByThanhPhos(int thanhPhoId)
        {
            try
            {
                var cumRaps = _unitOfWork.CumRapRepository.Find(r => r.ThanhPhoId == thanhPhoId);
                var cumRapVMs = _mapper.Map<IEnumerable<CumRapViewModel>>(cumRaps);

                return cumRapVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public IEnumerable<CumRapViewModel> GetWithRowPerPage(int index, string tenCumRap, int? thanhPhoId)
        {
            try
            {
                var cumRaps = _unitOfWork.CumRapRepository.GetAllDetailWithRowPerPage(index).OrderBy(x => x.ThanhPho.TenThanhPho).AsQueryable();
                if (tenCumRap != null)
                {
                    cumRaps = cumRaps.Where(x => x.TenRap.ToLower().Contains(tenCumRap.ToLower()));
                }
                if (thanhPhoId != null && thanhPhoId != 0)
                {
                    cumRaps = cumRaps.Where(x => x.ThanhPhoId == thanhPhoId);
                }
                var cumRapVMs = _mapper.Map<IEnumerable<CumRapViewModel>>(cumRaps);
                return cumRapVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public ResponseResult<CreateCumRapViewModel> Update(CreateCumRapViewModel cumRapVM)
        {
            try
            {
                var cumRap = _unitOfWork.CumRapRepository.GetById(cumRapVM.CumRapId);
                cumRap.TenRap = cumRapVM.TenRap;
                cumRap.DiaChi = cumRapVM.DiaChi;
                cumRap.SoDienThoai = cumRapVM.SoDienThoai;
                _unitOfWork.CumRapRepository.Update(cumRap);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CreateCumRapViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateCumRapViewModel>(ex.Message);
            }
        }

        IEnumerable<CumRapViewModel> ICumRapServices.GetCumRapByThanhPhos(int thanhPhoId)
        {
            throw new NotImplementedException();
        }
    }
}

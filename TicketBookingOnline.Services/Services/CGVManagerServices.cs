﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Helper;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.CGVManagers;

namespace TicketBookingOnline.Services.Services
{
    public class CGVManagerServices : ICGVManagerServices
    {
        private readonly UserManager<CGVManager> _userManager;
        private readonly SignInManager<CGVManager> _signInManager;
        private readonly IMapper _mapper;
        private readonly IPasswordHasher<CGVManager> _passwordHasher;
        private readonly IPasswordValidator<CGVManager> _passwordValid;
        private readonly IUserValidator<CGVManager> _userValid;
        private readonly IUnitOfWork _unitOfWork;

        public CGVManagerServices(UserManager<CGVManager> userManager, SignInManager<CGVManager> signInManager, IMapper mapper
            , IPasswordHasher<CGVManager> passwordHash, IPasswordValidator<CGVManager> passwordVal, IUserValidator<CGVManager> userValid
            , IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _passwordHasher = passwordHash;
            _passwordValid = passwordVal;
            _userValid = userValid;
            _unitOfWork = unitOfWork;
        }

        public async Task<ResponseResult<CGVManagerViewModel>> CreateAsync(CreateCGVManagerViewModel request)
        {
            try
            {   
                var user = new CGVManager()
                {
                    UserName = request.UserName,
                    Email = request.Email,           
                    PhoneNumber=request.PhoneNumber,
                    CumRapId = request.CumRapId,
                    IsActive = true,
                };
                var isExistEmail = _userManager.Users.Where(p=>p.Email.Equals(request.Email) && p.IsActive == true).FirstOrDefault();
                if(isExistEmail!=null)
                    return new ResponseResult<CGVManagerViewModel>("Email is exist");
                if(!request.PhoneNumber.PhoneNumberValidate())
                    return new ResponseResult<CGVManagerViewModel>("Phone number must have 10 digits");
                var result1 = await _userManager.CreateAsync(user, request.Password);
                if(!result1.Succeeded)
                    return new ResponseResult<CGVManagerViewModel>(result1.Errors.Select(x => x.Description).ToArray());
                var result2 = await _userManager.AddToRoleAsync(user, request.RoleName);
                if (result2.Succeeded)
                    return new ResponseResult<CGVManagerViewModel>();
                return new ResponseResult<CGVManagerViewModel>(result2.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<CGVManagerViewModel>(ex.Message);
            }
        }

        public async Task<ResponseResult<CGVManagerViewModel>> DeleteAsync(string id)
        {
            try
            {
                var user = _userManager.Users.FirstOrDefault(x => x.Id == id);
                _unitOfWork.CGVManagerRepository.Delete(user);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CGVManagerViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CGVManagerViewModel>(ex.Message);
            }
        }

        public CGVManager Find(string id)
        { 
            return _userManager.Users.FirstOrDefault(x => x.Id == id);
        }

        public CGVManagerDetailViewModel FindCGVManager(string id)
        {
            var cGVManager = _mapper.Map<CGVManagerDetailViewModel>(Find(id));
            if(cGVManager.CumRapId != 0)
            {
                var cumRap = _unitOfWork.CumRapRepository.GetById(cGVManager.CumRapId);
                cGVManager.TenCumRap = cumRap.TenRap;
                cGVManager.DiaChi = cumRap.DiaChi;
            }
            else
            {
                cGVManager.TenCumRap = "CGV Administator";
                cGVManager.DiaChi = "CGV Headquater";
            }
            return cGVManager;
        }

        public IEnumerable<CGVManagerViewModel> GetAll()
        {
            var user = _userManager.Users.Where(x=>x.IsActive == true);
            var cGVManagers = _mapper.Map<IEnumerable<CGVManagerViewModel>>(user);
            foreach (var item in cGVManagers)
            {
                if(item.CumRapId != 0)
                {
                    var cumRap = _unitOfWork.CumRapRepository.GetById(item.CumRapId);
                    item.TenCumRap = cumRap.TenRap;
                    item.DiaChi = cumRap.DiaChi;
                }
                else
                {
                    item.TenCumRap = "CGV Administator";
                    item.DiaChi = "CGV Headquater";
                }
            }
            return cGVManagers;
        }

        public async Task<ResponseResult<CGVManagerViewModel>> RoleAssignAsync(string email, params string[] roleNames)
        {
            try
            {
                var userExisting = await _userManager.FindByEmailAsync(email);
                if (userExisting == null)
                    return new ResponseResult<CGVManagerViewModel>($"{email} khong ton tai");

                foreach (var role in roleNames)
                {
                    if (!await _userManager.IsInRoleAsync(userExisting, role))
                    {
                        var result = await _userManager.AddToRoleAsync(userExisting, role);
                        if (!result.Succeeded)
                            return new ResponseResult<CGVManagerViewModel>(result.Errors.Select(x => x.Description).ToArray());
                    }
                }
                return new ResponseResult<CGVManagerViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CGVManagerViewModel>(ex.Message);
            }
        }

        public async Task<ResponseResult<CGVManagerViewModel>> SignInAsync(LoginCGVManagerViewModel request)
        {
            try
            {
                var userExisting = await _userManager.FindByNameAsync(request.UserName);
                if (userExisting == null)
                    return new ResponseResult<CGVManagerViewModel>($"User: {request.UserName} khong ton tai");
                var result = await _signInManager.PasswordSignInAsync(userExisting, request.Password, request.RememberMe, false);
                if (result.Succeeded)
                    return new ResponseResult<CGVManagerViewModel>();
                if (result.IsLockedOut)
                    return new ResponseResult<CGVManagerViewModel>("Tai khoan cua ban da bi khoa");
                return new ResponseResult<CGVManagerViewModel>("Dang nhap khong thanh cong");
            }
            catch (Exception ex)
            {
                return new ResponseResult<CGVManagerViewModel>(ex.Message);
            }
        }

        public async Task<ResponseResult<CGVManagerViewModel>> UpdateAsync(CGVManagerDetailViewModel request)
        {
            try
            {
                CGVManager user = await _userManager.FindByIdAsync(request.Id);
                user.UserName = request.UserName;
                user.PhoneNumber = request.PhoneNumber;
                user.CumRapId = request.CumRapId;
                IdentityResult validEmail = null;
                if (!string.IsNullOrEmpty(request.Email))
                {
                    validEmail = await _userValid.ValidateAsync(_userManager, user);
                    if (validEmail.Succeeded)
                        user.Email = request.Email;
                    else
                        return new ResponseResult<CGVManagerViewModel>("Email is in valid");
                }
                IdentityResult result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                    return new ResponseResult<CGVManagerViewModel>();
                else
                    return new ResponseResult<CGVManagerViewModel>(result.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<CGVManagerViewModel>(ex.Message);
            }
        }

        public async Task<ResponseResult<CGVManagerViewModel>> Delete(CGVManagerDetailViewModel request)
        {
            try
            {
                CGVManager user = await _userManager.FindByIdAsync(request.Id);
                //user.IsActive = false;
                //IdentityResult validEmail = null;
                //if (!string.IsNullOrEmpty(request.Email))
                //{
                //    validEmail = await _userValid.ValidateAsync(_userManager, user);
                //    if (validEmail.Succeeded)
                //        user.Email = request.Email;
                //    else
                //        return new ResponseResult<CGVManagerViewModel>("Email is in valid");
                //}
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                    return new ResponseResult<CGVManagerViewModel>();
                else
                    return new ResponseResult<CGVManagerViewModel>(result.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<CGVManagerViewModel>(ex.Message);
            }
        }

        public IEnumerable<CGVManagerViewModel> GetWithRowPerPage(int index, int thanhPhoId, int cumRapId)
        {
            int rowPerPage = 10;
            var users = new List<CGVManager>();
            if (cumRapId != 0)
            {
                users = _userManager.Users.Where(x => x.IsActive == true && x.CumRapId == cumRapId).Skip((index - 1) * rowPerPage).Take(rowPerPage).ToList();
            }
            else
            {
                users = _userManager.Users.Where(x => x.IsActive == true).Skip((index - 1) * rowPerPage).Take(rowPerPage).ToList();
            }
            
            var cGVManagers = _mapper.Map<IEnumerable<CGVManagerViewModel>>(users);
            foreach (var item in cGVManagers)
            {
                if (item.CumRapId != 0)
                {
                    var cumRap = _unitOfWork.CumRapRepository.GetById(item.CumRapId);
                    item.TenCumRap = cumRap.TenRap;
                    item.DiaChi = cumRap.DiaChi;
                }
                else
                {
                    item.TenCumRap = "CGV Administators";
                    item.DiaChi = "CGV Headquater";
                }
            }
            return cGVManagers;
        }

        public int CountAll()
        {
            var users = _userManager.Users.Where(x => x.IsActive == true);
            return users.Count();
        }

        public IEnumerable<CGVManagerViewModel> GetNhanVienInCumRapWithRowPerPage(int cumRapId, int index)
        {
            var user = _unitOfWork.CGVManagerRepository.GetNhanVienInCumRapWithRowPerPage(cumRapId, index); 
            var cGVManagers = _mapper.Map<IEnumerable<CGVManagerViewModel>>(user);
            foreach (var item in cGVManagers)
            {
                if (item.CumRapId != 0)
                {
                    var cumRap = _unitOfWork.CumRapRepository.GetById(item.CumRapId);
                    item.TenCumRap = cumRap.TenRap;
                    item.DiaChi = cumRap.DiaChi;
                }
                else
                {
                    item.TenCumRap = "CGV Administators";
                    item.DiaChi = "CGV Headquater";
                }
            }
            return cGVManagers;
        }

        public int CountAllNhanVienInCumRap(int cumRapId)
        {
            try
            {
                var result = _unitOfWork.CGVManagerRepository.CountAllNhanVienInCumRap(cumRapId);
                return result;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

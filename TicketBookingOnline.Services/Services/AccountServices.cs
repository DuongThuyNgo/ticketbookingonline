﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.CGVManagers;

namespace TicketBookingOnline.Services.Services
{
    public class AccountServices : IAccountServices
    {
        private readonly UserManager<CGVManager> _userManager;
        private readonly SignInManager<CGVManager> _signInManager;
        private readonly IUserValidator<CGVManager> _userValid;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public AccountServices(UserManager<CGVManager> userManager, SignInManager<CGVManager> signInManager, 
            IUserValidator<CGVManager> userValid, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userValid = userValid;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public UpdateAccountViewModel FindAccount(string id)
        {
            return _mapper.Map<UpdateAccountViewModel>(_userManager.Users.FirstOrDefault(p=>p.Id==id));
        }

        public async Task<ResponseResult<ChangePassword>> ResetPassword(ChangePassword request)
        {
            try
            {
                if(!request.NewPassword.Equals(request.ConfirmPassword))
                    return new ResponseResult<ChangePassword>("Confirm password is not correct");
                var user = await _userManager.FindByEmailAsync(request.Email);
                var result = await _signInManager.PasswordSignInAsync(user, request.CurrentPassword, false, false);
                if (!result.Succeeded)
                {
                    return new ResponseResult<ChangePassword>("Current password is not correct");
                }
                var changePassResult = await _userManager.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);
                if (changePassResult.Succeeded)
                    return new ResponseResult<ChangePassword>();
                return new ResponseResult<ChangePassword>(changePassResult.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<ChangePassword>(ex.Message);
            }
        }

        public async Task<LoginViewModel> SignInAsync(LoginViewModel request)
        {
            try
            {
                var userExisting = _userManager.Users.FirstOrDefault(x => x.Email == request.Email.Trim() && x.IsActive == true);
                //var userExisting = await _userManager.FindByEmailAsync(request.Email);
                if (userExisting == null)
                    return null;
                var result = await _signInManager.PasswordSignInAsync(userExisting, request.Password, request.RememberMe, false);
                if (result.Succeeded)
                {
                    var userLogin = _mapper.Map<LoginViewModel>(userExisting);
                    userLogin.RoleName = _unitOfWork.CGVManagerRepository.RoleName(userLogin.Id);
                    if(userLogin.CumRapId == 0)
                    {
                        userLogin.TenRap = "CGV Headquater";
                    }
                    else
                    {
                        var cumRap = _unitOfWork.CumRapRepository.GetById(userLogin.CumRapId);
                        userLogin.TenRap = cumRap.TenRap;
                    }
                    return userLogin;
                }
                if (result.IsLockedOut)
                    return null;
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ResponseResult<string>> SingOutAsync()
        {
            try
            {
                _signInManager.SignOutAsync();
                return new ResponseResult<string>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }

        public async Task<ResponseResult<CGVManagerViewModel>> UpdateAsync(CGVManagerDetailViewModel request)
        {
            try
            {
                CGVManager user = await _userManager.FindByIdAsync(request.Id);
                user.UserName = request.UserName;
                user.PhoneNumber = request.PhoneNumber;
                //IdentityResult validEmail = null;
                //if (!string.IsNullOrEmpty(request.Email))
                //{
                //    validEmail = await _userValid.ValidateAsync(_userManager, user);
                //    if (validEmail.Succeeded)
                //        user.Email = request.Email;
                //    else
                //        return new ResponseResult<CGVManagerViewModel>("Email is invalid");
                //}
                IdentityResult result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                    return new ResponseResult<CGVManagerViewModel>();
                else
                    return new ResponseResult<CGVManagerViewModel>(result.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<CGVManagerViewModel>(ex.Message);
            }
        }
    }
}

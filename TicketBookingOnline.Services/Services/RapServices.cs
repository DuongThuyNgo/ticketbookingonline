﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketBookingOnline.Core;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Raps;

namespace TicketBookingOnline.Services.Services
{
    public class RapServices : IRapServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RapServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<RapViewModel> Create(RapViewModel rapVM)
        {
            try
            {
                var rap = _mapper.Map<Rap>(rapVM);
                _unitOfWork.RapRepository.Create(rap);
                _unitOfWork.SaveChanges();
                return new ResponseResult<RapViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<RapViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int rapId)
        {
            try
            {
                _unitOfWork.RapRepository.Delete(rapId);
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public IEnumerable<RapViewModel> GetRapByCumRapId(int cumRapId)
        {
            try
            {
                var raps =_unitOfWork.RapRepository.GetRapByCumRapId(cumRapId);
                return _mapper.Map<IEnumerable<RapViewModel>>(raps);    
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public RapViewModel GetRapById(int rapId)
        {
            try
            {
                var rap = _unitOfWork.RapRepository.GetById(rapId);
                return _mapper.Map<RapViewModel>(rap);               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseResult<RapViewModel> Update(RapViewModel rapVM)
        {
            try
            {
                var rap = _unitOfWork.RapRepository.GetById(rapVM.RapId);
                if (rap != null)
                {
                    if(rapVM.TenRap != null)
                    {
                        rap.TenRap = rapVM.TenRap;
                    }
                    rap.LoaiRap = rapVM.LoaiRap;
                }
                _unitOfWork.RapRepository.Update(rap);
                _unitOfWork.SaveChanges();
                return new ResponseResult<RapViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<RapViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> UpdateLoaiRap(string loaiRap)
        {
            throw new NotImplementedException();
        }
    }
}

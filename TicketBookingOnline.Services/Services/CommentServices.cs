﻿using AutoMapper;
using System;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Services.IServices;

namespace TicketBookingOnline.Services.Services
{
    public class CommentServices : ICommentServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommentServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool ConfirmComment(int commentId)
        {
            try
            {
                var result = _unitOfWork.CommentRepository.ConfirmComment(commentId);
                _unitOfWork.SaveChanges();
                if (result)
                    return true;
                throw new Exception();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

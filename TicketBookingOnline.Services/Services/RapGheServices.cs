﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.LoaiGhes;
using TicketBookingOnline.ViewModels.RapGhes;

namespace TicketBookingOnline.Services.Services
{
    public class RapGheServices : IRapGheServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RapGheServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<RapGheViewModel> GetRapGheByRap(int rapId)
        {
            try
            {
                var rapGhes = _unitOfWork.RapGheRepository.GetRapGheByRap(rapId);
                var loaiGhes = _unitOfWork.LoaiGheRepository.GetAll();
                //var result = loaiGhes.Join(rapGhes, a => a.LoaiGheId, b => b.LoaiGhe.LoaiGheId,
                //    (a, b) => new RapGheViewModel()
                //    {
                //        RapId = b.RapId,
                //        GiaGhe = b.GiaGhe,
                //        SoLuongGhe = b.SoLuongGhe,
                //        LoaiGhe = new LoaiGheViewModel()
                //        {
                //            LoaiGheId = a.LoaiGheId,
                //            TenLoaiGhe = a.TenLoaiGhe,
                //        }
                //    });


                //var result = loaiGhes.GroupJoin(rapGhes, a => a.LoaiGheId, b => b.LoaiGhe.LoaiGheId,
                //    (ghes, raps) => new
                //    {
                //        ghes, raps
                //    })
                //    .SelectMany(z=>z.raps.DefaultIfEmpty(),
                //                (a, b) => new
                //                {
                //                    RapId = b.RapId,
                //                    GiaGhe = b.GiaGhe,
                //                    SoLuongGhe = b.SoLuongGhe,
                //                    LoaiGhe = new LoaiGheViewModel()
                //                    {
                //                        LoaiGheId = a.LoaiGheId,
                //                        TenLoaiGhe = a.TenLoaiGhe,
                //                    }
                //                });

                var result = from lg in loaiGhes
                             join rg in rapGhes on lg.LoaiGheId equals rg.LoaiGheId into a
                             from b in a.DefaultIfEmpty()
                             select new RapGheViewModel()
                             {

                                 RapId = b?.RapId ?? 0,
                                 GiaGhe = b?.GiaGhe ?? 0,
                                 SoLuongGhe = b?.SoLuongGhe ?? 0,
                                 LoaiGheId = lg.LoaiGheId,
                                 LoaiGhe = new LoaiGheViewModel()

                                 {
                                     LoaiGheId = lg.LoaiGheId,
                                     TenLoaiGhe = lg.TenLoaiGhe,
                                 }
                             };
            //foreach (var item in result)
            //    {
            //        if(item.SoLuongGhe == null)
            //        {
            //            item.RapId = rapId;
            //            item.GiaGhe = 0;
            //            item.SoLuongGhe = 0;
            //        }
            //    }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseResult<RapGheViewModel> Update(RapGheViewModel rapGheVM)
        {
            try
            {
                var rapGhe = _mapper.Map<RapGhe>(rapGheVM);
                _unitOfWork.RapGheRepository.Update(rapGhe);
                return new ResponseResult<RapGheViewModel>();
            }
            catch(Exception ex)
            {
                return new ResponseResult<RapGheViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> UpdateRange(List<RapGheViewModel> rapGheVMs)
        {
            try
            {              
                var rapGhes = _unitOfWork.RapGheRepository.GetRapGheByRap(rapGheVMs[0].RapId).ToList();
                _unitOfWork.RapGheRepository.DeleteRange(rapGhes);
                _unitOfWork.SaveChanges();
                var rapGheUpdates = _mapper.Map<List<UpdateRapGheViewModel>>(rapGheVMs);
                foreach (var item in rapGheUpdates) 
                { 
                    if(item.SoLuongGhe > 0 || item.GiaGhe > 0)
                    {
                        var rapGhe = _mapper.Map<RapGhe>(item);
                        _unitOfWork.RapGheRepository.Create(rapGhe);
                    }
                        
                }
                    //rapGhe = rapGheUpdates;
                // _unitOfWork.RapGheRepository.CreateRange(_mapper.Map<List<RapGhe>>(rapGheUpdates));
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }
    }
}

﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.PhimTheLoais;

namespace TicketBookingOnline.Services.Services
{
    public class PhimTheLoaiServices : IPhimTheLoaiServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PhimTheLoaiServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<int> Create(List<PhimTheLoaiViewModel> phimTheLoaiVM)
        {
            try
            {
                var phimTheLoais = _mapper.Map<List<PhimTheLoai>>(phimTheLoaiVM);
                _unitOfWork.PhimTheLoaiRepository.CreateRange(phimTheLoais);
                //foreach(var item in phimTheLoaiVM)
                //{
                //    _unitOfWork.PhimTheLoaiRepository.Create(_mapper.Map<PhimTheLoai>(item));
                //}               
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }
    }
}

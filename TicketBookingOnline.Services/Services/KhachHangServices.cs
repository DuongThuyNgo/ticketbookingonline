﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.KhachHangs;

namespace TicketBookingOnline.Services.Services
{
    public class KhachHangServices : IKhachHangServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public KhachHangServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool ConfirmKhachHang(int userId)
        {
            try
            {
                var result = _unitOfWork.KhachHangRepository.CofirmKhachHang(userId);
                _unitOfWork.SaveChanges();
                if (result)
                    return true;
                throw new Exception();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int CountKhachHangFilter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email)
        {
            try
            {
                return _unitOfWork.KhachHangRepository.CountKhachHangFilter(cumRapId, ten, soDienThoai, diaChi, email);
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseResult<CreateKhachHangViewModel> Create(CreateKhachHangViewModel khachHangVM)
        {
            try
            {
                var checkEmail = _unitOfWork.KhachHangRepository.EmailIsExist(khachHangVM.Email);
                if(checkEmail)
                    return new ResponseResult<CreateKhachHangViewModel>("Email đã tồn tại !");
                var khachHang = _mapper.Map<KhachHang>(khachHangVM);
                khachHang.Anh = "";
                khachHang.Role = "User";
                _unitOfWork.KhachHangRepository.Create(khachHang);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CreateKhachHangViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateKhachHangViewModel>(ex.Message);
            }
        }

        public IEnumerable<KhachHangViewModel> Filter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email)
        {
            try
            {
                var khachHangs = _unitOfWork.KhachHangRepository.Filter(cumRapId, ten, soDienThoai, diaChi, email);
                var khachHangVMs = _mapper.Map<IEnumerable<KhachHangViewModel>>(khachHangs);
                return khachHangVMs;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<KhachHangViewModel> FilterWithRowPerPage(int? cumRapId, string ten, string soDienThoai, string diaChi, string email, int index)
        {
            try
            {
                var khachHangs = _unitOfWork.KhachHangRepository.FilterWithRowperPage(cumRapId, ten, soDienThoai, diaChi, email, index);
                var khachHangVMs = _mapper.Map<IEnumerable<KhachHangViewModel>>(khachHangs);
                return khachHangVMs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public KhachHangViewModel GetById(int khachHangId)
        {
            try
            {
                var khachHang = _unitOfWork.KhachHangRepository.GetById(khachHangId);
                var khachHangVM = _mapper.Map<KhachHangViewModel>(khachHang);
                return khachHangVM;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public KhachHangViewModel Login(string email, string password)
        {
            try
            {
                var khachHang = _unitOfWork.KhachHangRepository.Login(email, password);
                return _mapper.Map<KhachHangViewModel>(khachHang);           
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseResult<ChangePassword> ResetPassword(ChangePassword request)
        {
            try
            {
                var khachHang = _unitOfWork.KhachHangRepository.Find(x => x.Email.Equals(request.Email)).FirstOrDefault();
                if (khachHang == null)
                    return new ResponseResult<ChangePassword>("Tài khoản không tồn tại");
                if(request.CurrentPassword != khachHang.Password)
                    return new ResponseResult<ChangePassword>("Mật khẩu cũ không chính xác");
                if (request.NewPassword != request.ConfirmPassword)
                    return new ResponseResult<ChangePassword>("Xác nhận mật khẩu không khớp với mật khẩu mới");
                khachHang.Password = request.NewPassword;
                _unitOfWork.KhachHangRepository.Update(khachHang);
                _unitOfWork.SaveChanges();
                return new ResponseResult<ChangePassword>();
            }
            catch(Exception ex){
                return new ResponseResult<ChangePassword>(ex.Message);
            }
        }

        public ResponseResult<KhachHangViewModel> Update(KhachHangViewModel khachHangVM)
        {
            try
            {
                var khachHang = _mapper.Map<KhachHang>(khachHangVM);
                _unitOfWork.KhachHangRepository.Update(khachHang);
                _unitOfWork.SaveChanges();
                return new ResponseResult<KhachHangViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<KhachHangViewModel>(ex.Message);
            }
        }

       
    }
}

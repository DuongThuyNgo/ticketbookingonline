﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Helper;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.Comments;
using TicketBookingOnline.ViewModels.CumRaps;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.ThongKes;

namespace TicketBookingOnline.Services.Services
{
    public class ThongKeServices : IThongKeServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ThongKeServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public DashBoard DashBoard(int ? cumRapId)
        {
            int veMois = 0;
            int khachHangMoi = 0;
            int commentMoi = 0;
            decimal doanhThuNgay = 0;
            decimal doanhThuTuan = 0;
            if (cumRapId == null)
            {
                veMois = _unitOfWork.VeRepository.GetAllIsActive().Count();
            }
            else
            {
                veMois = _unitOfWork.VeRepository.Filter(cumRapId, null, null, null, null).Count();
            }
            khachHangMoi = _unitOfWork.KhachHangRepository.KhachHangMoi(cumRapId).Count();
            var veNgays = _unitOfWork.VeRepository.GetVeByNgay(cumRapId, DateTime.Now);

            var tttt = veNgays.ToList();
            veNgays = veNgays.Where(x => x.TrangThai == "Đã xác nhận");
            doanhThuNgay = veNgays.Sum(x => x.TongGiaVe);
            var veTuans = _unitOfWork.VeRepository.GetVeByTuan(cumRapId, DateTime.Now);
            veTuans = veTuans.Where(x => x.TrangThai == "Đã xác nhận");
            doanhThuTuan = veTuans.Sum(x => x.TongGiaVe);
            commentMoi = _unitOfWork.CommentRepository.CommentMoi().Count();
            var dashBoard = new DashBoard();
            dashBoard.CumRapId = cumRapId;
            dashBoard.SoLuongVeMoi = veMois; 
            dashBoard.SoLuongKhachHangMoi = khachHangMoi;
            dashBoard.SoLuongBinhLuanMoi = commentMoi;
            dashBoard.DoanhThuNgay = doanhThuNgay;
            dashBoard.DoanhThuTuan = doanhThuTuan;
            return dashBoard;
        }

        public DoanhThuTheoNam DoanhThuNam(int? cumRapId, int nam)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.GetVeByNam(cumRapId, nam);
                ves = ves.Where(x => x.TrangThai == "Đã xác nhận");
                DoanhThuTheoNam doanhThu = new DoanhThuTheoNam();
                int id = 0;
                CumRapViewModel cumRapVM = new CumRapViewModel();
                if(cumRapId.HasValue)
                {
                    id = cumRapId.Value;
                    var cumRap = _unitOfWork.CumRapRepository.GetDetailById(id);
                    cumRapVM = _mapper.Map<CumRapViewModel>(cumRap);
                }                
                doanhThu.CumRapId = id;
                doanhThu.CumRap = cumRapVM;
                doanhThu.Nam = nam;
                doanhThu.DoanhThu = ves.Sum(x => x.TongGiaVe);
                return doanhThu;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DoanhThuTheoNgay DoanhThuNgay(int? cumRapId, DateTime ngay)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.GetVeByNgay(cumRapId, ngay);
                ves = ves.Where(x => x.TrangThai == "Đã xác nhận");
                DoanhThuTheoNgay doanhThu = new DoanhThuTheoNgay();
                int id = 0;
                CumRapViewModel cumRapVM = new CumRapViewModel();
                if (cumRapId.HasValue)
                {
                    id = cumRapId.Value;
                    var cumRap = _unitOfWork.CumRapRepository.GetDetailById(id);
                    cumRapVM = _mapper.Map<CumRapViewModel>(cumRap);
                }
                doanhThu.CumRapId = id;
                doanhThu.CumRap = cumRapVM;
                doanhThu.Ngay = ngay;
                doanhThu.DoanhThu = ves.Sum(x => x.TongGiaVe);
                return doanhThu;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DoanhThuTheoThang DoanhThuThang(int? cumRapId, int thang, int nam)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.GetVeByThang(cumRapId, thang, nam);
                ves = ves.Where(x => x.TrangThai == "Đã xác nhận");
                DoanhThuTheoThang doanhThu = new DoanhThuTheoThang();
                int id = 0;
                CumRapViewModel cumRapVM = new CumRapViewModel();
                if (cumRapId.HasValue)
                {
                    id = cumRapId.Value;
                    var cumRap = _unitOfWork.CumRapRepository.GetDetailById(id);
                    cumRapVM = _mapper.Map<CumRapViewModel>(cumRap);
                }
                doanhThu.CumRapId = id;
                doanhThu.CumRap = cumRapVM;
                doanhThu.Thang = thang + "-" + nam;
                doanhThu.DoanhThu = ves.Sum(x => x.TongGiaVe);
                return doanhThu;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DoanhThuTheoTuan DoanhThuTuan(int? cumRapId, DateTime ngay)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.GetVeByTuan(cumRapId, ngay);
                ves = ves.Where(x => x.TrangThai == "Đã xác nhận");
                DoanhThuTheoTuan doanhThu = new DoanhThuTheoTuan();
                int tuan = 1;
                if (ngay.Day > 7 && ngay.Day <= 14)
                    tuan = 2;
                else if (ngay.Day <= 21)
                    tuan = 3;
                else if (ngay.Day <= 28)
                    tuan = 4;
                else
                    tuan = 5;
                doanhThu.CumRapId = cumRapId;
                doanhThu.Tuan = "Tuần " + tuan + "- Tháng " + ngay.Month;
                doanhThu.DoanhThu = ves.Sum(x => x.TongGiaVe);
                return doanhThu;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<DoanhThuTheoNgay> GetAllDoanhThuNgay(DateTime ngay)
        {
            try
            {
                var cumRaps = _unitOfWork.CumRapRepository.GetAll();
                var doanhThus = new List<DoanhThuTheoNgay>();
                foreach (var item in cumRaps)
                {
                    var ves = _unitOfWork.VeRepository.GetVeByNgay(item.CumRapId, ngay);
                    ves = ves.Where(x => x.TrangThai == "Đã xác nhận");
                    DoanhThuTheoNgay doanhThu = new DoanhThuTheoNgay();
                    CumRapViewModel cumRapVM = new CumRapViewModel();                   
                    var cumRap = _unitOfWork.CumRapRepository.GetDetailById(item.CumRapId);
                    cumRapVM = _mapper.Map<CumRapViewModel>(cumRap);                    
                    doanhThu.CumRapId = item.CumRapId;
                    doanhThu.CumRap = cumRapVM;
                    doanhThu.Ngay = ngay;
                    doanhThu.DoanhThu = ves.Sum(x => x.TongGiaVe);
                    doanhThus.Add(doanhThu);
                }
                return doanhThus.OrderBy(x=>x.CumRap.ThanhPhoId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<DoanhThuTheoNgay> GetAllDoanhThuNgayOfThangInCumRap(int cumRapId, int thang, int nam)
        {
            try
            {
                DateTime ngay = new DateTime(nam, thang, 01);
                var result = new List<DoanhThuTheoNgay>();
                while(ngay.Month == thang)
                {
                    var ves = _unitOfWork.VeRepository.GetVeByNgay(cumRapId, ngay);
                    ves = ves.Where(x => x.TrangThai == "Đã xác nhận");
                    DoanhThuTheoNgay doanhThu = new DoanhThuTheoNgay();
                    int id = cumRapId;
                    CumRapViewModel cumRapVM = new CumRapViewModel();
                    var cumRap = _unitOfWork.CumRapRepository.GetDetailById(id);
                    cumRapVM = _mapper.Map<CumRapViewModel>(cumRap);                   
                    doanhThu.Ngay = ngay;
                    doanhThu.DoanhThu = ves.Sum(x => x.TongGiaVe);
                    result.Add(doanhThu);
                    ngay = ngay.AddDays(1);
                }
                
                return result;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<CommentViewModel> GetBinhLuanMoi()
        {
            try
            {
                var comments = _unitOfWork.CommentRepository.CommentMoi();
                return _mapper.Map<IEnumerable<CommentViewModel>>(comments);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<KhachHangViewModel> GetKhachHangMoi(int? cumRapId)
        {
            try
            {
                var khachHangs = _unitOfWork.KhachHangRepository.KhachHangMoi(cumRapId);
                return _mapper.Map<IEnumerable<KhachHangViewModel>> (khachHangs);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<DoanhThuTheoThang> ThongKeDoanhThuNam(int? cumRapId, int nam)
        {
            try
            {
                var doanhThus = new List<DoanhThuTheoThang>();
                for(int i = 1; i <= 12; i++)
                {
                    var ves = _unitOfWork.VeRepository.GetVeByThang(cumRapId, i, nam);
                    ves = ves.Where(x => x.TrangThai == "Đã xác nhận");
                    var doanhThuThang = new DoanhThuTheoThang();
                    doanhThuThang.CumRapId = cumRapId;
                    doanhThuThang.Thang = "Tháng " + i;
                    doanhThuThang.DoanhThu = ves.Sum(x => x.TongGiaVe);
                    doanhThus.Add(doanhThuThang);
                }
                return doanhThus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<DoanhThuTheoTuan> ThongKeDoanhThuThang(int? cumRapId, int thang, int nam)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DoanhThuTheoNgay> ThongKeDoanhThuTuan(int? cumRapId, DateTime ngay)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.GetVeByTuan(cumRapId, ngay).ToList();
                ves = ves.Where(x => x.TrangThai == "Đã xác nhận").ToList();               
                var daysOfWeek = Extension.DaysOfWeek(ngay);
                var doanhThus = new List<DoanhThuTheoNgay>();
                if (ves.Count != 0)
                {
                    doanhThus = ves.GroupBy(x => x.LichChieu.XuatChieu.Date).Select(x => new DoanhThuTheoNgay()
                    {
                        Ngay = x.Key,
                        DoanhThu = x.Sum(x => x.TongGiaVe)
                    }).ToList();
                }
                    var result1 = from day in daysOfWeek
                                 join dt in doanhThus on day.Date equals dt.Ngay into a
                                 from b in a.DefaultIfEmpty()
                                 select new DoanhThuTheoNgay()
                                 {
                                     CumRapId = cumRapId,
                                     Ngay = day.Date,
                                     DoanhThu = b?.DoanhThu ?? 0,
                                 };

                    return result1.OrderBy(x => x.Ngay);
                
                //foreach( var day in daysOfWeek)
                //{
                //    var doanhThu = new DoanhThuTheoNgay()
                //    {
                //        CumRapId = cumRapId,
                //        Ngay = day.Date,
                //        DoanhThu = 0,
                //    };
                //    doanhThus.Add(doanhThu);
                //}
                //return doanhThus.OrderBy(x => x.Ngay);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TrangThaiVe> ThongKeTrangThaiVe(int? cumRapId, DateTime ngay)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.GetVeByTuan(cumRapId, ngay);
                var tttt = ves.ToList();
                int chuaXacNhan = 0;
                int daXacNhan = 0;
                int daHuy = 0;

                chuaXacNhan = ves.Where(x => x.TrangThai == "Chưa xác nhận").Count();
                daXacNhan = ves.Where(x => x.TrangThai == "Đã xác nhận").Count();
                daHuy = ves.Where(x => x.TrangThai == "Đã hủy").Count();

                var result = new List<TrangThaiVe>();
                result.Add(new TrangThaiVe() { CumRapId=cumRapId, TrangThai = "Chưa xác nhận", SoLuong = chuaXacNhan });
                result.Add(new TrangThaiVe() { CumRapId = cumRapId, TrangThai = "Đã xác nhận", SoLuong = daXacNhan });
                result.Add(new TrangThaiVe() { CumRapId = cumRapId, TrangThai = "Đã hủy", SoLuong = daHuy });
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

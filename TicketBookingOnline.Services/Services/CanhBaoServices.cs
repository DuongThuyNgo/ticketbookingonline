﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.CanhBaos;

namespace TicketBookingOnline.Services.Services
{
    public class CanhBaoServices : ICanhBaoServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CanhBaoServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<CanhBaoViewModel> Create(CanhBaoViewModel canhBaoVM)
        {
            try
            {
                var canhBao = _mapper.Map<CanhBao>(canhBaoVM);
                _unitOfWork.CanhBaoRepository.Create(canhBao);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CanhBaoViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CanhBaoViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int canhBaoId)
        {
            try
            {
                _unitOfWork.CanhBaoRepository.Delete(canhBaoId);
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public IEnumerable<CanhBaoViewModel> GetAll()
        {
            try
            {
                var canhbaos = _unitOfWork.CanhBaoRepository.GetAll();
                var canhbaoVMs = _mapper.Map<IEnumerable<CanhBaoViewModel>>(canhbaos);
                return canhbaoVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

        }

        public CanhBaoViewModel GetById(int canhBaoId)
        {
            try
            {
                var canhbao = _unitOfWork.CanhBaoRepository.GetById(canhBaoId);
                var canhbaoVM = _mapper.Map<CanhBaoViewModel>(canhbao);
                return canhbaoVM;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public ResponseResult<CanhBaoViewModel> Update(CanhBaoViewModel canhBaoVM)
        {
            try
            {
                var canhBao = _mapper.Map<CanhBao>(canhBaoVM);
                _unitOfWork.CanhBaoRepository.Update(canhBao);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CanhBaoViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CanhBaoViewModel>(ex.Message);
            }
        }
    }
}

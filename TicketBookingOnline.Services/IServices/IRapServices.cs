﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Raps;

namespace TicketBookingOnline.Services.IServices
{
    public interface IRapServices
    {
        ResponseResult<RapViewModel> Create(RapViewModel rapVM);
        ResponseResult<RapViewModel> Update(RapViewModel rapVM);
        ResponseResult<int> UpdateLoaiRap(string loaiRap);
        ResponseResult<int> Delete(int rapId);
        IEnumerable<RapViewModel> GetRapByCumRapId(int cumRapId);
        RapViewModel GetRapById(int rapId);
    }
}

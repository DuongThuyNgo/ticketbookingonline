﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.RapGhes;

namespace TicketBookingOnline.Services.IServices
{
    public interface IRapGheServices
    {
        ResponseResult<RapGheViewModel> Update(RapGheViewModel rapGheVM);
        ResponseResult<int> UpdateRange(List<RapGheViewModel> rapGheVMs);
        IEnumerable<RapGheViewModel> GetRapGheByRap(int rapId);
    }
}

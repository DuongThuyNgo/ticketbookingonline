﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.CanhBaos;

namespace TicketBookingOnline.Services.IServices
{
    public interface ICanhBaoServices
    {
        CanhBaoViewModel GetById(int canhBaoId);
        IEnumerable<CanhBaoViewModel> GetAll();
        ResponseResult<CanhBaoViewModel> Create(CanhBaoViewModel canhBaoVM);
        ResponseResult<CanhBaoViewModel> Update(CanhBaoViewModel canhBaoVM);
        ResponseResult<int> Delete(int canhBaoId);
    }
}

﻿using System.Collections.Generic;

using TicketBookingOnline.ViewModels.ThanhPhos;

namespace TicketBookingOnline.Services.IServices
{
    public interface IThanhPhoServices
    {
        IEnumerable<ThanhPhoViewModel> GetAll();
    }
}

﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Sliders;

namespace TicketBookingOnline.Services.IServices
{
    public interface ISliderServices
    {
        SliderViewModel GetById(int sliderId);
        IEnumerable<SliderViewModel> GetAll();
        ResponseResult<SliderViewModel> Create(SliderViewModel sliderVM);
        ResponseResult<SliderViewModel> Update(SliderViewModel sliderVM);
        ResponseResult<int> Delete(int sliderId);
    }
}

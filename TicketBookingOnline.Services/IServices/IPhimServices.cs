﻿using System;
using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Phims;

namespace TicketBookingOnline.Services.IServices
{
    public interface IPhimServices
    {
        IEnumerable<PhimViewModel> GetAll();
        IEnumerable<PhimViewModel> GetAllPhimIsActive();
        IEnumerable<PhimViewModel> GetAllPhimIsActiveWithRowPage(int index);
        PhimDetailViewModel GetPhim(int phimId);
        CreatePhimViewModel GetPhimUpdate(int phimId);
        PhimDetailViewModel GetLastestPhim();
        PhimDetailViewModel GetPhimByAnh(string anh);
        IEnumerable<PhimViewModel> GetPhimByTrangThai(string trangThai);
        IEnumerable<PhimViewModel> GetPhimByTrangThaiWithRowPage(string trangThai, int index);
        IEnumerable<PhimViewModel> GetPhimByTheLoai(int maTheLoai);
        IEnumerable<PhimViewModel> GetPhimByTheLoais(List<int> maTheLoai);
        IEnumerable<PhimViewModel> GetPhimByTheLoaisWithRowPerPage(List<int> maTheLoai,int index, int rowPerPage);
        IEnumerable<PhimViewModel> GetPhimByTheLoaiRowPerPage(int maTheLoai, int index);
        int CountPhimByTheLoai(int maTheLoai);
        int CountPhimByTheLoais(List<int> maTheLoai);
        IEnumerable<PhimViewModel> GetPhimByTenPhim(string tenPhim);
        IEnumerable<PhimViewModel> GetPhimByTenPhimWithRowPage(string tenPhim, int index);
        string GetTheLoaiOfPhim(int PhimId);
        IEnumerable<PhimViewModel> Filter(int? thanhPhoId, int? cumRapId, DateTime? ngayChieu);
        IEnumerable<PhimViewModel> PhimHot();
        IEnumerable<PhimViewModel> PhimCungTheLoai(int phimId);
        ResponseResult<CreatePhimViewModel> Create(CreatePhimViewModel phimVM);
        ResponseResult<CreatePhimViewModel> Update(CreatePhimViewModel phimVM);
        ResponseResult<int> Delete(int phimId);
        ResponseResult<int> UpdateTrangThaiPhim(int phimId, string trangThai);
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Roles;

namespace TicketBookingOnline.Services.IServices
{
    public interface IRoleServices
    {
        IEnumerable<IdentityRole> GetAll();
        Task<ResponseResult<string>> CreateAsync(string roleNames);
        Task<ResponseResult<string>> UpdateAsync(RoleModification model);
        Task<ResponseResult<string>> DeleteAsync(string roleId);
        Task<RoleEdit> FindRoleEdit(string id);
    }
}

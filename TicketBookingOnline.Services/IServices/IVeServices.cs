﻿using System;
using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Ves;

namespace TicketBookingOnline.Services.IServices
{
    public interface IVeServices
    {
        IEnumerable<VeViewModel> GetAllIsActive();
        int CountVe(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName);
        IEnumerable<VeViewModel> Filter(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName);
        IEnumerable<VeViewModel> VeWithRowPerPage(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, int index);
        IEnumerable<VeViewModel> GetVeByKhachHangId(int khachHangId);
        VeDetailViewModel GetById(int veId);
        ResponseResult<bool> ConfirmVe(int veId);
        ResponseResult<bool> CancelVe(DeleteVeViewModel ve);
    }
}

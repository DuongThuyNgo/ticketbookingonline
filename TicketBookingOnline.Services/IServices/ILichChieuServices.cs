﻿using System;
using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.LichChieus;

namespace TicketBookingOnline.Services.IServices
{
    public interface ILichChieuServices
    {
        IEnumerable<LichChieuViewModel> GetLichChieuByPhimId(int phimId);
        IEnumerable<LichChieuViewModel> GetLichChieuOfPhimInCumRap(int phimId, int cumRapId);
        IEnumerable<string> GetLichChieuInCumRap(int cumRapId);
        IEnumerable<LichChieuViewModel> Filter(int phimId, int? cumRapId, int? rapId, DateTime ? ngayChieu);
        ResponseResult<CreateLichChieuViewModel> Create(CreateLichChieuViewModel lichChieuVM);
        ResponseResult<int> Delete(int lichChieuId);
        
    }
}

﻿using System.Threading.Tasks;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.CGVManagers;

namespace TicketBookingOnline.Services.IServices
{
    public interface IAccountServices
    {
        Task<LoginViewModel> SignInAsync(LoginViewModel request);
        Task<ResponseResult<string>> SingOutAsync();
        Task<ResponseResult<CGVManagerViewModel>> UpdateAsync(CGVManagerDetailViewModel request);
        UpdateAccountViewModel FindAccount(string id);
        Task<ResponseResult<ChangePassword>> ResetPassword(ChangePassword request);
    }
}

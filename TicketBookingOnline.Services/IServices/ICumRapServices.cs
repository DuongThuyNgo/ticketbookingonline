﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.CumRaps;

namespace TicketBookingOnline.Services.IServices
{
    public interface ICumRapServices
    {
        CumRapViewModel GetById(int cumRapId);
        IEnumerable<CumRapViewModel> GetAll(string tenCumRap, int ? thanhPhoId);
        int CountAll(string tenCumRap, int? thanhPhoId);
        IEnumerable<CumRapViewModel> GetWithRowPerPage(int index, string tenCumRap, int? thanhPhoId);
        IEnumerable<CumRapViewModel> GetCumRapByThanhPhos(int thanhPhoId);
        ResponseResult<CreateCumRapViewModel> Create(CreateCumRapViewModel cumRapVM);
        ResponseResult<CreateCumRapViewModel> Update(CreateCumRapViewModel cumRapVM);
        ResponseResult<int> Delete(int cumRapId);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.CGVManagers;

namespace TicketBookingOnline.Services.IServices
{
    public interface ICGVManagerServices
    {
        Task<ResponseResult<CGVManagerViewModel>> CreateAsync(CreateCGVManagerViewModel request);
        Task<ResponseResult<CGVManagerViewModel>> UpdateAsync(CGVManagerDetailViewModel request);
        Task<ResponseResult<CGVManagerViewModel>> Delete(CGVManagerDetailViewModel request);
        Task<ResponseResult<CGVManagerViewModel>> DeleteAsync(string id);
        Task<ResponseResult<CGVManagerViewModel>> SignInAsync(LoginCGVManagerViewModel request);
        Task<ResponseResult<CGVManagerViewModel>> RoleAssignAsync(string email, params string[] roleNames);
        IEnumerable<CGVManagerViewModel> GetAll();
        IEnumerable<CGVManagerViewModel> GetWithRowPerPage(int index, int thanhPhoId, int cumRapId);
        int CountAll();
        IEnumerable<CGVManagerViewModel> GetNhanVienInCumRapWithRowPerPage(int cumRapId, int index);
        int CountAllNhanVienInCumRap(int cumRapId);
        CGVManager Find(string id);
        CGVManagerDetailViewModel FindCGVManager(string id);

    }
}

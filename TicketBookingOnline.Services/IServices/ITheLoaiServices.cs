﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.TheLoais;

namespace TicketBookingOnline.Services.IServices
{
    public interface ITheLoaiServices
    {
        IEnumerable<TheLoaiViewModel> GetAll();
        TheLoaiViewModel GetById(int theLoaiId);
        ResponseResult<TheLoaiViewModel> Create(TheLoaiViewModel theLoaiVM);
        ResponseResult<TheLoaiViewModel> Update(TheLoaiViewModel theLoaiVM);
        ResponseResult<int> Delete(int theLoaiId);
    }
}

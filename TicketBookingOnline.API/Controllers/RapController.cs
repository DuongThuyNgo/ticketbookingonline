﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.Raps;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RapController : ControllerBase
    {
        private readonly IRapServices _rapServices;

        public RapController(IRapServices rapServices)
        {
            _rapServices = rapServices;
        }

        [HttpGet]
        [Route("GetRapByCumRapId/{cumRapId}")]
        public IActionResult GetRapByCumRapId(int cumRapId)
        {
            if (cumRapId == null)
            {
                return BadRequest();
            }
            var raps = _rapServices.GetRapByCumRapId(cumRapId);
            return Ok(raps);

        }

        [HttpGet]
        [Route("GetRapById/{rapId}")]
        public IActionResult GetRapById(int rapId)
        {
            if (rapId == null)
            {
                return BadRequest();
            }
            var rap = _rapServices.GetRapById(rapId);
            return Ok(rap);

        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(RapViewModel rapVM)
        {
            if (rapVM == null)
            {
                return BadRequest();
            }
            var response = _rapServices.Create(rapVM);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Create Error");
                return StatusCode(500, ModelState);
            }
            return Ok(response);

        }

        [HttpPatch]
        [Route("Update")]
        public IActionResult Update(RapViewModel rap)
        {
            if (rap == null)
            {
                return BadRequest();
            }
            var response = _rapServices.Update(rap);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error");
                return StatusCode(500, ModelState);
            }
            return Ok();

        }

        [HttpDelete]
        [Route("Delete/{rapId}")]
        public IActionResult Delete(int rapId)
        {
            if (rapId == null)
            {
                return BadRequest();
            }
            var response = _rapServices.Delete(rapId);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Delete Error");
                return StatusCode(500, ModelState);
            }
            return NoContent();

        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.PhimTheLoais;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhimTheLoaiController : ControllerBase
    {
        private readonly IPhimTheLoaiServices _phimTheLoaiServices;

        public PhimTheLoaiController(IPhimTheLoaiServices phimTheLoaiServices)
        {
            _phimTheLoaiServices = phimTheLoaiServices;
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(List<PhimTheLoaiViewModel> phimTheLoaiVMs)
        {
            if (phimTheLoaiVMs == null)
            {
                return BadRequest(ModelState);
            }
            var response = _phimTheLoaiServices.Create(phimTheLoaiVMs);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Create Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

    }
}

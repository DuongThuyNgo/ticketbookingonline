﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Services.IServices;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentServices _commentServices;

        public CommentController(ICommentServices commentServices)
        {
            _commentServices = commentServices;
        }

        [HttpGet]
        [Route("ConfirmBinhLuan/{commentId}")]
        public IActionResult ConfirmBinhLuan(int commentId)
        {
            var response = _commentServices.ConfirmComment(commentId);
            if (!response)
            {
                ModelState.AddModelError($"", "Confirm error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }
    }
}

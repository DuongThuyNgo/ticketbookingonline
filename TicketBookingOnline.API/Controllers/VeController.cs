﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.Ves;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VeController : ControllerBase
    {
        private readonly IVeServices _veServices;

        public VeController(IVeServices veServices)
        {
            _veServices = veServices;
        }

        [HttpGet]
        [Route("GetAllIsActive")]
        public IActionResult GetAllIsAcitve()
        {
            var ves = _veServices.GetAllIsActive();
            return Ok(ves);
        }

        [HttpGet]
        [Route("CountTotalVe")]
        public IActionResult CountTotalVe(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName)
        {
            int total = _veServices.CountVe(cumRapId, rapId, ngayChieu, tenPhim, userName);
            return Ok(total);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName)
        {
            var ves = _veServices.Filter(cumRapId, rapId, ngayChieu, tenPhim, userName);
            return Ok(ves);
        }

        [HttpGet]
        [Route("VeWithPerRowPage/{index}")]
        public IActionResult VeWithPerRowPage(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, int index)
        {
            var ves = _veServices.VeWithRowPerPage(cumRapId, rapId, ngayChieu, tenPhim, userName, index);
            return Ok(ves);
        }

        [HttpGet]
        [Route("GetVeByKhachHangId/{khachHangId}")]
        public IActionResult GetVeByKhachHangId(int khachHangId)
        {
            var ves = _veServices.GetVeByKhachHangId(khachHangId);
            return Ok(ves);
        }

        [HttpGet]
        [Route("GetById/{veId}")]
        public IActionResult GetById(int veId)
        {
            if (veId == null)
            {
                return BadRequest();
            }
            var ve = _veServices.GetById(veId);
            return Ok(ve);
        }

        [HttpGet]
        [Route("ConfirmVe/{veId}")]
        public IActionResult ConfirmVe(int veId)
        {
            if(veId == null)
            {
                return BadRequest();
            }
            var response = _veServices.ConfirmVe(veId);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Confirm error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpPost]
        [Route("CancelVe")]
        public IActionResult CancelVe(DeleteVeViewModel veVM)
        {
            if (veVM == null)
            {
                return BadRequest();
            }
            var response = _veServices.CancelVe(veVM);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Cancel error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }
    }
}

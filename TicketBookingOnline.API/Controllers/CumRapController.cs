﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.CumRaps;

namespace TicketBookingOnline.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CumRapController : ControllerBase
    {

        private readonly ICumRapServices _cumRapServices;

        public CumRapController(ICumRapServices cumRapServices)
        {
            _cumRapServices = cumRapServices;
        }

        [HttpGet]
        [Route("GetById/{cumRapId}")]
        public IActionResult GetById(int cumRapId)
        {
            if (cumRapId == null)
            {
                return BadRequest(ModelState);
            }
            var cumRap = _cumRapServices.GetById(cumRapId);
            return Ok(cumRap);
        }


        [HttpGet]
        [Route("GetCumRapByThanhPhos/{thanhPhoId}")]
        public IActionResult GetCumRapByThanhPhos(int thanhPhoId)
        {
            if (thanhPhoId == null)
            {
                return BadRequest(ModelState);
            }
            var cumRaps = _cumRapServices.GetCumRapByThanhPhos(thanhPhoId);

            return Ok(cumRaps);
        }

        [HttpGet]
        [Route("CountAll")]
        public IActionResult CountAll(string tenCumRap, int thanhPhoId)
        {
            int count = _cumRapServices.CountAll(tenCumRap, thanhPhoId);

            return Ok(count);
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll(string tenCumRap, int thanhPhoId)
        {
            var cumRaps = _cumRapServices.GetAll(tenCumRap, thanhPhoId);

            return Ok(cumRaps);
        }

        [HttpGet]
        [Route("GetWithRowPerPage/{index}")]
        public IActionResult GetWithRowPerPage(int index, string tenCumRap, int thanhPhoId)
        {
            var cumRaps = _cumRapServices.GetWithRowPerPage(index, tenCumRap, thanhPhoId);

            return Ok(cumRaps);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(CreateCumRapViewModel cumRapVM)
        {
            if(cumRapVM == null)
            {
                return BadRequest();
            }
            var response = _cumRapServices.Create(cumRapVM);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Create Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpPatch]
        [Route("Update")]
        public IActionResult Update(CreateCumRapViewModel cumRapVM)
        {
            if (cumRapVM == null)
            {
                return BadRequest();
            }
            var response = _cumRapServices.Update(cumRapVM);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpDelete]
        [Route("Delete/{cumRapId}")]
        public IActionResult Delete(int cumRapId)
        {
            if (cumRapId == null)
            {
                return BadRequest();
            }
            var response = _cumRapServices.Delete(cumRapId);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Delete Error");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.Sliders;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SliderController : ControllerBase
    {
        private readonly ISliderServices _sliderServices;

        public SliderController(ISliderServices sliderServices)
        {
            _sliderServices = sliderServices;
        }

        [HttpGet]
        [Route("GetById/{sliderId}")]
        public IActionResult GetById(int sliderId)
        {
            var slider = _sliderServices.GetById(sliderId);
            return Ok(slider);
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            var sliders = _sliderServices.GetAll();
            return Ok(sliders);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(SliderViewModel slider)
        {
            if (slider == null)
                return BadRequest();
            var response = _sliderServices.Create(slider);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Create Error !");
                return StatusCode(500, ModelState);
            }
            return Ok(response);
        }

        [HttpPatch]
        [Route("Update")]
        public IActionResult Update(SliderViewModel slider)
        {
            if (slider == null)
                return BadRequest();
            var response = _sliderServices.Update(slider);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpDelete]
        [Route("Delete/{sliderId}")]
        public IActionResult Delete(int sliderId)
        {
            if (sliderId == null)
                return BadRequest();
            var response = _sliderServices.Delete(sliderId);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Delete Error !");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }
    }
}

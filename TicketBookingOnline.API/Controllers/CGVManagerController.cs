﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.CGVManagers;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CGVManagerController : ControllerBase
    {
        private readonly ICGVManagerServices _cGVMangerServices;

        public CGVManagerController(ICGVManagerServices cGVMangerServices)
        {
            _cGVMangerServices = cGVMangerServices;
        }

        [HttpGet]
        [Route("CountAll")]
        public IActionResult CountAll()
        {
            int count = _cGVMangerServices.CountAll();
            return Ok(count);
        }

        [HttpGet]
        [Route("CountAllNhanVienInCumRapId/{cumRapId}")]
        public IActionResult CountAllNhanVienInCumRapId(int cumRapId)
        {
            int count = _cGVMangerServices.CountAllNhanVienInCumRap(cumRapId);
            return Ok(count);
        }

        [HttpGet]
        [Route("GetAllAsync")]
        public IActionResult GetAllAsync()
        {
            var cGVManagers = _cGVMangerServices.GetAll().ToList();
            if (cGVManagers.Count == 0)
            {
                ModelState.AddModelError("", $"List CGV Manager is null !");
                return StatusCode(404, ModelState);
            }
            return Ok(cGVManagers);
        }

        [HttpGet]
        [Route("GetWithRowPerPage/{index}/{thanhPhoId}/{cumRapId}")]
        public IActionResult GetWithRowPerPage(int index, int thanhPhoId, int cumRapId)
        {
            var cGVManagers = _cGVMangerServices.GetWithRowPerPage(index, thanhPhoId, cumRapId).ToList();
            if (cGVManagers.Count == 0)
            {
                ModelState.AddModelError("", $"List CGV Manager is null !");
                return StatusCode(404, ModelState);
            }
            return Ok(cGVManagers);
        }

       
        [HttpGet]
        [Route("GetAllNhanVienInCumRapWithRowPerPage/{cumRapId}/{index}")]
        public IActionResult GetAllNhanVienInCumRapWithRowPerPage(int cumRapId, int index)
        {
            if( cumRapId == null ||index == null)
            {
                return BadRequest();
            }
            var cGVManagers = _cGVMangerServices.GetNhanVienInCumRapWithRowPerPage(cumRapId, index).ToList();
            if (cGVManagers.Count == 0)
            {
                ModelState.AddModelError("", $"List staff is null !");
                return StatusCode(404, ModelState);
            }
            return Ok(cGVManagers);
        }



        [HttpGet]
        [Route("GetCGVManager/{id}")]
        public IActionResult GetCGVManager(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var cGVManager = _cGVMangerServices.FindCGVManager(id);
            return Ok(cGVManager);
        }

        [HttpPost]
        [Route("CreateAsync")]
        public async Task<IActionResult> CreateAsync(CreateCGVManagerViewModel request)
        {
            if (request == null)
            {
                return BadRequest(ModelState);
            }
            var response = await _cGVMangerServices.CreateAsync(request);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new Error() { Message= response.ErrrorMessages[0]});
            }
            return Ok();
        }

        [HttpPatch]
        [Route("UpdateAsync")]
        public async Task<IActionResult> UpdateAsync(CGVManagerDetailViewModel request)
        {
            if (request == null)
            {
                return BadRequest(ModelState);
            }
            var response = await _cGVMangerServices.UpdateAsync(request);

            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpDelete]
        [Route("DeleteAsync/{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            if (id == null)
            {
                return BadRequest(ModelState);
            }
           // var cgv = new CGVManagerDetailViewModel() { Id = id };
            var response = _cGVMangerServices.DeleteAsync(id);

            return NoContent();
        }

        [HttpGet]
        [Route("SignInAsync")]
        public IActionResult SignInAsync(LoginCGVManagerViewModel request)
        {
            if (request == null)
            {
                return BadRequest(ModelState);
            }
            var cGVManager = _cGVMangerServices.SignInAsync(request);

            return NoContent();
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.Phims;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhimController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IPhimServices _phimServices;

        public PhimController(IPhimServices phimServices)
        {
            _phimServices = phimServices;
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(CreatePhimViewModel phimVM)
        {
            if (phimVM == null)
            {
                return BadRequest(ModelState);
            }
            var response = _phimServices.Create(phimVM);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Create Error");
                return StatusCode(500, ModelState);
            }
            return Ok(response);
        }

        [HttpPatch]
        [Route("Update")]
        public IActionResult Update(CreatePhimViewModel phimVM)
        {
            if (phimVM == null)
            {
                return BadRequest(ModelState);
            }
            var response = _phimServices.Update(phimVM);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpDelete]
        [Route("Delete/{phimId}")]
        public IActionResult Delete(int phimId)
        {
            if (phimId == null)
            {
                return BadRequest(ModelState);
            }
            var response = _phimServices.Delete(phimId);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Delete Error");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }

        [HttpGet]
        [Route("GetPhimById/{phimId}")]       
        public IActionResult GetPhimById(int phimId)
        {
            if (phimId == null)
            {
                return BadRequest(ModelState);
            }
            var phim = _phimServices.GetPhim(phimId);

            return Ok(phim);
        }

        [HttpGet]
        [Route("GetPhimByTenPhim/{tenPhim}")]
        public IActionResult GetPhimByTenPhim(string tenPhim)
        {
            if (tenPhim == null)
            {
                return BadRequest(ModelState);
            }
            var phims = _phimServices.GetPhimByTenPhim(tenPhim);

            return Ok(phims);
        }

        [HttpGet]
        [Route("CountPhimByTenPhim/{tenPhim}")]
        public IActionResult CountPhimByTenPhim(string tenPhim)
        {
            if (tenPhim == null)
            {
                return BadRequest(ModelState);
            }
            var phims = _phimServices.GetPhimByTenPhim(tenPhim).ToList();

            return Ok(phims.Count());
        }

        [HttpGet]
        [Route("GetPhimByTenPhimWithRowperPage/{index}/{tenPhim}")]
        public IActionResult GetPhimByTenPhimWithRowperPage(string tenPhim, int index)
        {
            if (tenPhim == null)
            {
                return BadRequest(ModelState);
            }
            var phims = _phimServices.GetPhimByTenPhimWithRowPage(tenPhim, index);

            return Ok(phims);
        }

        [HttpGet]
        [Route("GetPhimUpdate/{phimId}")]
        public IActionResult GetPhimUpdate(int phimId)
        {
            if (phimId == null)
            {
                return BadRequest(ModelState);
            }
            var phim = _phimServices.GetPhimUpdate(phimId);

            return Ok(phim);
        }

        [HttpGet]
        [Route("GetLastestPhim")]
        public IActionResult GetLastestPhim()
        {
            var phim = _phimServices.GetLastestPhim();
            return Ok(phim);
        }

        [HttpGet]
        [Route("GetTheLoaiOfPhim")]
        public IActionResult GetTheLoaiOfPhim(int phimId)
        {
            var theLoai = _phimServices.GetTheLoaiOfPhim(phimId);
            return Ok(theLoai);
        }

        [HttpGet]
        [Route("CountPhimByTheLoai/{maTheLoai}")]
        public IActionResult CountPhimByTheLoai(int maTheLoai)
        {
            if (maTheLoai == null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTheloai = _phimServices.CountPhimByTheLoai(maTheLoai);

            return Ok(getPhimByTheloai);
        }

        [HttpGet]
        [Route("CountPhimByTheLoais")]
        public IActionResult CountPhimByTheLoais( List<int> maTheLoai)
        {
            if (maTheLoai == null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTheloai = _phimServices.CountPhimByTheLoais(maTheLoai);

            return Ok(getPhimByTheloai);
        }

        [HttpGet]
        [Route("GetPhimByTheLoaiRowPerPage/{maTheLoai}/{index}")]
        public IActionResult GetPhimByTheLoaiRowPerPage(int maTheLoai, int index)
        {
            if (maTheLoai == null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTheloai = _phimServices.GetPhimByTheLoaiRowPerPage(maTheLoai, index);

            return Ok(getPhimByTheloai);
        }


        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            var phims = _phimServices.GetAll();
            return Ok(phims);
        }

        [HttpGet]
        [Route("GetAllPhimIsActive")]
        public IActionResult GetAllPhimIsActive()
        {
            var phims = _phimServices.GetAllPhimIsActive();
            return Ok(phims);
        }

        [HttpGet]
        [Route("CountPhimIsActive")]
        public IActionResult CountPhimIsActive()
        {
            var phims = _phimServices.GetAllPhimIsActive().ToList();
            return Ok(phims.Count());
        }

        [HttpGet]
        [Route("GetAllPhimIsActiveWithRowperPage/{index}")]
        public IActionResult GetAllPhimIsActiveWithRowperPage(int index)
        {
            var phims = _phimServices.GetAllPhimIsActiveWithRowPage(index);
            return Ok(phims);
        }


        [HttpGet]
        [Route("GetPhimByTrangThai/{trangThai}")]

        public IActionResult GetPhimByTrangThai(string trangThai)
        {
            if (trangThai == null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTrangThai = _phimServices.GetPhimByTrangThai(trangThai);

            return Ok(getPhimByTrangThai);
        }

        [HttpGet]
        [Route("CountPhimByTrangThai/{trangThai}")]

        public IActionResult CountPhimByTrangThai(string trangThai)
        {
            if (trangThai == null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTrangThai = _phimServices.GetPhimByTrangThai(trangThai).ToList();

            return Ok(getPhimByTrangThai.Count());
        }

        [HttpGet]
        [Route("GetPhimByTrangThaiWithRowperPage/{index}/{trangThai}")]
        public IActionResult GetPhimByTrangThaiWithRowperPage(string trangThai, int index)
        {
            if (trangThai == null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTrangThai = _phimServices.GetPhimByTrangThaiWithRowPage(trangThai, index);

            return Ok(getPhimByTrangThai);
        }

        [HttpGet]
        [Route("GetPhimByTheLoai/{maTheLoai}")]

        public IActionResult GetPhimByTheLoai(int maTheLoai)
        {
            if (maTheLoai == null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTheloai = _phimServices.GetPhimByTheLoai(maTheLoai);

            return Ok(getPhimByTheloai);
        }

        [HttpGet("GetPhimByTheLoais")]
        public IActionResult GetPhimByTheLoais(List<int> maTheLoai)
        {
            if (maTheLoai == null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTheloai = _phimServices.GetPhimByTheLoais(maTheLoai);

            return Ok(getPhimByTheloai);
        }

        [HttpGet("GetPhimByTheLoaisWithRowPerPage/{index}/{rowPerPage}")]
        public IActionResult GetPhimByTheLoais(List<int> maTheLoai, int index, int rowPerPage)
        {
            if (maTheLoai == null || index == null || rowPerPage ==null)
            {
                return BadRequest(ModelState);
            }
            var getPhimByTheloai = _phimServices.GetPhimByTheLoaisWithRowPerPage(maTheLoai, index, rowPerPage);

            return Ok(getPhimByTheloai);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(int? thanhPhoId, int? cumRapId, DateTime? ngayChieu)
        {
            var phims = _phimServices.Filter(thanhPhoId, cumRapId, ngayChieu);
            return Ok(phims);
        }

        [HttpGet]
        [Route("PhimHot")]
        public IActionResult PhimHot()
        {
            var phims = _phimServices.PhimHot();
            return Ok(phims);
        }

        [HttpGet]
        [Route("PhimLienQuan/{phimId}")]
        public IActionResult PhimLienQuan(int phimId)
        {
            if(phimId == null)
                return BadRequest();
            var phims = _phimServices.PhimCungTheLoai(phimId);
            return Ok(phims);
        }

        [HttpPost]
        [Route("UpdateTrangThaiPhim/{phimId}/{trangThai}")]
        public IActionResult UpdateTrangThaiPhim(int phimId, string trangThai)
        {
            if(phimId == null || trangThai == null)
            {
                return BadRequest();
            }
            var response = _phimServices.UpdateTrangThaiPhim(phimId, trangThai);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }
    }
}

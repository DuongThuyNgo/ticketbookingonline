﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.LichChieus;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LichChieuController : ControllerBase
    {
        private readonly ILichChieuServices _lichChieuServices;

        public LichChieuController(ILichChieuServices lichChieuServices)
        {
            _lichChieuServices = lichChieuServices;
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(CreateLichChieuViewModel lichChieu)
        {
            if(lichChieu == null)
            {
                return BadRequest();
            }
            var response = _lichChieuServices.Create(lichChieu);
             if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Lich Chieu is existing !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpDelete]
        [Route("Delete/{lichChieuId}")]
        public IActionResult Delete(int lichChieuId)
        {
            if (lichChieuId == null)
            {
                return BadRequest(ModelState);
            }
            var response = _lichChieuServices.Delete(lichChieuId);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Delete Error");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }

        [HttpGet]
        [Route("GetLichChieuInCumRap/{cumRapId}")]
        public IActionResult GetLichChieuInCumRap(int cumRapId)
        {
            if (cumRapId == 0)
            {
                return BadRequest();
            }
            var lichChieus = _lichChieuServices.GetLichChieuInCumRap(cumRapId);
            return Ok(lichChieus);
        }

        [HttpGet]
        [Route("GetLichChieuOfPhimInCumRap/{phimId}/{cumRapId}")]
        public IActionResult GetLichChieuOfPhimInCumRap(int phimId, int cumRapId)
        {
            if (phimId == null || cumRapId == null)
            {
                return BadRequest();
            }
            var lichChieus = _lichChieuServices.GetLichChieuOfPhimInCumRap(phimId, cumRapId);
            return Ok(lichChieus);
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(int phimId, int? cumRapId, int? rapId, DateTime? ngayChieu)
        {
            if (phimId == null)
            {
                return BadRequest();
            }
            var lichChieus = _lichChieuServices.Filter(phimId, cumRapId, rapId, ngayChieu);
            return Ok(lichChieus);
        }
    }
}

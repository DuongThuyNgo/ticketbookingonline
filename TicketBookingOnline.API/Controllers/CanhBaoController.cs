﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.CanhBaos;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CanhBaoController : ControllerBase
    {
        private readonly ICanhBaoServices _canhBaoServices;

        public CanhBaoController(ICanhBaoServices canhBaoServices)
        {
            _canhBaoServices = canhBaoServices;
        }

        [HttpGet]
        [Route("GetById/{canhBaoId}")]
        public IActionResult GetById(int canhBaoId)
        {
            var canhbao = _canhBaoServices.GetById(canhBaoId);
            return Ok(canhbao);
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            var canhbaos = _canhBaoServices.GetAll();
            return Ok(canhbaos);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(CanhBaoViewModel canhBao)
        {
            if (canhBao == null)
                return BadRequest();
            var response = _canhBaoServices.Create(canhBao);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Create Error !");
                return StatusCode(500, ModelState);
            }
            return Ok(response);
        }

        [HttpPatch]
        [Route("Update")]
        public IActionResult Update(CanhBaoViewModel canhBao)
        {
            if (canhBao == null)
                return BadRequest();
            var response = _canhBaoServices.Update(canhBao);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpDelete]
        [Route("Delete/{canhBaoId}")]
        public IActionResult Delete(int canhBaoId)
        {
            if (canhBaoId == null)
                return BadRequest();
            var response = _canhBaoServices.Delete(canhBaoId);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Delete Error !");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }
    }
}
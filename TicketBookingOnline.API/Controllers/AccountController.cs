﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.CGVManagers;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountServices _accountServices;

        public AccountController(IAccountServices accountServices)
        {
            _accountServices = accountServices;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginViewModel request)
        {
            if (request == null)
            {
                return BadRequest(ModelState);
            }
            var userLogin = await _accountServices.SignInAsync(request);
            if (userLogin == null)
            {
                ModelState.AddModelError("", $"Error Sign In !");
                return StatusCode(500, ModelState);
            }
            return Ok(userLogin);
        }

        [HttpGet]
        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            await _accountServices.SingOutAsync();
            return Ok();
        }

        [HttpGet]
        [Route("FindAccount/{id}")]
        public async Task<IActionResult> FindAccount(string id)
        {
            if (id == null)
            {
                return BadRequest(ModelState);
            }
            var account = _accountServices.FindAccount(id);
            return Ok(account);
        }

        [HttpPost]
        [Route("UpdateInforCGVManager")]
        public async Task<IActionResult> UpdateInforCGVManager(CGVManagerDetailViewModel request)
        {
            if (request == null)
            {
                return BadRequest(ModelState);
            }
            var response = await _accountServices.UpdateAsync(request);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new Error() { Message = response.ErrrorMessages[0] });
            }
            return Ok();
        }

        [HttpPost]
        [Route("ChangePassword")]
        public async Task<IActionResult> ChangePassword(ChangePassword resetPassword)
        {
            if (resetPassword == null)
            {
                return BadRequest(ModelState);
            }
            var response = await _accountServices.ResetPassword(resetPassword);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new Error() { Message = response.ErrrorMessages[0] });
            }
            return Ok();
        }
    }
}
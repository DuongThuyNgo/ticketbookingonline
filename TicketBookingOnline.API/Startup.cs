﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;
using TicketBookingOnline.Core;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Helper;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.Services.Services;

namespace TicketBookingOnline.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TicketBookingOnlineDBContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
                );
            services.AddIdentity<CGVManager, IdentityRole>(options =>
            {
                options.User.AllowedUserNameCharacters = "aạàáảãăặắằẳẵâậấầẩẫbcdđeẹéèẻẽêệếềểễfghiịíìỉĩjklmnoọòóỏõôộồóổỗơợờớởỡpqrstuụùúủũưựừứửữvwxyzABCDĐEÊFGHIJKLMNOÔƠWPQRSTUVWXYZ0123456789 ";
                options.User.RequireUniqueEmail = true;
                options.SignIn.RequireConfirmedEmail = false;
                options.Password.RequireDigit = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
            }).AddEntityFrameworkStores<TicketBookingOnlineDBContext>();

            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
            });

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ICanhBaoServices, CanhBaoServices>();
            services.AddScoped<ICumRapServices, CumRapServices>();
            services.AddScoped<IThanhPhoServices, ThanhPhoServices>();
            services.AddScoped<ILichChieuServices, LichChieuServices>();
            services.AddScoped<IPhimServices, PhimServices>();
            services.AddScoped<IRapGheServices, RapGheServices>();
            services.AddScoped<IRapServices, RapServices>();
            services.AddScoped<ITheLoaiServices, TheLoaiServices>();
            services.AddScoped<IPhimTheLoaiServices, PhimTheLoaiServices>();
            services.AddScoped<IVeServices, VeServices>();
            services.AddScoped<IKhachHangServices, KhachHangServices>();
            services.AddScoped<ICGVManagerServices, CGVManagerServices>();
            services.AddScoped<IThongKeServices, ThongKeServices>();
            services.AddScoped<IAccountServices, AccountServices>();
            services.AddScoped<ICommentServices, CommentServices>();
            services.AddScoped<IRoleServices, RoleServices>();
            services.AddScoped<ISliderServices, SliderServices>();

            services.AddAutoMapper(typeof(Mapper));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TicketBookingOnline.API", Version = "v1" });
            });
            services.AddApplicationInsightsTelemetry();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TicketBookingOnline.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

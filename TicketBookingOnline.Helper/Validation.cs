﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketBookingOnline.Helper
{
    public static class Validation
    {
        public static bool PhoneNumberValidate(this string phoneNumber)
        {
            if (phoneNumber.Length != 10)
            {
                return false;
            }
            return Int64.TryParse(phoneNumber,out long phone);
        }
    }
}

﻿using AutoMapper;
using TicketBookingOnline.Core;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.CanhBaos;
using TicketBookingOnline.ViewModels.CGVManagers;
using TicketBookingOnline.ViewModels.CumRaps;
using TicketBookingOnline.ViewModels.LichChieus;
using TicketBookingOnline.ViewModels.LoaiGhes;
using TicketBookingOnline.ViewModels.Phims;
using TicketBookingOnline.ViewModels.PhimTheLoais;
using TicketBookingOnline.ViewModels.RapGhes;
using TicketBookingOnline.ViewModels.Raps;
using TicketBookingOnline.ViewModels.ThanhPhos;
using TicketBookingOnline.ViewModels.TheLoais;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.Ves;
using TicketBookingOnline.ViewModels.Comments;
using TicketBookingOnline.ViewModels.Sliders;

namespace TicketBookingOnline.Helper
{
    public class Mapper:Profile
    {
        public Mapper()
        {
            CreateMap<Slider, SliderViewModel>().ReverseMap();

            CreateMap<Phim, PhimViewModel>().ReverseMap();
            CreateMap<Phim, PhimDetailViewModel>().ReverseMap();
            CreateMap<Phim, CreatePhimViewModel>().ReverseMap();

            CreateMap<LichChieu, CreateLichChieuViewModel>().ReverseMap();
            CreateMap<LichChieu, LichChieuViewModel>().ReverseMap();

            CreateMap<ThanhPho, ThanhPhoViewModel>().ReverseMap();

            CreateMap<CumRap, CumRapViewModel>().ReverseMap();
            CreateMap<CumRap, CreateCumRapViewModel>().ReverseMap();

            CreateMap<Rap, RapViewModel>().ReverseMap();
            CreateMap<RapGhe, RapGheViewModel>().ReverseMap();
            CreateMap<RapGhe, UpdateRapGheViewModel>().ReverseMap();
            CreateMap<RapGheViewModel, UpdateRapGheViewModel>().ReverseMap();
            CreateMap<LoaiGhe, LoaiGheViewModel>().ReverseMap();

            CreateMap<CanhBao, CanhBaoViewModel>().ReverseMap();

            CreateMap<TheLoai, TheLoaiViewModel>().ReverseMap();

            CreateMap<PhimTheLoai, PhimTheLoaiViewModel>().ReverseMap();

            CreateMap<Ve, VeViewModel>().ReverseMap();
            CreateMap<Ve, VeDetailViewModel>().ReverseMap();

            CreateMap<Comment, CommentViewModel>().ReverseMap();

            CreateMap<KhachHang, KhachHangViewModel>().ReverseMap();
            CreateMap<KhachHang, CreateKhachHangViewModel>().ReverseMap();

            CreateMap<CGVManager, CGVManagerViewModel>().ReverseMap();
            CreateMap<CGVManager, CreateCGVManagerViewModel>().ReverseMap();
            CreateMap<CGVManager, CGVManagerDetailViewModel>().ReverseMap();
            CreateMap<CGVManager, UpdateCGVManagerViewModel>().ReverseMap();
            CreateMap<CGVManager, UpdateAccountViewModel>().ReverseMap();

            CreateMap<CGVManager, LoginViewModel>().ReverseMap();
        }
        
    }
}

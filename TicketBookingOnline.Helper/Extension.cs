﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketBookingOnline.Helper
{
    public static class Extension
    {
        public static List<DateTime> DaysOfWeek(DateTime ngay)
        {
            //DateTime backUp = ngay;
            //List<string> daysOfWeek = new List<string>();
            //while (ngay.DayOfWeek.ToString() != "Monday")
            //{
            //    daysOfWeek.Add(ngay.ToString("dd/MM/yyyy"));
            //    ngay = ngay.AddDays(-1);
            //}
            //daysOfWeek.Add(ngay.ToString("dd/MM/yyyy"));
            //if (backUp.DayOfWeek.ToString() != "Sunday")
            //{
            //    ngay = backUp.AddDays(1);
            //    while (ngay.DayOfWeek.ToString() != "Sunday")
            //    {
            //        daysOfWeek.Add(ngay.ToString("dd/MM/yyyy"));
            //        ngay = ngay.AddDays(1);
            //    }
            //    daysOfWeek.Add(ngay.ToString("dd/MM/yyyy"));
            //}
            //return daysOfWeek;
            DateTime backUp = ngay;
            List<DateTime> daysOfWeek = new List<DateTime>();
            while (ngay.DayOfWeek.ToString() != "Monday")
            {
                daysOfWeek.Add(ngay);
                ngay = ngay.AddDays(-1);
            }
            daysOfWeek.Add(ngay);
            if (backUp.DayOfWeek.ToString() != "Sunday")
            {
                ngay = backUp.AddDays(1);
                while (ngay.DayOfWeek.ToString() != "Sunday")
                {
                    daysOfWeek.Add(ngay);
                    ngay = ngay.AddDays(1);
                }
                daysOfWeek.Add(ngay);
            }
            return daysOfWeek;
        }
    }
}

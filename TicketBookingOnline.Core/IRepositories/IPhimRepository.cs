﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface IPhimRepository : IBaseRepository<Phim>
    {
        Phim GetPhim(int phimId);
        Phim GetPhimByAnh(string anh);
        Phim GetLastestPhim();
        IEnumerable<Phim> GetPhimByTrangThai(string trangThai);
        IEnumerable<Phim> GetPhimByTrangThaiWithRowperPage(string trangThai, int index);
        IEnumerable<Phim> SearchPhim(string tenPhim);
        IEnumerable<Phim> SearchPhimWithRowperPage(string tenPhim, int index);
        IEnumerable<Phim> GetPhimByTheLoai(int maTheLoai);
        IEnumerable<Phim> GetPhimByTheLoais(List<int> maTheLoai);
        IEnumerable<Phim> GetPhimByTheLoaisWithRowPerPage(List<int> maTheLoai, int index, int rowPerPage);
        IEnumerable<Phim> GetAllPhimIsActive();
        IEnumerable<Phim> GetAllPhimIsActiveWithRowperPage(int index);
        string TheLoaiOfPhim(int phimId);
        public IEnumerable<Phim> Filter(int? thanhPhoId, int? cumRapId, DateTime? ngayChieu);
        bool DeletePhim(int phimId);
        IEnumerable<Phim> GetPhimByTheLoaiRowPerPage(int maTheLoai, int index);
        int CountPhimByTheLoai(int maTheLoai);
        int CountPhimByTheLoais(List<int> maTheLoai);
        IEnumerable<Phim> PhimHot();
        IEnumerable<Phim> PhimCungTheLoai(int phimId);

    }
}

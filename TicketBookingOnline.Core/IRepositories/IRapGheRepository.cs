﻿using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface IRapGheRepository:IBaseRepository<RapGhe>
    {
        //void UpdateRange(List<RapGhe> rapGhes);
        IEnumerable<RapGhe> GetRapGheByRap(int ? rapId);
    }
}

﻿using System.Collections.Generic;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface ICGVManagerRepository
    {
        void Delete(CGVManager cGVManager);
        IEnumerable<CGVManager> GetNhanVienInCumRapWithRowPerPage(int cumRapId, int index);
        int CountAllNhanVienInCumRap(int cumRapId);
        string RoleName(string userId);
    }
}

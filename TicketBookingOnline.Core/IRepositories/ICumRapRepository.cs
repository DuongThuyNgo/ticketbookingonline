﻿using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface ICumRapRepository : IBaseRepository<CumRap>
    {
        IEnumerable<CumRap> GetAllDetails();
        IEnumerable<CumRap> GetAllDetailWithRowPerPage(int index);
        CumRap GetDetailById(int cumRapId);
        bool DeleteCumRap(int cumRapId);
    }
}

﻿using System;
using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface IVeRepository : IBaseRepository<Ve>
    {
        IEnumerable<Ve> GetAllIsActive();
        IEnumerable<Ve> Filter(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName);
        IEnumerable<Ve> VeWithRowPerPage(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, int index);
        IEnumerable<Ve> GetVeByKhachHangId(int khachHangId);
        IEnumerable<Ve> GetVeByNgay(int? cumRapId, DateTime ngay);
        IEnumerable<Ve> GetVeByTuan(int? cumRapId, DateTime ngay);
        IEnumerable<Ve> GetVeByThang(int? cumRapId, int thang, int nam);
        IEnumerable<Ve> GetVeByNam(int? cumRapId, int nam);
        IEnumerable<Ve> GetVeByTrangThai(int? cumRapId, string trangThai);
        Ve GetDetailById(int veId);
        bool ConfirmVe(int veId);
        bool CancelVe(int veId, string note);
    }
}

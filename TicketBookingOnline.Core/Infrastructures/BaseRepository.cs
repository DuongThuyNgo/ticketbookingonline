﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Infrastructures
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly TicketBookingOnlineDBContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public BaseRepository(TicketBookingOnlineDBContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }



        /// <summary>
        /// Change state of entity to added.
        /// </summary>
        /// <param name="entity"></param>
        public void Create(TEntity entity)
        {
            _dbSet.Add(entity);

        }

        /// <summary>
        /// Change state of entities to added.
        /// </summary>
        /// <param name="entity"></param>
        public void CreateRange(IEnumerable<TEntity> entities)
        {
            _dbSet.AddRange(entities);
        }

        /// <summary>
        /// Change state of entity to deleted.
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void Delete(int id)
        {
            var entity = _dbSet.Find(id);
            if (entity == null)
                throw new ArgumentNullException($"Khong the tim thay {entity.GetType().Name} Id={id}");
            _dbSet.Remove(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        
        public IEnumerable<TEntity> Find(Func<TEntity, bool> confident)
        {
            return _dbSet.Where(confident);
        }


        //5 model
        //public TEntity GetByUrl(string condition)
        //{
        //    return _dbSet.First(condition);
        //}

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.Where(x=>x.IsActive == true).ToList();
        }       

        public TEntity GetById(int primaryKey)
        {
            return _dbSet.Find(primaryKey);
        }


        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            _dbSet.UpdateRange(entities);
        }
    }
}

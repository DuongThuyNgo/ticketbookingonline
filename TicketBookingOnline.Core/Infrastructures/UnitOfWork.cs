﻿using System.Threading.Tasks;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Repositories;

namespace TicketBookingOnline.Core.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private ICanhBaoRepository _canhBaoRepository;
        private ICommentRepository _commentRepository;
        private ICumRapRepository _cumRapRepository;       
        private ILichChieuRepository _lichChieuRepository;
        private ILoaiGheRepository _loaiGheRepository;        
        private IPhimRepository _phimRepository;
        private IPhimTheLoaiRepository _phimTheLoaiRepository;        
        private IRapRepository _rapRepository;
        private IRapGheRepository _rapgheRepository;
        private IThanhPhoRepository _thanhPhoRepository;
        private ITheLoaiRepository _theLoaiRepository;
        private IVeRepository _veRepository;
        private IKhachHangRepository _khachHangRepository;
        private ICGVManagerRepository _cGVManagerRepository;
        private ISliderRepository _sliderRepository;

        private readonly TicketBookingOnlineDBContext _context;

        public UnitOfWork(TicketBookingOnlineDBContext context)
        {
            _context = context;
        }


        public ICanhBaoRepository CanhBaoRepository => _canhBaoRepository ?? (_canhBaoRepository = new CanhBaoRepository(_context));
        public ICommentRepository CommentRepository => _commentRepository ?? (_commentRepository = new CommentRepository(_context));
        public ICumRapRepository CumRapRepository => _cumRapRepository ?? (_cumRapRepository = new CumRapRepository(_context));
        public ILichChieuRepository LichChieuRepository => _lichChieuRepository ?? (_lichChieuRepository = new LichChieuRepository(_context));
        public ILoaiGheRepository LoaiGheRepository => _loaiGheRepository ?? (_loaiGheRepository = new LoaiGheRepository(_context));
        public IPhimRepository PhimRepository => _phimRepository ?? (_phimRepository = new PhimRepository(_context));
        public IPhimTheLoaiRepository PhimTheLoaiRepository => _phimTheLoaiRepository ?? (_phimTheLoaiRepository = new PhimTheLoaiRepository(_context));
        public IRapRepository RapRepository => _rapRepository ?? (_rapRepository = new RapRepository(_context));
        public IRapGheRepository RapGheRepository => _rapgheRepository ?? (_rapgheRepository = new RapGheRepository(_context));
        public IThanhPhoRepository ThanhPhoRepository => _thanhPhoRepository ?? (_thanhPhoRepository = new ThanhPhoRepository(_context));
        public ITheLoaiRepository TheLoaiRepository => _theLoaiRepository ?? (_theLoaiRepository = new TheLoaiRepository(_context));
        public IVeRepository VeRepository => _veRepository ?? (_veRepository = new VeRepository(_context));
        public IKhachHangRepository KhachHangRepository => _khachHangRepository ?? (_khachHangRepository = new KhachHangRepository(_context));
        public ICGVManagerRepository CGVManagerRepository => _cGVManagerRepository ?? (_cGVManagerRepository = new CGVManagerRepository(_context));
        public ISliderRepository SliderRepository => _sliderRepository ?? (_sliderRepository = new SliderRepository(_context));
        public TicketBookingOnlineDBContext TicketBookingOnlineContext => _context;

        public void Dispose()
        {
            _context.Dispose();
        }
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}

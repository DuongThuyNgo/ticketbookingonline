﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketBookingOnline.Core.IRepositories;

namespace TicketBookingOnline.Core.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        ICanhBaoRepository CanhBaoRepository { get; }
        ICommentRepository CommentRepository { get; }
        ICumRapRepository CumRapRepository { get; }
        ILichChieuRepository LichChieuRepository { get; }
        ILoaiGheRepository LoaiGheRepository { get; }       
        IPhimRepository PhimRepository { get; }
        IPhimTheLoaiRepository PhimTheLoaiRepository { get; }       
        IRapRepository RapRepository { get; }
        IRapGheRepository RapGheRepository { get; }
        IThanhPhoRepository ThanhPhoRepository { get; }
        ITheLoaiRepository TheLoaiRepository { get; }
        IVeRepository VeRepository { get; }
        IKhachHangRepository KhachHangRepository { get; }
        ICGVManagerRepository CGVManagerRepository { get; }
        ISliderRepository SliderRepository { get; }
        TicketBookingOnlineDBContext TicketBookingOnlineContext { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}

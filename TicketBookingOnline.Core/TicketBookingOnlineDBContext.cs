﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.BaseEntities;
using TicketBookingOnline.Core.Configurations;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core
{
    public class TicketBookingOnlineDBContext : IdentityDbContext<CGVManager>
    {
        public TicketBookingOnlineDBContext()
        {
        }
        public TicketBookingOnlineDBContext(DbContextOptions<TicketBookingOnlineDBContext> options) : base(options)
        {
        }
        //public DbSet<CGVManager> CGVManagers { get; set; }

        public DbSet<CanhBao> CanhBaos { get; set; }

        public DbSet<CumRap> CumRaps { get; set; }
        
        public DbSet<LichChieu> LichChieus { get; set; }
        public DbSet<LoaiGhe> LoaiGhes { get; set; }
        public DbSet<RapGhe> RapGhes { get; set; }


        public DbSet<Phim> Phims { get; set; }
        
        public DbSet<Rap> Raps { get; set; }
        public DbSet<ThanhPho> ThanhPhos { get; set; }
        public DbSet<TheLoai> TheLoais { get; set; }
        public DbSet<KhachHang> KhachHangs { get; set; }
        public DbSet<Ve> Ves { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public DbSet<PhimTheLoai> PhimTheLoais { get; set; }
        public DbSet<RapPhim> RapPhims { get; set; }
        public DbSet<Slider> Sliders { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                string connectionString = "Server=.;Database=TicketBooking;Trusted_Connection=True; MultipleActiveResultSets = true";
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new RoleSeedConfiguration());
            modelBuilder.ApplyConfiguration(new UserSeedConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleSeedConfiguration());

            modelBuilder.Entity<ThanhPho>(tp =>
            {
                tp.HasKey(c => c.ThanhPhoId);

                tp.Property(c => c.TenThanhPho)
                        .HasColumnType("nvarchar(50)")
                        .IsRequired();
               
            });
            

            modelBuilder.Entity<TheLoai>(sp =>
            {

                sp.HasKey(c => c.TheLoaiId);

                sp.Property(c => c.TenTheLoai)
                        .HasColumnType("nvarchar(50)")
                        .IsRequired();

            });

            

            modelBuilder.Entity<LoaiGhe>(sp =>
            {

                sp.HasKey(c => c.LoaiGheId);

                sp.Property(c => c.TenLoaiGhe)
                        .HasColumnType("nvarchar(50)")
                        .IsRequired();

                
            });

            modelBuilder.Entity<CanhBao>(sp =>
            {

                sp.HasKey(c => c.CanhBaoId);

                sp.Property(c => c.TenCanhBao)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                sp.Property(c => c.Anh)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

            });

            modelBuilder.Entity<CumRap>(sp =>
            {

                sp.HasOne(p => p.ThanhPho)
                        .WithMany(c => c.CumRaps)
                        .HasForeignKey(p => p.ThanhPhoId);
                        
                sp.Property(c => c.TenRap)
                        .HasColumnType("nvarchar(50)")
                        .IsRequired();

                sp.Property(c => c.DiaChi)
                        .HasColumnType("nvarchar(500)")
                        .IsRequired();

                sp.Property(c => c.SoDienThoai)
                        .HasColumnType("nvarchar(11)").HasMaxLength(11)
                        .IsRequired();

            });

            modelBuilder.Entity<Rap>(sp =>
            {

                sp.HasOne(p => p.CumRap)
                        .WithMany(c => c.Raps)
                        .HasForeignKey(p => p.CumRapId);

                sp.Property(c => c.LoaiRap)
                         .HasColumnType("nvarchar(50)")
                         .IsRequired();


                sp.Property(c => c.TenRap)
                        .HasColumnType("nvarchar(50)")
                        .IsRequired();
  

            });

            modelBuilder.Entity<Phim>(sp =>
            {
                             
                sp.Property(c => c.TenPhim)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                sp.Property(c => c.ThoiLuong)
                        .HasColumnType("int")
                        .IsRequired();

                sp.Property(c => c.Anh)
                        .HasColumnType("nvarchar(255)");

                sp.Property(c => c.Banner)
                        .HasColumnType("nvarchar(255)");

                sp.Property(c => c.Trailer)
                        .HasColumnType("nvarchar(255)");

                sp.Property(c => c.MoTa)
                        .HasColumnType("nvarchar(max)")
                        .IsRequired();

                sp.Property(c => c.KhoiChieu)
                        .HasColumnType("datetime")
                        .IsRequired();

                sp.Property(c => c.NgonNgu)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                sp.Property(c => c.DaoDien)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                sp.Property(c => c.DienVien)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                sp.HasOne(p => p.CanhBao)
                        .WithMany(c => c.Phims)
                        .HasForeignKey(p => p.CanhBaoId);
                        

                sp.Property(c => c.TrangThai)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                //sp.Property(c => c.TotalRate)
                //        .HasMaxLength(5).HasDefaultValue(0);

                //sp.Property(c => c.KhuyenMai)
                //        .HasDefaultValue(0);

                sp.Property(c => c.TotalRate)
                        .HasColumnType("float")
                        .HasMaxLength(5).HasDefaultValue(0)
                        .IsRequired();

                sp.Property(c => c.GiaVe)
                        .HasColumnType("decimal")
                        .HasDefaultValue(0)
                        .IsRequired();

                sp.Property(c => c.KhuyenMai)
                        .HasColumnType("float")
                        .HasDefaultValue(0)
                        .IsRequired();

            });

            modelBuilder.Entity<LichChieu>(sp =>
            {

                sp.Property(c => c.RapId)
                        .HasColumnType("int")
                        .IsRequired();

                sp.Property(c => c.XuatChieu)
                        .HasColumnType("datetime")
                        .IsRequired();

                sp.Property(c => c.PhanTramKM)
                        .HasColumnType("int")
                        .IsRequired();
            });

            modelBuilder.Entity<Comment>(sp =>
            {
                sp.HasOne(p => p.Phim)
                       .WithMany(c => c.Comments)
                       .HasForeignKey(p => p.PhimId);
                       

                sp.HasOne(p => p.User)
                       .WithMany(c => c.Comments)
                       .HasForeignKey(p => p.UserId);
                       


                sp.Property(c => c.ThoiGian)
                        .HasColumnType("datetime")
                        .IsRequired();

                sp.Property(c => c.Rate)
                        .HasColumnType("int")
                        .HasMaxLength(5).HasDefaultValue(0)
                        .IsRequired();

                sp.Property(c => c.NoiDung)
                        .HasColumnType("nvarchar(1000)")
                        .IsRequired();
            });

            modelBuilder.Entity<KhachHang>(sp =>
            {
                sp.HasKey(c => c.UserId);

                sp.Property(c => c.Password)
                        .HasColumnType("nvarchar(50)")
                        .IsRequired();

                sp.Property(c => c.HoTen)
                        .HasColumnType("nvarchar(100)")
                        .IsRequired();

                sp.Property(c => c.DiemThuong)
                        .HasColumnType("int")
                        .IsRequired();

                sp.Property(c => c.DiaChi)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                sp.Property(c => c.SoDienThoai)
                        .HasColumnType("nvarchar(11)")
                        .IsRequired();

                sp.Property(c => c.Email)
                        .HasColumnType("nvarchar(50)")
                        .IsRequired();

                sp.Property(c => c.Anh)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                sp.Property(c => c.Role)
                        .HasColumnType("nvarchar(50)")
                        .IsRequired();
            });

            modelBuilder.Entity<Ve>(sp =>
            {
              
                sp.HasOne(p => p.LichChieu)
                       .WithMany(c => c.Ves)
                       .HasForeignKey(p => p.LichChieuId);

                sp.HasOne(p => p.RapGhe)
                       .WithMany(c => c.Ves)
                       .HasForeignKey(p => new { p.RapId, p.LoaiGheId });

                sp.HasOne(p => p.User)
                       .WithMany(c => c.Ves)
                       .HasForeignKey(p => p.UserId);

                sp.Property(c => c.SoLuongGheNgoi)
                        .HasColumnType("int")
                        .HasMaxLength(5).HasDefaultValue(0)
                        .IsRequired();

                sp.Property(c => c.TongGiaVe)
                        .HasColumnType("decimal")
                        .IsRequired();

                
                sp.Property(c => c.ViTri)
                        .HasColumnType("nvarchar(100)")
                        .IsRequired();

                sp.Property(c => c.TrangThai)
                        .HasColumnType("nvarchar(255)")
                        .IsRequired();

                sp.Property(c => c.NgayDatVe)
                        .HasColumnType("datetime")
                        .IsRequired();

            });

            modelBuilder.Entity<PhimTheLoai>(sp =>
            {
                sp.HasKey(pc => new { pc.PhimId, pc.TheLoaiId });
                sp.ToTable("PhimTheLoai");
            });

            modelBuilder.Entity<RapPhim>(sp =>
            {
                sp.HasKey(pc => new { pc.RapId, pc.PhimId });
                sp.ToTable("RapPhim");
            });

            modelBuilder.Entity<RapGhe>(sp =>
            {
                sp.HasKey(pc => new { pc.RapId, pc.LoaiGheId });
                sp.ToTable("RapGhe");
            });

            modelBuilder.Seed();
        }
        public override int SaveChanges()
        {
            BeforSaveChanges();
            return base.SaveChanges();
        }

        private void BeforSaveChanges()
        {
            var entities = this.ChangeTracker.Entries();
            foreach (var entity in entities)
            {

                if (entity.Entity is BaseEntity baseEntity)
                {
                    var now = DateTime.Now;
                    switch (entity.State)
                    {
                        case EntityState.Modified:
                            baseEntity.UpdatedDate = now;
                            break;
                        case EntityState.Added:
                            baseEntity.CreatedDate = now;
                            baseEntity.UpdatedDate = now;
                            baseEntity.IsActive = true;
                            break;
                        case EntityState.Deleted:
                            baseEntity.IsActive= false;
                            break;
                    }
                }

                if (entity.Entity is Phim phim)
                {
                    int countRate = Comments.Where(c => c.PhimId == phim.PhimId).Count();
                    int sumRate = Comments.Where(c => c.PhimId == phim.PhimId).Select(c => c.Rate).Sum();
                    if(countRate != 0)
                    {
                        switch (entity.State)
                        {
                            case EntityState.Modified:
                                float totalRateModified = sumRate / countRate;
                                phim.TotalRate = totalRateModified;
                                break;
                            case EntityState.Added:
                                float totalRateAdded = sumRate / countRate;
                                phim.TotalRate = totalRateAdded;
                                break;
                        }
                    }
                }
             }
        }
    }
}

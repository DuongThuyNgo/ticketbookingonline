﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TicketBookingOnline.Core.Migrations
{
    public partial class UpdateDataBaseSeedData1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                column: "ConcurrencyStamp",
                value: "b36309f9-68af-4471-bbe8-42a9c1303e0b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038a11",
                column: "ConcurrencyStamp",
                value: "73c54837-27f4-4e70-8028-84c2796ddbd6");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038d8b",
                column: "ConcurrencyStamp",
                value: "34a6e639-3787-4005-968b-ded96632dfcf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f2049908-8a39-4236-bbcc-109f03ced012",
                column: "ConcurrencyStamp",
                value: "572e4101-9408-479d-b733-8b862ab618bb");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "70dc1302-cf04-4f4e-83d7-da3eadd87111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1f5ef395-b3fe-4789-862a-722098354011", "AQAAAAEAACcQAAAAEBeqP+qP1BeK+daviFdcP54R+wnXHK/wgcOf0LSbGbw0cKBy9sjNkodyKopI4V4SkA==", "d1aa39a8-6815-43ec-8242-661f7b86e430" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a27fa5f7-a648-4570-bb86-0a9acee2b111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fcba2fc0-5161-43ba-8f60-26809645accf", "AQAAAAEAACcQAAAAEGKz1BQEgz4ip4aePSXI01h+yYgYQXID0cbCx3yLZSS/Dc5608hetQieb/n6WgYKrw==", "aa066cff-22ea-4deb-ae7f-0c36c25588b0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a52f4105-7ff7-4aa7-a105-c5a1287491c2", "AQAAAAEAACcQAAAAEG98Y5a/dAqSmzqWawXNAs5ARov6mUlfXbipFB7te28x3MCMiLWtEqBd/WeRCDRafQ==", "8388530f-0581-4b76-877e-96514df15261" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083222",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "69be8d05-6fd2-4298-b855-ef22c9cdd1c9", "AQAAAAEAACcQAAAAEKpbrjpp+8g6ItydYua8K2pJcB7mfyMux2a3PDyzS9mzPusr6f2zvYl1tGkDYzI2RQ==", "3dee0348-8a1b-4889-99de-794dc986d21a" });

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 1,
                column: "XuatChieu",
                value: new DateTime(2022, 5, 2, 8, 30, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 2,
                column: "XuatChieu",
                value: new DateTime(2022, 5, 2, 9, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 3,
                columns: new[] { "PhimId", "XuatChieu" },
                values: new object[] { 3, new DateTime(2022, 5, 2, 9, 30, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 4,
                columns: new[] { "PhimId", "RapId", "XuatChieu" },
                values: new object[] { 4, 1, new DateTime(2022, 5, 3, 10, 30, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 5,
                columns: new[] { "PhimId", "RapId", "XuatChieu" },
                values: new object[] { 5, 1, new DateTime(2022, 5, 3, 11, 30, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Slider",
                columns: new[] { "SlideId", "Anh", "CreatedDate", "IsActive", "Ten", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, "Lazada.jpg", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), true, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, "Zalo2.jpg", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), true, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4, "Zalo1.jpg", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), true, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, "NgheSieuDe.jpg", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), true, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, "Music.jpg", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), true, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "Ve",
                columns: new[] { "VeId", "CreatedDate", "IsActive", "LichChieuId", "LoaiGheId", "NgayDatVe", "Note", "RapId", "SoLuongGheNgoi", "TongGiaVe", "TrangThai", "UpdatedDate", "UserId", "ViTri" },
                values: new object[,]
                {
                    { 8, new DateTime(2022, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 1, 1, new DateTime(2022, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 2, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "" },
                    { 11, new DateTime(2022, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 5, 1, new DateTime(2022, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 2, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "" },
                    { 12, new DateTime(2022, 5, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 1, 2, new DateTime(2022, 5, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 2, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "" },
                    { 7, new DateTime(2022, 5, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 2, 3, new DateTime(2022, 5, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 1, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "" },
                    { 6, new DateTime(2022, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 1, 2, new DateTime(2022, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 2, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "" },
                    { 5, new DateTime(2022, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 5, 1, new DateTime(2022, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 3, 1, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "" },
                    { 4, new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 4, 1, new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 2, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "" },
                    { 3, new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 3, 2, new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 2, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "" },
                    { 2, new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 2, 1, new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 1, 150000m, "Chưa xác nhận", new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "" },
                    { 1, new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 1, 2, new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 2, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "A1, A2" },
                    { 9, new DateTime(2022, 5, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 3, 3, new DateTime(2022, 5, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 1, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "" },
                    { 10, new DateTime(2022, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), true, 4, 1, new DateTime(2022, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "", 1, 2, 200000m, "Chưa xác nhận", new DateTime(2022, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Slider",
                keyColumn: "SlideId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Slider",
                keyColumn: "SlideId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Slider",
                keyColumn: "SlideId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Slider",
                keyColumn: "SlideId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Slider",
                keyColumn: "SlideId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Ve",
                keyColumn: "VeId",
                keyValue: 12);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                column: "ConcurrencyStamp",
                value: "685e0b63-23ec-4bc3-80e1-d8579882759d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038a11",
                column: "ConcurrencyStamp",
                value: "0d115c4f-6809-489d-8c6a-96314abf139a");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038d8b",
                column: "ConcurrencyStamp",
                value: "d25b1a3e-470a-499f-9031-623dcb104bdf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f2049908-8a39-4236-bbcc-109f03ced012",
                column: "ConcurrencyStamp",
                value: "17692c7b-fb72-4200-b1a8-432d611775c0");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "70dc1302-cf04-4f4e-83d7-da3eadd87111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "27fdaca9-da42-4345-b4ba-6e878b5553ff", "AQAAAAEAACcQAAAAEDsCT5eSRWbuj/+KaNkIF8+C68pGGKi9ayPquylwQKSGoZ47vW31J61Q3nyfn1X4JQ==", "cec2454e-ebc7-42a4-886f-cb7867569283" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a27fa5f7-a648-4570-bb86-0a9acee2b111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2a2ec586-70bd-424a-ad17-f3cbac29a177", "AQAAAAEAACcQAAAAEEUYx+p1E80YKL5V81E38zYwSrUP+dpRrRiqB2FX//Ag7HcZiJdmdX3fL59smOFhHw==", "8f3750f8-e941-4a6a-9484-40d912471b4c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a8130999-7f54-4bba-bf83-0e02b6e2debe", "AQAAAAEAACcQAAAAEPvViHKb7t528x4lrPI/XOdc2vZQqCBBtUtfE9qPSPVvJs4scGTlYsS2rdqfLL7GVw==", "83c60a1b-af8b-4f67-a9c3-82f0edc7dfc9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083222",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b2c9e690-d58f-4d18-9acd-177a19038749", "AQAAAAEAACcQAAAAEEfZyHOy0WD7hrrmMVRwv4rpp+ExUhjSDpqWxUOAmY0AI1AJNr/CY9HgHsDeuVP7Gg==", "c4839bac-204b-481f-940c-05a83485673e" });

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 1,
                column: "XuatChieu",
                value: new DateTime(2022, 3, 1, 8, 30, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 2,
                column: "XuatChieu",
                value: new DateTime(2022, 3, 1, 9, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 3,
                columns: new[] { "PhimId", "XuatChieu" },
                values: new object[] { 1, new DateTime(2022, 3, 1, 9, 30, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 4,
                columns: new[] { "PhimId", "RapId", "XuatChieu" },
                values: new object[] { 3, 2, new DateTime(2022, 3, 1, 10, 30, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "LichChieu",
                keyColumn: "LichChieuId",
                keyValue: 5,
                columns: new[] { "PhimId", "RapId", "XuatChieu" },
                values: new object[] { 1, 2, new DateTime(2022, 3, 1, 11, 30, 0, 0, DateTimeKind.Unspecified) });
        }
    }
}

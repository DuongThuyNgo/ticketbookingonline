﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Configurations
{
    public class UserSeedConfiguration : IEntityTypeConfiguration<CGVManager>
    {
        public void Configure(EntityTypeBuilder<CGVManager> builder)
        {
            var hasher = new PasswordHasher<CGVManager>();
            builder.HasData(
                //Admin
                new CGVManager
                {
                    Id = "70dc1302-cf04-4f4e-83d7-da3eadd87111",
                    Email = "lethithanhmy@gmail.com",
                    NormalizedEmail = "LETHITHANHMY@GMAIL.COM",
                    NormalizedUserName = "LETHITHANHMY",
                    UserName = "Lê Thị Thanh Mỹ",
                    PasswordHash = hasher.HashPassword(null, "lethithanhmy"),
                    EmailConfirmed = true,
                    CumRapId = 0,
                    IsActive = true
                },
                //Admin
                new CGVManager
                {
                    Id = "a27fa5f7-a648-4570-bb86-0a9acee2b111",
                    Email = "ngothuyduong@gmail.com",
                    NormalizedEmail = "NGOTHUYDUONG@GMAIL.COM",
                    NormalizedUserName = "NGOTHUYDUONG",
                    UserName = "Ngô Thùy Dương",
                    PasswordHash = hasher.HashPassword(null, "ngothuyduong"),
                    EmailConfirmed = true,
                    CumRapId = 0,
                    IsActive = true
                },
                //Manager
                new CGVManager
                {
                    Id = "c554fd29-f07b-4d28-aea0-815692083111",
                    Email = "tranthuhuyen@gmail.com",
                    NormalizedEmail = "TRANTHUHUYEN@GMAIL.COM",
                    NormalizedUserName = "TRANTHUHUYEN",
                    UserName = "Trần Thu Huyền",
                    PasswordHash = hasher.HashPassword(null, "tranthuhuyen"),
                    EmailConfirmed = true,
                    CumRapId = 1,
                    IsActive = true
                },
                //Staff
                new CGVManager
                {
                    Id = "c554fd29-f07b-4d28-aea0-815692083222",
                    Email = "nguyenthiyen@gmail.com",
                    NormalizedEmail = "NGUYENTHIYEN@GMAIL.COM",
                    NormalizedUserName = "NGUYENTHIYEN",
                    UserName = "Nguyễn Thị Yến",
                    PasswordHash = hasher.HashPassword(null, "nguyenthiyen"),
                    EmailConfirmed = true,
                    CumRapId = 1,
                    IsActive = true
                }
            );
        }
    }
}

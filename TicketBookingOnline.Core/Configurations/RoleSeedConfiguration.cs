﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace TicketBookingOnline.Core.Configurations
{
    public class RoleSeedConfiguration : IEntityTypeConfiguration<IdentityRole>
    {
        public void Configure(EntityTypeBuilder<IdentityRole> builder)
        {
            builder.HasData(
                new IdentityRole
                {
                    Id = "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new IdentityRole
                {
                    Id = "1f45ab0a-c177-42da-b07d-671387038d8b",
                    Name = "Manager",
                    NormalizedName = "MANAGER"
                },
                new IdentityRole
                {
                    Id = "1f45ab0a-c177-42da-b07d-671387038a11",
                    Name = "Staff",
                    NormalizedName = "STAFF"
                },
                new IdentityRole
                {
                    Id = "f2049908-8a39-4236-bbcc-109f03ced012",
                    Name = "User",
                    NormalizedName = "USER"
                }
            ); 

        }
    }
}

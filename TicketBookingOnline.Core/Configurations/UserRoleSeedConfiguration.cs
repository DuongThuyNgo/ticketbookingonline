﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TicketBookingOnline.Core.Configurations
{
    public class UserRoleSeedConfiguration : IEntityTypeConfiguration<IdentityUserRole<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserRole<string>> builder)
        {
            builder.HasData(
               new IdentityUserRole<string>
               {
                   RoleId = "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                   UserId = "70dc1302-cf04-4f4e-83d7-da3eadd87111",
               },
               new IdentityUserRole<string>
               {
                   RoleId = "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                   UserId = "a27fa5f7-a648-4570-bb86-0a9acee2b111",
               },
               new IdentityUserRole<string>
               {
                   RoleId = "1f45ab0a-c177-42da-b07d-671387038d8b",
                   UserId = "c554fd29-f07b-4d28-aea0-815692083111",
               },
               new IdentityUserRole<string>
               {
                   RoleId = "1f45ab0a-c177-42da-b07d-671387038a11",
                   UserId = "c554fd29-f07b-4d28-aea0-815692083222",
               }
           );
        }
    }
}

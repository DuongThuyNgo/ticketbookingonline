﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;


namespace TicketBookingOnline.Core.Repositories
{
    public class PhimRepository : BaseRepository<Phim>, IPhimRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public PhimRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<Phim> GetPhimByTrangThai(string trangThai)
        {
            var phims = _context.Phims.Include(x=> x.CanhBao).Where(p => p.TrangThai == trangThai).OrderByDescending(p=>p.UpdatedDate);
            return phims;
        }

        public IEnumerable<Phim> SearchPhim(string tenPhim)
        {
            return _context.Phims.Include(x => x.CanhBao).Where(p => p.TenPhim.Contains(tenPhim)).OrderByDescending(p => p.UpdatedDate);
        }

        public IEnumerable<Phim> GetPhimByTheLoai(int maTheLoai)
        {
            return _context.Phims.Include(x => x.CanhBao).Where(p => p.PhimTheLoais.Any(t => t.TheLoaiId == maTheLoai)).OrderByDescending(p => p.UpdatedDate);           

        }

        public IEnumerable<Phim> GetPhimByTheLoais(List<int> maTheLoai)
        {
            return _context.Phims.Include(x => x.CanhBao).Where(p => p.PhimTheLoais.Any(t => maTheLoai.Contains(t.TheLoaiId))).OrderByDescending(p => p.UpdatedDate);
        }

        
        public string TheLoaiOfPhim(int phimId)
        {
            var phimTheLoai = _context.PhimTheLoais.Join(_context.TheLoais, a => a.TheLoaiId, b => b.TheLoaiId,
                (a, b) => new
                {
                    PhimId = a.PhimId,
                    TenTheLoai = b.TenTheLoai
                }).Where(p => p.PhimId == phimId).ToList();

            string theLoai = "";
            if(phimTheLoai.Count()==0)
            {
                return theLoai;
            }
            foreach( var item in phimTheLoai)
            {
                theLoai += item.TenTheLoai + ", ";
            }
            theLoai = theLoai.Substring(0, theLoai.Length - 2);
            return theLoai;
        }

        public IEnumerable<Phim> Filter(int? thanhPhoId, int? cumRapId, DateTime? ngayChieu)
        {
            var phims = _context.Phims.Include(x => x.CanhBao).AsQueryable();
            if (thanhPhoId != null)
            {
                if (cumRapId != null)
                {
                    List<int> rapIds = new List<int>();
                    var raps = _context.Raps.Where(rap => rap.CumRapId == cumRapId).ToList();
                    foreach (var rap in raps)
                    {
                        rapIds.Add(rap.RapId);
                    }
                    phims = phims.Where(p => p.RapPhims.Any(x => rapIds.Contains(x.RapId))).OrderByDescending(p => p.UpdatedDate);
                }
                else
                {
                    var cumRaps = _context.CumRaps.Where(cr => cr.ThanhPhoId == thanhPhoId).ToList();
                    List<int> rapIds = new List<int>();
                    foreach (var item in cumRaps)
                    {
                        var raps = _context.Raps.Where(rap => rap.CumRapId == item.CumRapId);
                        foreach (var rap in raps)
                        {
                            rapIds.Add(rap.RapId);
                        }
                    }
                    phims = phims.Where(p => p.RapPhims.Any(x => rapIds.Contains(x.RapId))).OrderByDescending(p => p.UpdatedDate);
                }
            }
            if (ngayChieu.HasValue)
            {
                List<int> lichChieuIds = new List<int>();
                var lichChieus = _context.LichChieus.Where(lc => lc.XuatChieu.Day == ngayChieu.Value.Day).ToList();
                foreach (var item in lichChieus)
                {
                    lichChieuIds.Add(item.LichChieuId);
                }
                phims = phims.Where(p => p.LichChieus.Any(x => lichChieuIds.Contains(x.LichChieuId))).OrderByDescending(p => p.UpdatedDate);
            }
            return phims;
        }

        public Phim GetPhim(int phimId)
        {
            return _context.Phims.Include(x => x.CanhBao).FirstOrDefault(x => x.PhimId == phimId);
        }

        public Phim GetPhimByAnh(string anh)
        {
            return _context.Phims.Include(x => x.CanhBao).FirstOrDefault(x => x.Anh == anh);
        }

        public Phim GetLastestPhim()
        {
            return _context.Phims.OrderBy(x => x.PhimId).LastOrDefault();
        }

        public bool DeletePhim(int phimId)
        {
            var phim = _context.Phims.Where(x => x.PhimId == phimId).FirstOrDefault();
            if (phim != null)
            {
                phim.IsActive = false;
                phim.TrangThai = "DaChieu";
                _context.Phims.Update(phim);
                return true;
            }
            return false;
        }

        public IEnumerable<Phim> GetAllPhimIsActive()
        {
            return _context.Phims.Include(x => x.CanhBao).Where(p => p.IsActive == true).OrderByDescending(p => p.UpdatedDate);
        }

        public IEnumerable<Phim> GetPhimByTrangThaiWithRowperPage(string trangThai, int index)
        {
            int rowPerPage = 8;
            var phims = _context.Phims.Include(x => x.CanhBao).Where(p => p.TrangThai == trangThai).OrderByDescending(p => p.UpdatedDate).Skip((index - 1) * rowPerPage).Take(rowPerPage).ToList();
            return phims;
        }

        public IEnumerable<Phim> SearchPhimWithRowperPage(string tenPhim, int index)
        {
            int rowPerPage = 8;
            return _context.Phims.Include(x => x.CanhBao).Where(p => p.TenPhim.Contains(tenPhim)).OrderByDescending(p => p.UpdatedDate).Skip((index - 1) * rowPerPage).Take(rowPerPage).ToList();
        }

        public IEnumerable<Phim> GetAllPhimIsActiveWithRowperPage(int index)
        {
            int rowPerPage = 8;
            return _context.Phims.Include(x => x.CanhBao).Where(p => p.IsActive == true).OrderByDescending(p => p.UpdatedDate).Skip((index - 1) * rowPerPage).Take(rowPerPage).ToList();
        }

        public IEnumerable<Phim> GetPhimByTheLoaiRowPerPage(int maTheLoai, int index)
        {
            int rowPerPage = 9;
            return _context.Phims.Include(x => x.CanhBao).Where(p => p.PhimTheLoais.Any(t => t.TheLoaiId == maTheLoai)).OrderByDescending(p => p.UpdatedDate).Skip((index - 1) * rowPerPage).Take(rowPerPage).ToList();
        }

        public int CountPhimByTheLoai(int maTheLoai)
        {
            return _context.Phims.Where(p => p.PhimTheLoais.Any(t => t.TheLoaiId == maTheLoai)).Count();
        }

        public IEnumerable<Phim> PhimHot()
        {
            //Lấy ra tất cả các vé trong 7 ngày gần đây
            var day = DateTime.Now.AddDays(-1);
            var ves = _context.Ves.Include(x => x.LichChieu).Where(x => x.LichChieu.XuatChieu.Date.Equals(day.Date)).ToList();
            for (int i = -2; i >= -7; i--)
            {
                var addVes = _context.Ves.Include(x => x.LichChieu).Where(x => x.LichChieu.XuatChieu.Date.Equals(day.AddDays(i).Date)).ToList();
                ves.AddRange(addVes);               
            }
            if (ves.Count()!=0)
            {
                var result = ves.GroupBy(x => x.LichChieu.PhimId).Select(x => new
                {
                    PhimId = x.Key,
                    Sum = x.Sum(x => x.SoLuongGheNgoi)
                }).OrderByDescending(a => a.Sum).Take(4).ToList();

                var listPhimId = result.Select(y => y.PhimId).ToList();
                var phims = _context.Phims.Include(x => x.CanhBao).Where(x => listPhimId.Contains(x.PhimId)).ToList();
                int count = phims.Count();
                if ( count != 4) {
                    var listAdd = _context.Phims.Include(x => x.CanhBao).OrderByDescending(x => x.UpdatedDate).Take(4).ToList();
                    int add = 4 - count;
                    for(int i= 0;i< add; i++)
                    {
                        phims.Add(listAdd[i]);
                    }
                }
            }            
            return _context.Phims.Include(x=>x.CanhBao).OrderByDescending(x=>x.UpdatedDate).Take(4);
        }

        public IEnumerable<Phim> PhimCungTheLoai(int phimId)
        {
            var theLoais = _context.PhimTheLoais.Where(x => x.PhimId == phimId);
            var listTheLoaiId = theLoais.Select(x => x.TheLoaiId).ToList();
            var result = GetPhimByTheLoais(listTheLoaiId);
            result = result.Where( x => x.PhimId != phimId && x.TrangThai == "DangChieu").Take(3);
            return result;
        }

        public IEnumerable<Phim> GetPhimByTheLoaisWithRowPerPage(List<int> maTheLoai, int index, int rowPerPage)
        {
            return _context.Phims.Include(x => x.CanhBao).Where(p => p.PhimTheLoais.Any(t => maTheLoai.Contains(t.TheLoaiId)))
                .OrderByDescending(p => p.UpdatedDate).Skip((index - 1)*rowPerPage).Take(rowPerPage);
        }

        public int CountPhimByTheLoais(List<int> maTheLoai)
        {
            return _context.Phims.Where(p => p.PhimTheLoais.Any(t => maTheLoai.Contains(t.TheLoaiId))).Count();
        }
    }
}

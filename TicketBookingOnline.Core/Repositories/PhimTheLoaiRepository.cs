﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class PhimTheLoaiRepository : BaseRepository<PhimTheLoai>, IPhimTheLoaiRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public PhimTheLoaiRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<PhimTheLoai> GetPhimTheLoaiByPhimId(int phimId)
        {
            return _context.PhimTheLoais.Where(x => x.PhimId == phimId);
        }
    }
}

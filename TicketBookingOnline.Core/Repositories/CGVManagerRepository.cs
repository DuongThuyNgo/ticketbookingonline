﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class CGVManagerRepository : ICGVManagerRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public CGVManagerRepository(TicketBookingOnlineDBContext context)
        {
            _context = context;
        }

        public int CountAllNhanVienInCumRap(int cumRapId)
        {           
            var roleId = _context.Roles.Where(x => x.Name.Equals("Staff")).FirstOrDefault().Id;
            var users = _context.Users.Join(_context.UserRoles, a => a.Id, b => b.UserId, (a, b) => a)
                .Where(x=>x.CumRapId == cumRapId && x.IsActive == true);     
            return users.Count();
        }

        public void Delete(CGVManager cGVManager)
        {
            _context.Users.Remove(cGVManager);
        }

        public IEnumerable<CGVManager> GetNhanVienInCumRapWithRowPerPage(int cumRapId, int index)
        {
            int pageSize = 10;
            var roleId = _context.Roles.Where(x => x.Name.Equals("Staff")).FirstOrDefault().Id;
            var userRoles = _context.UserRoles.Where(x => x.RoleId == roleId).ToList();
            var listUserId = new List<string>();
            listUserId = userRoles.Select(x => x.UserId).ToList();
            var users = _context.Users.Where(x => listUserId.Contains(x.Id) && x.CumRapId == cumRapId && x.IsActive == true);
            return users;
        }

        public string RoleName(string userId)
        {
            var roleId = _context.UserRoles.Where(x => x.UserId.Equals(userId)).FirstOrDefault().RoleId;
            return _context.Roles.Where(x => x.Id.Equals(roleId)).FirstOrDefault().Name.ToString();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class LichChieuRepository : BaseRepository<LichChieu>, ILichChieuRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public LichChieuRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<LichChieu> Filter(int phimId, int? cumRapId, int? rapId, DateTime? ngayChieu)
        {
            var lichChieus = _context.LichChieus.Where(x => x.PhimId == phimId && x.XuatChieu.Date >= DateTime.Now.Date).AsQueryable();
            if(rapId != null)
            {
                lichChieus = lichChieus.Where(x => x.RapId == rapId);
            }
            else if(cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    lichChieus = lichChieus.Where(x => listRapIds.Contains(x.RapId));
                }
                else
                {
                    return new List<LichChieu>();
                }
            }
            var defaultDateTime = new DateTime(0001, 01, 01);
            if(ngayChieu.HasValue && ngayChieu != defaultDateTime)
            {
                lichChieus = lichChieus.Where(x => x.XuatChieu.Date == ngayChieu.Value.Date);
            }
            return lichChieus;
        }

        public IEnumerable<LichChieu> GetLichChieuByPhimId(int phimId)
        {
            return _context.LichChieus.Where(x => x.PhimId == phimId && x.XuatChieu.Date >= DateTime.Now.Date).ToList();
        }

        public IEnumerable<string> GetLichChieuInCumRap(int CumRapId)
        {
            var raps = _context.Raps.Where(x => x.CumRapId == CumRapId);
            if (raps.Count() > 0)
            {
                List<int> listRapIds = new List<int>();
                foreach (var item in raps)
                {
                    listRapIds.Add(item.RapId);
                }
                var lichchieus = _context.LichChieus.Where(x => listRapIds.Contains(x.RapId));
                var xuatchieus = lichchieus.Select(x => x.XuatChieu.ToShortDateString().ToString());
                return xuatchieus;
            }
            return null;
        }

        public IEnumerable<LichChieu> GetLichChieuOfPhimInCumRap(int phimId, int CumRapId)
        {
            var raps = _context.Raps.Where(x => x.CumRapId == CumRapId);
            if(raps.Count() > 0)
            {
                List<int> listRapIds = new List<int>();
                foreach (var item in raps)
                {
                    listRapIds.Add(item.RapId);
                }
                return _context.LichChieus.Where(x => x.PhimId == phimId && listRapIds.Contains(x.RapId) && x.XuatChieu.Date >= DateTime.Now.Date);
            }
            return null;
        }

        public bool IsLichChieuExists(int phimId, DateTime xuatChieu, int rapId)
        {
            int timeWait = 15;
            var lichChieu= _context.LichChieus.Include(x=>x.Phim).Where(x=>x.PhimId==phimId && x.RapId==rapId);
            if (lichChieu != null)
            {
                foreach (var item in lichChieu)
                {
                    if (item.XuatChieu <= xuatChieu && xuatChieu <= item.XuatChieu.AddMinutes(timeWait + item.Phim.ThoiLuong))
                        return true;
                }
            }
            return false;
        }
    }
}

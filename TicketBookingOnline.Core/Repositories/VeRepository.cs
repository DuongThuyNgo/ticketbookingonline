﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class VeRepository : BaseRepository<Ve>, IVeRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public VeRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public bool CancelVe(int veId, string note)
        {
            var ve = _context.Ves.Where(x => x.VeId == veId).FirstOrDefault();
            if (ve != null)
            {
                ve.Note = note;
                ve.TrangThai = "Đã hủy";
                ve.IsActive = false;
                _context.Update(ve);
                return true;
            }
            return false;
        }

        public bool ConfirmVe(int veId)
        {
            var ve = _context.Ves.Where(x => x.VeId == veId).FirstOrDefault();
            if (ve != null)
            {
                ve.TrangThai = "Đã xác nhận";
                ve.IsActive = false;
                _context.Update(ve);
                return true;
            }
            return false;
        }

        public int CountVeWithFilter(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName)
        {
            var ves = _context.Ves.Include(x => x.User).Include(x => x.LichChieu).Include(x => x.LichChieu.Phim)
                .Include(x => x.RapGhe.LoaiGhe).Where(x => x.IsActive == true);
            if (cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    ves = ves.Where(x => listRapIds.Contains(x.RapId));
                }
                else
                {
                    return 0;
                }
            }
            else if (rapId != null)
            {
                ves = ves.Where(x => x.RapId == rapId);
            }
            DateTime defaultDate = new DateTime(0001, 01, 01);
            if (ngayChieu.HasValue && ngayChieu != defaultDate)
            {
                ves = ves.Where(x => x.LichChieu.XuatChieu.Date == ngayChieu.Value.Date);
            }
            if (tenPhim != null)
            {
                ves = ves.Where(x => x.LichChieu.Phim.TenPhim.ToLower().Contains(tenPhim.ToLower()));
            }
            if (userName != null)
            {
                ves = ves.Where(x => x.User.HoTen.ToLower().Contains(userName.ToLower()));
            }
            return ves.Count();
        }

        public IEnumerable<Ve> Filter(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName)
        {
            var ves = _context.Ves.Include(x => x.User).Include(x => x.LichChieu).Include(x => x.LichChieu.Phim)
                .Include(x => x.RapGhe.LoaiGhe).Where(x => x.IsActive == true);
            if (cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    ves = ves.Where(x => listRapIds.Contains(x.RapId));
                }
                else
                {
                    return new List<Ve>();
                }
            }
            else if(rapId != null)
            {
                ves = ves.Where(x => x.RapId == rapId);
            }
            DateTime defaultDate = new DateTime(0001, 01, 01);
            if (ngayChieu.HasValue && ngayChieu != defaultDate)
            {
                ves = ves.Where(x => x.LichChieu.XuatChieu.Date == ngayChieu.Value.Date);
            }
            if(tenPhim != null)
            {
                ves = ves.Where(x => x.LichChieu.Phim.TenPhim.ToLower().Contains(tenPhim.ToLower()));
            }
            if(userName != null)
            {
                ves = ves.Where(x => x.User.HoTen.ToLower().Contains(userName.ToLower()));
            }
            var trrrrrrr = ves.AsQueryable().ToList();
            return ves;
        }

        public IEnumerable<Ve> GetAllIsActive()
        {
            return _context.Ves.Include(x => x.User).Include(x => x.LichChieu).Include(x => x.LichChieu.Phim)
                .Include(x => x.RapGhe.LoaiGhe).Where(x => x.IsActive == true);
        }

        public Ve GetDetailById(int veId)
        {
            return _context.Ves.Include(x => x.User).Include(x => x.LichChieu).Include(x => x.LichChieu.Phim)
                .Include(x => x.RapGhe.LoaiGhe).Where(x=>x.VeId == veId).FirstOrDefault();
        }

        public IEnumerable<Ve> GetVeByKhachHangId(int khachHangId)
        {
            return _context.Ves.Include(x => x.User).Include(x => x.LichChieu).Include(x => x.LichChieu.Phim)
                .Include(x => x.RapGhe.LoaiGhe).Where(x => x.UserId == khachHangId);
        }

        public IEnumerable<Ve> GetVeByNam(int? cumRapId, int nam)
        {
            var ves = _context.Ves.Include(x => x.LichChieu).Where(x => x.LichChieu.XuatChieu.Year == nam);
            if (cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    ves = ves.Where(x => listRapIds.Contains(x.RapId));
                }
                else
                {
                    return new List<Ve>();
                }
            }
            return ves;

        }

        public IEnumerable<Ve> GetVeByNgay(int? cumRapId, DateTime ngay)
        {
            var ves = _context.Ves.Include(x => x.LichChieu).Where(x => x.LichChieu.XuatChieu.Date == ngay.Date);
            if(cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                var ttttt = raps.ToList();
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    ves = ves.Where(x => listRapIds.Contains(x.RapId));
                }
                else
                {
                    return new List<Ve>();
                }
            }
            return ves;
        }

        public IEnumerable<Ve> GetVeByThang(int? cumRapId, int thang, int nam)
        {
            var ves = _context.Ves.Include(x => x.LichChieu).Where(x => x.LichChieu.XuatChieu.Month == thang
                        && x.LichChieu.XuatChieu.Year == nam);
            if (cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    ves = ves.Where(x => listRapIds.Contains(x.RapId));
                }
                else
                {
                    return new List<Ve>();
                }
            }
            return ves;
        }

        public IEnumerable<Ve> GetVeByTrangThai(int? cumRapId, string trangThai)
        {
            var ves = _context.Ves.Where(x => x.TrangThai == trangThai);
            if(cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    ves = ves.Where(x => listRapIds.Contains(x.RapId));
                }
                else
                {
                    return new List<Ve>();
                }
            }
            return ves;
        }

        public IEnumerable<Ve> GetVeByTuan(int ? cumRapId, DateTime ngay)
        {
            DateTime backUp = ngay;
            List<DateTime> daysOfWeek = new List<DateTime>();
            while (ngay.DayOfWeek.ToString() != "Monday")
            {
                daysOfWeek.Add(ngay);
                ngay = ngay.AddDays(-1);
            }
            daysOfWeek.Add(ngay);
            if (backUp.DayOfWeek.ToString() != "Sunday")
            {
                ngay = backUp.AddDays(1);
                while (ngay.DayOfWeek.ToString() != "Sunday")
                {
                    daysOfWeek.Add(ngay);
                    ngay = ngay.AddDays(1);
                }
                daysOfWeek.Add(ngay);
            }
            
            var ves = _context.Ves.Include(x => x.LichChieu).Where(x => daysOfWeek.Contains(x.LichChieu.XuatChieu.Date)).ToList();
            if (cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    ves = ves.Where(x => listRapIds.Contains(x.RapId)).ToList();
                }
                else
                {
                    return new List<Ve>();
                }
            }
            return ves.ToList();
        }

        public IEnumerable<Ve> VeWithRowPerPage(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, int index)
        {
            var ves = _context.Ves.Include(x => x.User).Include(x => x.LichChieu).Include(x => x.LichChieu.Phim)
                .Include(x => x.RapGhe.LoaiGhe).Where(x => x.IsActive == true);
            if (cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    ves = ves.Where(x => listRapIds.Contains(x.RapId));
                }
                else
                {
                    return new List<Ve>();
                }
            }
            else if (rapId != null)
            {
                ves = ves.Where(x => x.RapId == rapId);
            }
            DateTime defaultDate = new DateTime(0001, 01, 01);
            if (ngayChieu.HasValue && ngayChieu != defaultDate)
            {
                ves = ves.Where(x => x.LichChieu.XuatChieu.Date == ngayChieu.Value.Date);
            }
            if (tenPhim != null)
            {
                ves = ves.Where(x => x.LichChieu.Phim.TenPhim.ToLower().Contains(tenPhim.ToLower()));
            }
            if (userName != null)
            {
                ves = ves.Where(x => x.User.HoTen.ToLower().Contains(userName.ToLower()));
            }
            int rowPerPage = 20;
            return ves.Skip((index - 1)*rowPerPage).Take(rowPerPage);
        }
    }
}

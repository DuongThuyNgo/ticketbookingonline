﻿using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    
    public class KhachHangRepository :  BaseRepository<KhachHang>, IKhachHangRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public KhachHangRepository(TicketBookingOnlineDBContext context): base(context)
        {
            _context = context;
        }

        public bool CofirmKhachHang(int userId)
        {
            var khachHang = _context.KhachHangs.Where(x => x.UserId == userId).FirstOrDefault();
            if (khachHang != null)
            {
                khachHang.Confirm = true;
                _context.Update(khachHang);
                return true;
            }
            return false;
        }

        public int CountKhachHangFilter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email)
        {
            var khachHangs = _context.KhachHangs.Where(x => x.IsActive == true).AsQueryable();
            if (cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    var ves = _context.Ves.Where(x => listRapIds.Contains(x.RapId));
                    khachHangs = khachHangs.Join(ves, a => a.UserId, b => b.UserId, (a, b) => a).Distinct();
                }
            }
            if (ten != null)
            {
                khachHangs = khachHangs.Where(x => x.HoTen.ToLower().Contains(ten.ToLower()));
            }
            if (soDienThoai != null)
            {
                khachHangs = khachHangs.Where(x => x.SoDienThoai.Contains(soDienThoai));
            }
            if (diaChi != null)
            {
                khachHangs = khachHangs.Where(x => x.DiaChi.Contains(diaChi));
            }
            if (email != null)
            {
                khachHangs = khachHangs.Where(x => x.Email.Contains(email));
            }
            return khachHangs.Count();
        }

        public bool EmailIsExist(string email)
        {
            var khachHang = _context.KhachHangs.Where(x => x.Email.Equals(email)).FirstOrDefault();
            if(khachHang != null)
            {
                return true;
            }
            return false;
        }

        public IEnumerable<KhachHang> Filter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email)
        {
            var khachHangs = _context.KhachHangs.Where(x => x.IsActive == true).AsQueryable();
            if(cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    var ves = _context.Ves.Where(x=> listRapIds.Contains(x.RapId));
                    khachHangs = khachHangs.Join(ves, a => a.UserId, b => b.UserId, (a, b) => a).Distinct();
                }
            }
            if(ten != null)
            {
                khachHangs = khachHangs.Where(x => x.HoTen.ToLower().Contains(ten.ToLower()));
            }
            if (soDienThoai != null)
            {
                khachHangs = khachHangs.Where(x => x.SoDienThoai.Contains(soDienThoai));
            }
            if (diaChi != null)
            {
                khachHangs = khachHangs.Where(x => x.DiaChi.Contains(diaChi));
            }
            if (email != null)
            {
                khachHangs = khachHangs.Where(x => x.Email.Contains(email));
            }
            return khachHangs;
        }

        public IEnumerable<KhachHang> FilterWithRowperPage(int? cumRapId, string ten, string soDienThoai, string diaChi, string email, int index)
        {
            int rowPerPage = 20;
            var khachHangs = _context.KhachHangs.Where(x => x.IsActive == true).AsQueryable();
            if (cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    var ves = _context.Ves.Where(x => listRapIds.Contains(x.RapId));
                    khachHangs = khachHangs.Join(ves, a => a.UserId, b => b.UserId, (a, b) => a).Distinct();
                }
            }
            if (ten != null)
            {
                khachHangs = khachHangs.Where(x => x.HoTen.ToLower().Contains(ten.ToLower()));
            }
            if (soDienThoai != null)
            {
                khachHangs = khachHangs.Where(x => x.SoDienThoai.Contains(soDienThoai));
            }
            if (diaChi != null)
            {
                khachHangs = khachHangs.Where(x => x.DiaChi.Contains(diaChi));
            }
            if (email != null)
            {
                khachHangs = khachHangs.Where(x => x.Email.Contains(email));
            }
            return khachHangs.Skip((index - 1) * rowPerPage).Take(rowPerPage);
        }

        public IEnumerable<KhachHang> KhachHangMoi(int? cumRapId)
        {
            var khachHangs = _context.KhachHangs.Where(x => x.Confirm == false && x.IsActive == true);
            if(cumRapId != null)
            {
                var raps = _context.Raps.Where(x => x.CumRapId == cumRapId);
                if (raps.Count() > 0)
                {
                    List<int> listRapIds = new List<int>();
                    foreach (var item in raps)
                    {
                        listRapIds.Add(item.RapId);
                    }
                    var ves = _context.Ves.Where(x => listRapIds.Contains(x.RapId));
                    khachHangs = khachHangs.Join(ves, a => a.UserId, b => b.UserId, (a, b) => a).Distinct();
                }
            }
            return khachHangs;
        }

        public KhachHang Login(string email, string password)
        {
            return _context.KhachHangs.Where(x => x.Email.Equals(email) && x.Password.Equals(password) && x.IsActive == true).FirstOrDefault();           
        }
    }
}

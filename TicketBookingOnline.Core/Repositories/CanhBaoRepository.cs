﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class CanhBaoRepository : BaseRepository<CanhBao>, ICanhBaoRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public CanhBaoRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;


namespace TicketBookingOnline.Core.Repositories
{
    public class RapRepository : BaseRepository<Rap>, IRapRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public RapRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<Rap> GetRapByCumRapId(int cumRapId)
        {
            return _context.Raps.Where(x => x.CumRapId == cumRapId);
        }
    }
}

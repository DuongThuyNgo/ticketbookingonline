﻿using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class SliderRepository : BaseRepository<Slider>, ISliderRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public SliderRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public bool DeleteSlider(int sliderId)
        {
            Slider slider = _context.Sliders.Find(sliderId);
            if( slider == null)
                return false;
            slider.IsActive = false;
            return true;
        }
    }
}

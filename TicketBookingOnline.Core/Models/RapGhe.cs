﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    public class RapGhe : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int RapId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int LoaiGheId { get; set; }

        public decimal GiaGhe { get; set; }
        public int SoLuongGhe { get; set; }//22 ghe: A1->A10. 
        
        [ForeignKey("RapId")]
        public Rap Rap { get; set; }
        [ForeignKey("LoaiGheId")]
        public LoaiGhe LoaiGhe { get; set; }

        public ICollection<Ve> Ves { get; set; }

    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("ThanhPho")]
    public class ThanhPho : BaseEntity
    {
        [Key]
        public int ThanhPhoId { get; set; }
        public string TenThanhPho { get; set; }
        public ICollection<CumRap> CumRaps { get; set; }

    }
}

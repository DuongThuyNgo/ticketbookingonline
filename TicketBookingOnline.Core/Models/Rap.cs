﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core
{
    [Table("Rap")]
    public class Rap : BaseEntity
    {
        [Key]
        public int RapId { get; set; }
        public int CumRapId { get; set; }
        public string LoaiRap{ get; set; }
        public string TenRap { get; set; }
     

        [ForeignKey("CumRapId")]
        public CumRap CumRap { get; set; }
        public ICollection<RapPhim> RapPhims { get; set; }

       
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("Comment")]
    public class Comment : BaseEntity
    {
        [Key]
        public int CommentId { get; set; }
        public int UserId { get; set; }
        public int PhimId { get; set; }
        public string NoiDung { get; set; }
        public int Rate { get; set; }
        public DateTime ThoiGian { get; set; }
        public bool Confirm { get; set; }

        [ForeignKey("UserId")]
        public KhachHang User { get; set; }
        [ForeignKey("PhimId")]
        public Phim Phim { get; set; }

    }
}

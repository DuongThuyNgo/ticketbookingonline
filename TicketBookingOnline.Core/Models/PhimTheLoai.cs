﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    public class PhimTheLoai : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int PhimId { get; set; }

               
        //public string TheLoaiId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int TheLoaiId { get; set; }

        
        [ForeignKey("PhimId")]
        public Phim Phim { get; set; }
        [ForeignKey("TheLoaiId")]
        public TheLoai TheLoai { get; set; }
    }
}

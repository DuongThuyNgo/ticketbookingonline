﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("TheLoai")]
    public class TheLoai : BaseEntity
    {
        [Key]
        public int TheLoaiId { get; set; }
        public string TenTheLoai { get; set; }
        public ICollection<PhimTheLoai> PhimTheLoais { get; set; }
    }
}

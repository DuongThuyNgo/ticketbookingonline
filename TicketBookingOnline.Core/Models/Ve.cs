﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("Ve")]
    public class Ve : BaseEntity
    {
        [Key]
        public int VeId { get; set; }
        public int RapId { get; set; }
        public int LoaiGheId { get; set; }
        public int LichChieuId { get; set; }
        public int SoLuongGheNgoi { get; set; }
        public string ViTri { get; set; }
        public DateTime NgayDatVe { get; set; }
        public decimal TongGiaVe { get; set; }
        public string TrangThai { get; set; }
        public int UserId { get; set; }
        public string Note { get; set; }

        public RapGhe RapGhe { get; set; }
        public LichChieu LichChieu { get; set; }
        public KhachHang User { get; set; }
    }
}

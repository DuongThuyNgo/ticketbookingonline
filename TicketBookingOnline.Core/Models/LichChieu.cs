﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("LichChieu")]
    public class LichChieu : BaseEntity
    {
        [Key]
        public int LichChieuId { get; set; }

        public int PhimId { get; set; }

        public int RapId { get; set; }

        public DateTime XuatChieu { get; set; }

        public int PhanTramKM { get; set; }

        public ICollection<Ve> Ves { get; set; } 
        public Phim Phim { get; set; }

    }
}

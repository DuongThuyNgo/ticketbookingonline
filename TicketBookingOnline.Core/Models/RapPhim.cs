﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    public class RapPhim : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int RapId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int PhimId { get; set; }

        [ForeignKey("RapId")]
        public Rap Rap { get; set; }

        [ForeignKey("PhimId")]
        public Phim Phim { get; set; }
    }
}

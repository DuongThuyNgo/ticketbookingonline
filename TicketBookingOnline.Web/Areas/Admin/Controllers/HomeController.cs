﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController : BaseController
    {
        private readonly IThongKeServices _thongKeServices;
        private readonly IKhachHangServices _khachHangServices;
        private readonly ICommentServices _commentServices;

        public HomeController(IThongKeServices thongKeServices, IKhachHangServices khachHangServices,
            ICommentServices commentServices)
        {
            _thongKeServices = thongKeServices;
            _khachHangServices = khachHangServices;
            _commentServices = commentServices;
        }

        public async Task<IActionResult> Index(int? cumRapId)
        {
            var tt = HttpContext.Session.GetString("CumRapId");
            if (HttpContext.Session.GetString("CumRapId") != null)
            {
                cumRapId = Int32.Parse(HttpContext.Session.GetString("CumRapId"));
            }
            var thongKeDoanhThuTuans = await _thongKeServices.ThongKeDoanhThuTuanAsync(cumRapId, DateTime.Now
                , StaticDetails.ThongKeAPIPath + "ThongKeDoanhThuTuan");
            var doanhThuTuans = new List<decimal>();
            var ngays = new List<string>();
            foreach (var item in thongKeDoanhThuTuans)
            {
                ngays.Add(item.Ngay.ToString("dd/MM/yyyy"));
                doanhThuTuans.Add(item.DoanhThu);
            }
            ViewBag.Ngays = ngays;
            ViewBag.DoanhThuTuan = doanhThuTuans;

            var thongKeTrangThaiVes = await _thongKeServices.ThongKeTrangThaiVeAsync(cumRapId, DateTime.Now
                , StaticDetails.ThongKeAPIPath + "ThongKeTrangThaiVe");
            var trangThais = new List<string>();
            var soLuongs = new List<int>();
            foreach (var item in thongKeTrangThaiVes)
            {
                trangThais.Add(item.TrangThai);
                soLuongs.Add(item.SoLuong);
            }
            ViewBag.TrangThais = trangThais;
            ViewBag.SoLuongs = soLuongs;

            var dashBoard = await _thongKeServices.DashBoardAsync(cumRapId, StaticDetails.ThongKeAPIPath + "DashBoard");
            return View(dashBoard);
        }

        public async Task<IActionResult> KhachHangMoi(int? cumRapId)
        {
            if (HttpContext.Session.GetString("CumRapId") != null)
            {
                cumRapId = Int32.Parse(HttpContext.Session.GetString("CumRapId"));
            }
            var khachHangs = await _thongKeServices.GetKhachHangMoiAsync(cumRapId, StaticDetails.ThongKeAPIPath + "GetKhachHangMoi");
            return View(khachHangs);
        }

        public async Task<IActionResult> ConfirmKhachHang(int userId)
        {
            var response = await _khachHangServices.ConfirmKhachHang(userId, StaticDetails.KhachHangAPIPath + "ConfirmKhachHang");
            if (response)
            {
                TempData["Success"] = "Xác nhận thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction(nameof(KhachHangMoi));
        }

        public async Task<IActionResult> BinhLuanMoi()
        {
            var binhLuans = await _thongKeServices.GetBinhLuanMoiAsync(StaticDetails.ThongKeAPIPath + "GetBinhLuanMoi");
            return View(binhLuans);
        }

        public async Task<IActionResult> ConfirmBinhLuan(int commentId)
        {
            var response = await _commentServices.ConfirmComment(commentId, StaticDetails.CommentAPIPath + "ConfirmBinhLuan");
            if (response)
            {
                TempData["success"] = "Xác nhận thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction(nameof(BinhLuanMoi));
        }

    }
}

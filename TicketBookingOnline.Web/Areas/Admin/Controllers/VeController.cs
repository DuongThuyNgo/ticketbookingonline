﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.ViewModels.Ves;
using TicketBookingOnline.Web.Areas.Admin.Controllers;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.CGV.Controllers
{
    [Area("Admin")]
    public class VeController : BaseController
    {
        private readonly IVeServices _veServices;

        public VeController(IVeServices veServices)
        {
            _veServices = veServices;
        }

        public async Task<IActionResult> Index(int? rapId, DateTime ? ngayChieu, string tenPhim, string userName, int index = 1)
        {
            var defaultDate = new DateTime(0001, 01, 01);
            if (ngayChieu == defaultDate)
                ngayChieu = null;
            int cumRapId = int.Parse(HttpContext.Session.GetString("CumRapId"));
            var ves = await _veServices.VeWithRowPerPage(cumRapId, rapId, ngayChieu, tenPhim, userName, 
                StaticDetails.VeAPIPath + "VeWithPerRowPage", index);
            int count  = await _veServices.CountTotalVe (cumRapId, rapId, ngayChieu, tenPhim, userName,
                StaticDetails.VeAPIPath + "CountTotalVe");
            ViewBag.Page = index;
            ViewBag.MaxPage = (count / 10) - (count % 10 == 0 ? 1 : 0);
            ViewBag.RapId = rapId;
           
            ViewBag.TenPhim = tenPhim;
            ViewBag.UserName = userName;
            
            if(ngayChieu != null)
            {
                ViewData["NgayChieu"] = ngayChieu.Value.ToString("yyyy'-'MM'-'dd");
            }
            else
            {
                ViewData["NgayChieu"] = "dd/mm/yyyy";
            }
            return View(ves);
        }

        public async Task<IActionResult> Details(int veId)
        {
            var ve = await _veServices.GetByIdAsync(veId, StaticDetails.VeAPIPath + "GetById");
            return View(ve);
        }

        public async Task<IActionResult> Confirm(int veId)
        {
            var response = await _veServices.ConfirmVeAsync(veId, StaticDetails.VeAPIPath + "ConfirmVe");
            if (response)
            {
                TempData["Success"] = "Xác nhận vé thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi ...!";
            }
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Cancel(int veId, string note)
        {
            var veVM = new DeleteVeViewModel();
            veVM.VeId= veId;
            veVM.Note= note;
            var response = await _veServices.CancelVeAsync(veVM, StaticDetails.VeAPIPath + "CancelVe");
            if (response)
            {
                TempData["Success"] = "Hủy vé thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi ...!";
            }
            return RedirectToAction(nameof(Index));
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class KhachHangController : BaseController
    {
        private readonly IKhachHangServices _khachHangServices;
        private readonly IVeServices _veServices;

        public KhachHangController(IKhachHangServices khachHangServices, IVeServices veServices)
        {
            _khachHangServices = khachHangServices;
            _veServices = veServices;
        }

        public async Task<IActionResult> Index(int? cumRapId, string tenKhachHang, string soDienThoai, string diaChiKhachHang,
            string emailKhachHang, int index = 1)
        {
            if (HttpContext.Session.GetString("CumRapId") != null)
            {
                cumRapId = Int32.Parse(HttpContext.Session.GetString("CumRapId"));
            }

            var khachHangs = await _khachHangServices.FilterWithRowPerPageAsync(index, cumRapId, tenKhachHang, soDienThoai, diaChiKhachHang,
                emailKhachHang, StaticDetails.KhachHangAPIPath + "FilterWithRowPerPage");
            int count = 0;
            count = await _khachHangServices.CountKhachHangFilterAsync(cumRapId, tenKhachHang, soDienThoai, diaChiKhachHang,
                emailKhachHang, StaticDetails.KhachHangAPIPath + "CountKhachHangFilter");
            ViewBag.Page = index;
            ViewBag.MaxPage = (count / 20) - (count % 20 == 0 ? 1 : 0);
            ViewBag.CumRapId = cumRapId;
            ViewBag.TenKhachHang = tenKhachHang;
            ViewBag.SoDienThoai = soDienThoai;
            ViewBag.DiaChi = diaChiKhachHang;
            ViewBag.Email = emailKhachHang;
            return View(khachHangs);
        }

        public async Task<IActionResult> Transaction(int id)
        {
            var khachHang = await _khachHangServices.GetByIdAsync(id, StaticDetails.KhachHangAPIPath + "GetById");
            ViewBag.KhachHang = khachHang;
            var transactions = await _veServices.GetVeByKhachHangIdAsync(id, StaticDetails.VeAPIPath + "GetVeByKhachHangId");
            return View(transactions);
        }

        public async Task<IActionResult> TransactionDetails(int veId)
        {
            var ve = await _veServices.GetByIdAsync(veId, StaticDetails.VeAPIPath + "GetById");
            return View(ve);
        }
    }
}

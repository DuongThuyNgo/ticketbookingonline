﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TicketBookingOnline.ViewModels.LichChieus;
using TicketBookingOnline.ViewModels.Phims;
using TicketBookingOnline.ViewModels.Raps;
using TicketBookingOnline.Web.Areas.Admin.Controllers;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.CGV.Controllers
{
    [Area("Admin")]
    public class LichChieuController : BaseController
    {
        private readonly ILichChieuServices _lichChieuServices;
        private readonly IPhimServices _phimServices;
        private readonly IRapServices _rapServices;

        public LichChieuController(ILichChieuServices lichChieuServices, IPhimServices phimServices,
            IRapServices rapServices)
        {
            _lichChieuServices = lichChieuServices;
            _phimServices = phimServices;
            _rapServices = rapServices;
        }

        public async Task<IActionResult> Index(string timKiem, string trangThaiPhim, int index = 1)
        {
            int cumRapId = int.Parse(HttpContext.Session.GetString("CumRapId"));
            var raps = await _rapServices.GetRapByCumRapIdAsync(cumRapId, StaticDetails.RapAPIPath + "GetRapByCumRapId");
            ViewBag.Raps = raps as List<RapViewModel>;
            dynamic phims = "";
            int count = 0;
            if (!string.IsNullOrEmpty(timKiem))
            {
                count = await _phimServices.CountPhimByTen(timKiem, StaticDetails.PhimAPIPath + "CountPhimByTenPhim");
                phims = await _phimServices.GetPhimByTenWithRowperPage(index, timKiem, StaticDetails.PhimAPIPath + "GetPhimByTenPhimWithRowperPage");
            }
            else if (!string.IsNullOrEmpty(trangThaiPhim))
            {
                count = await _phimServices.CountPhimByTrangThaiWithRowperPage(trangThaiPhim, StaticDetails.PhimAPIPath + "CountPhimByTrangThai");
                phims = await _phimServices.GetPhimByTrangThaiWithRowperPage(index, trangThaiPhim, StaticDetails.PhimAPIPath + "GetPhimByTrangThaiWithRowperPage");
            }
            else
            {
                count = await _phimServices.CountAllPhimIsActiveAsync(StaticDetails.PhimAPIPath + "CountPhimIsActive");
                phims = await _phimServices.GetAllPhimIsActiveWithRowperPageAsync(index, StaticDetails.PhimAPIPath + "GetAllPhimIsActiveWithRowperPage");
            }
            ViewBag.Page = index;
            ViewBag.MaxPage = (count / 10) - (count % 10 == 0 ? 1 : 0);
            return View(phims);
        }

        [HttpGet]
        public async Task<JsonResult> Create(int id)
        {
            int cumRapId = int.Parse(HttpContext.Session.GetString("CumRapId"));
            var raps = await _rapServices.GetRapByCumRapIdAsync(cumRapId, StaticDetails.RapAPIPath + "GetRapByCumRapId");
            ViewBag.Raps = new SelectList(raps, "RapId", "TenRap");
            var phim = await _phimServices.GetPhimById(id, StaticDetails.PhimAPIPath + "GetPhimById");
            DateTime minDateTimeCreate = phim.KhoiChieu;
            if(phim.KhoiChieu < DateTime.Now)
                minDateTimeCreate = DateTime.Now;
            return Json( new { phim = phim, minDate = minDateTimeCreate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'00"), khoiChieu = phim.KhoiChieu.ToString("dd/MM/yyyy") });
        }

        [HttpPost]
        public async Task<IActionResult> Create(int phimId, int rapId, DateTime xuatChieu, int khuyenMai)
        {
            var lichChieu = new CreateLichChieuViewModel();
            lichChieu.PhimId = phimId;
            lichChieu.RapId = rapId;
            lichChieu.XuatChieu = xuatChieu;
            lichChieu.PhanTramKM = khuyenMai;
            var response = await _lichChieuServices.CreateAsync(lichChieu, StaticDetails.LichChieuAPIPath + "Create");
            if (response)
            {
                TempData["Success"]="Tạo lịch chiếu thành công !";
            }
            else
            {
                TempData["Error"] = "Lịch chiếu đã tồn tại !";
            }
            return RedirectToAction(nameof(Index));

        }

        [HttpGet]
        public async Task<IActionResult> LichChieu(int phimId, DateTime dateTime, int ? rapId)
        {
            int cumRapId = int.Parse(HttpContext.Session.GetString("CumRapId"));
            var lichChieus = await _lichChieuServices.FilterAsync(phimId, cumRapId, rapId, dateTime,
                StaticDetails.LichChieuAPIPath + "Filter/" );
            var t = lichChieus.AsQueryable().ToList();
            var phimVM = await _phimServices.GetPhimById(phimId, StaticDetails.PhimAPIPath + "GetPhimById");
            ViewBag.Phim = (PhimDetailViewModel)phimVM;
            var raps = await _rapServices.GetRapByCumRapIdAsync(cumRapId, StaticDetails.RapAPIPath + "GetRapByCumRapId");
            ViewData["Raps"] = raps.ToList();
            var defaultDate = new DateTime(0001, 01, 01);
            if(dateTime != defaultDate)
            {
                ViewData["FindDatetime"] = dateTime.ToString("yyyy'-'MM'-'dd");
            }
            ViewBag.RapId = rapId;
            return View(lichChieus);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int lichChieuId, int phim)
        {
            var response = await _lichChieuServices.DeleteAsync(lichChieuId, StaticDetails.LichChieuAPIPath + "Delete");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction("LichChieu", "LichChieu", new { phimId = phim });
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.ViewModels.CGVManagers;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class NhanVienController : Controller
    {
        private readonly ICGVManagerServices _cGVManagerServices;

        public NhanVienController(ICGVManagerServices cGVManagerServices)
        {
            _cGVManagerServices = cGVManagerServices;
        }

        public async Task<IActionResult> Index(int index = 1)
        {
            int cumRapId = int.Parse(HttpContext.Session.GetString("CumRapId"));
            var nhanViens = await _cGVManagerServices.GetNhanVienInCumRapWithRowPerPageAsync(cumRapId, index,
                StaticDetails.CGVManagerAPIPath + "GetAllNhanVienInCumRapWithRowPerPage");
            int count = await _cGVManagerServices.CountAllNhanVienInCumRapAsync(cumRapId, StaticDetails.CGVManagerAPIPath + "CountAllNhanVienInCumRapId");
            ViewBag.Page = index;
            ViewBag.MaxPage = (count / 10) - (count % 10 == 0 ? 1 : 0);
            return View(nhanViens);
        }

        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {

            var cGVManager = await _cGVManagerServices.FindAsync(id, StaticDetails.CGVManagerAPIPath + "GetCGVManager");
            return View(cGVManager);
        }

        [HttpGet]
        public IActionResult Create()
        {
            int cumRapId = int.Parse(HttpContext.Session.GetString("CumRapId"));
            CreateCGVManagerViewModel cGVManagerViewModel = new CreateCGVManagerViewModel();
            cGVManagerViewModel.CumRapId = cumRapId;
            return View(cGVManagerViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateCGVManagerViewModel request)
        {
            if (!ModelState.IsValid)
            {

                return View(request);
            }

            var response = await _cGVManagerServices.CreateAsync(request, StaticDetails.CGVManagerAPIPath + "CreateAsync");
            if (response == "Success")
            {
                TempData["Success"] = "Thêm mới thành công !";
                return RedirectToAction(nameof(Index));
            }
            TempData["Error"] = "Đã xảy ra lỗi ...!";
            ViewBag.ErrorMessage = response;
            return View(request);
        }

        [HttpGet]
        public async Task<IActionResult> Update(string id)
        {
            var cGVManager = await _cGVManagerServices.FindAsync(id, StaticDetails.CGVManagerAPIPath + "GetCGVManager");

            return View(cGVManager);
        }

        [HttpPost]
        public async Task<IActionResult> Update(CGVManagerDetailViewModel request)
        {
            var response = await _cGVManagerServices.UpdateAsync(request, StaticDetails.CGVManagerAPIPath + "UpdateAsync");
            if (response)
            {
                TempData["Success"] = "Cập nhật thành công !";
                return RedirectToAction(nameof(Index));
            }
            TempData["Success"] = "Đã xảy ra lỗi !";
            return View(request);
        }

        public async Task<IActionResult> Delete(string id)
        {
            var cGVManager = await _cGVManagerServices.FindAsync(id, StaticDetails.CGVManagerAPIPath + "GetCGVManager");
            return View(cGVManager);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var response = await _cGVManagerServices.DeleteAsync(id, StaticDetails.CGVManagerAPIPath + "DeleteAsync");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
                return RedirectToAction(nameof(Index));
            }
            TempData["Success"] = "Đã xảy ra lỗi !";
            return RedirectToAction(nameof(Index));
        }
    }
}


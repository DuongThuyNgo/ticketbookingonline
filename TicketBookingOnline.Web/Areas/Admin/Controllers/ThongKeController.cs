﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ThongKeController : BaseController
    {
        private readonly IThongKeServices _thongKeServices;
        private readonly IThanhPhoServices _thanhPhoServices;

        public ThongKeController(IThongKeServices thongKeServices, IThanhPhoServices thanhPhoServices)
        {
            _thongKeServices = thongKeServices;
            _thanhPhoServices = thanhPhoServices;
        }

        public async Task<IActionResult> Index(DateTime ngay)
        {
            DateTime dateTime = DateTime.Now;
            var dateDefault = new DateTime(0001, 01, 01);
            if (ngay != dateDefault && ngay != null)
                dateTime = ngay;
          
            var doanhThus = await _thongKeServices.GetAllDoanhThuNgayAsync(dateTime, null, StaticDetails.ThongKeAPIPath +
                "GetAllDoanhThuNgay");
            
            ViewBag.Ngay = dateTime.ToString("yyyy'-'MM'-'dd");
            
            ViewBag.TongDoanhThu = doanhThus.Sum(x => x.DoanhThu);
            return View(doanhThus);
        }

        public async Task<IActionResult> IndexCumRap(int thang, int nam)
        {
            int cumRapId = int.Parse(HttpContext.Session.GetString("CumRapId"));
            DateTime dateTime = DateTime.Now;
            if(thang == 0 || nam == 0)
            {
                thang = dateTime.Month;
                nam = dateTime.Year;
            }

            var doanhThus = await _thongKeServices.GetAllDoanhThuNgayOfThangInCumRap(cumRapId, thang, nam, StaticDetails.ThongKeAPIPath +
                "GetAllDoanhThuNgayOfThangInCumRap");

            ViewBag.Thang = thang;
            ViewBag.Nam = nam;
            ViewBag.TongDoanhThu = doanhThus.Sum(x => x.DoanhThu);
            return View(doanhThus);
        }

        public async Task<IActionResult> SaleMonth(int thang, int nam)
        {
            if(thang == null ||nam == null)
            {
                return View();
            }
            var doanhThus = await _thongKeServices.GetAllDoanhThuThangAsync(thang, nam, null, StaticDetails.ThongKeAPIPath +
                "GetAllDoanhThuThang");
            ViewBag.Thang = thang;
            ViewBag.Nam = nam;
            ViewBag.TongDoanhThu = doanhThus.Sum(x => x.DoanhThu);
            return View(doanhThus);
        }

        public async Task<IActionResult> SaleYear(int namTimKiem)
        {
            if (namTimKiem == null)
            {
                return View();
            }
            var doanhThus = await _thongKeServices.GetAllDoanhThuNamAsync(namTimKiem, null, StaticDetails.ThongKeAPIPath +
                "GetAllDoanhThuNam");
            ViewBag.NamTimKiem = namTimKiem;
            ViewBag.TongDoanhThu = doanhThus.Sum(x => x.DoanhThu);
            return View(doanhThus);
        }
        
        public async Task<IActionResult> BieuDoThongKeNgay(DateTime ? ngay, int thanhPhoId)
        {
            DateTime dateTime = DateTime.Now;
            var dateDefault = new DateTime(0001, 01, 01);
            if (ngay != dateDefault && ngay != null)
                dateTime = ngay.Value;
            if(thanhPhoId == 0)
            {
                thanhPhoId = 1;
            }

            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            var doanhThus = await _thongKeServices.GetAllDoanhThuNgayAsync(dateTime, thanhPhoId, StaticDetails.ThongKeAPIPath +
                "GetAllDoanhThuNgay");
            var doanhThuCumRaps = new List<decimal>();
            var tenCumRaps = new List<string>();
            foreach (var item in doanhThus)
            {
                doanhThuCumRaps.Add(item.DoanhThu);
                tenCumRaps.Add(item.CumRap.TenRap);
            }
            ViewBag.ThanhPhoId = thanhPhoId;
            ViewBag.TenThanhPho = thanhPhos.Where(x => x.ThanhPhoId == thanhPhoId).FirstOrDefault().TenThanhPho ;
            ViewBag.Ngay = dateTime.ToString("yyyy'-'MM'-'dd");
            //ViewData["Ngay"] = dateTime.ToString("yyyy'-'MM'-'dd");
            ViewBag.TenRaps = tenCumRaps;
            ViewBag.DoanhThu = doanhThuCumRaps;
            ViewBag.TongDoanhThu = doanhThus.Sum(x=>x.DoanhThu);
            return View(doanhThus);
        }

        public async Task<IActionResult> BieuDoThongKeThang(int thang, int nam, int thanhPhoId)
        {
            if(thang == null)
                thang = DateTime.Now.Month;
            if(nam == null)
            {
                nam = DateTime.Now.Year;
            }
            if (thanhPhoId == 0)
            {
                thanhPhoId = 1;
            }

            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            var doanhThus = await _thongKeServices.GetAllDoanhThuThangAsync(thang, nam, thanhPhoId, 
                StaticDetails.ThongKeAPIPath + "GetAllDoanhThuThang");
            var doanhThuCumRaps = new List<decimal>();
            var tenCumRaps = new List<string>();
            foreach (var item in doanhThus)
            {
                doanhThuCumRaps.Add(item.DoanhThu);
                tenCumRaps.Add(item.CumRap.TenRap);
            }
            ViewBag.ThanhPhoId = thanhPhoId;
            ViewBag.TenThanhPho = thanhPhos.Where(x => x.ThanhPhoId == thanhPhoId).FirstOrDefault().TenThanhPho;
            ViewBag.Thang = thang;
            ViewBag.Nam = nam;
            ViewBag.TenRaps = tenCumRaps;
            ViewBag.DoanhThu = doanhThuCumRaps;
            ViewBag.TongDoanhThu = doanhThus.Sum(x => x.DoanhThu);
            return View(doanhThus);
        }

        public async Task<IActionResult> BieuDoThongKeNam(int nam, int thanhPhoId)
        {
            if (nam == null)
            {
                nam = DateTime.Now.Year;
            }
            if (thanhPhoId == 0)
            {
                thanhPhoId = 1;
            }

            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            var doanhThus = await _thongKeServices.GetAllDoanhThuNamAsync(nam, thanhPhoId,
                StaticDetails.ThongKeAPIPath + "GetAllDoanhThuNam");
            var doanhThuCumRaps = new List<decimal>();
            var tenCumRaps = new List<string>();
            foreach (var item in doanhThus)
            {
                doanhThuCumRaps.Add(item.DoanhThu);
                tenCumRaps.Add(item.CumRap.TenRap);
            }
            ViewBag.ThanhPhoId = thanhPhoId;
            ViewBag.TenThanhPho = thanhPhos.Where(x => x.ThanhPhoId == thanhPhoId).FirstOrDefault().TenThanhPho;
            ViewBag.NamTimKiem = nam;
            ViewBag.TenRaps = tenCumRaps;
            ViewBag.DoanhThu = doanhThuCumRaps;
            ViewBag.TongDoanhThu = doanhThus.Sum(x => x.DoanhThu);
            return View(doanhThus);
        }
    }
}

﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.CGV.Controllers
{
    [Area("Admin")]
    public class AccountController : Controller
    {
        private readonly IAccountServices _accountServices;

        public AccountController(IAccountServices accountServices)
        {
            _accountServices = accountServices;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel request)
        {
            if (!ModelState.IsValid)
                return View(request);
            var userLogin = await _accountServices.SignInAsync(request, StaticDetails.AccountAPIPath + "Login");
            if (userLogin != null)
            {    
                // Claim identity
                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                identity.AddClaim(new Claim(ClaimTypes.Name, userLogin.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Sid, userLogin.CumRapId.ToString()));

                var principal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                
                //HttpContext.Session.SetString("JWToken", atm.Token);
                HttpContext.Session.SetString("CGVManagerId", userLogin.Id);
                HttpContext.Session.SetString("UserName", userLogin.UserName);
                HttpContext.Session.SetString("Email", userLogin.Email);
                HttpContext.Session.SetString("Role", userLogin.RoleName);
                if(userLogin.CumRapId != 0)
                {
                    HttpContext.Session.SetString("CumRapId", userLogin.CumRapId.ToString());
                    HttpContext.Session.SetString("TenRap", userLogin.TenRap.ToString());
                }
                return Redirect("/Admin");
            }
            TempData["ErrorMesages"] = "Email hoặc mật khẩu không chính xác !";
            return View(request);
        }

        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Clear();
            var response = await _accountServices.SingOutAsync(StaticDetails.AccountAPIPath + "Logout");
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public async Task<IActionResult> UpdateInfor()
        {
            var id = HttpContext.Session.GetString("CGVManagerId");
            var cGVManager = await _accountServices.FindAccountAsync(id, StaticDetails.AccountAPIPath + "FindAccount");
            return View(cGVManager);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateInfor(UpdateAccountViewModel request)
        {
            if (!ModelState.IsValid)
                return View(request);
            var response = await _accountServices.UpdateInforAsync(request, StaticDetails.AccountAPIPath + "UpdateInforCGVManager");
            if (response=="Success")
            {
                TempData["Success"] = "Cập nhật thành công !";
                ViewBag.ErrorMessage = null;
            }
            else
            {
                TempData["Error"] = "Xảy ra lỗi !";
                ViewBag.ErrorMessage = response;
            }
            return View(request);
        }

        public IActionResult ChangePassword(string email)
        {
            var model = new ChangePassword { Email = email };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePassword resetPassword)
        {
            if (!ModelState.IsValid)
            {
                return View(resetPassword);
            }
            if(resetPassword.NewPassword != resetPassword.ConfirmPassword)
            {
                TempData["Error"] = "Xác nhận mật khẩu không chính xác!";
                return View(resetPassword);
            }
            var response = await _accountServices.ChangePassword(resetPassword, StaticDetails.AccountAPIPath + "ChangePassword");

            if (response.IsSuccessed)
            {
                TempData["Success"] = "Đổi mật khẩu thành công ! ";
            }
            else
            {
                TempData["Error"] = response.ErrrorMessages[0];              
            }
            return View(resetPassword);
        }

    }
}

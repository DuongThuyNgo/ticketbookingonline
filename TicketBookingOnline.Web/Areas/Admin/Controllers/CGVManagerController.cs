﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TicketBookingOnline.ViewModels.CGVManagers;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CGVManagerController : BaseController
    {
        private readonly ICGVManagerServices _cGVManagerServices;
        private readonly IThanhPhoServices _thanhPhoServices;
        private readonly ICumRapServices _cumRapServices;

        public CGVManagerController(ICGVManagerServices cGVManagerServices, IThanhPhoServices thanhPhoServices,
            ICumRapServices cumRapServices)
        {
            _cGVManagerServices = cGVManagerServices;
            _thanhPhoServices = thanhPhoServices;
            _cumRapServices = cumRapServices;
        }

        public async Task<IActionResult> Index(int thanhPhoId = 0, int cumRapId = 0, int index = 1)
        {
            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            TempData["ThanhPhoId"] = thanhPhoId;
            TempData["TenThanhPho"] = "";
            if (thanhPhoId != 0)
            {
                TempData["TenThanhPho"] = thanhPhos.Where(x => x.ThanhPhoId == thanhPhoId).First().TenThanhPho;
            }
            var cumRaps = await _cumRapServices.GetAllAsync(null, thanhPhoId, StaticDetails.CumRapAPIPath +
                "GetAll");
            ViewBag.CumRap = new SelectList(cumRaps, "CumRapId", "TenRap");
            TempData["CumRapId"] = cumRapId;
            TempData["TenCumRap"] = "";
            if (cumRapId != 0)
            {
                TempData["TenCumRap"] = cumRaps.Where(x => x.CumRapId == cumRapId).First().TenRap;
            }
            var cGVManagers = await _cGVManagerServices.GetWithRowPerPageAsync(index, thanhPhoId, cumRapId,
                StaticDetails.CGVManagerAPIPath + "GetWithRowPerPage");
            int count = await _cGVManagerServices.CountAllAsync(StaticDetails.CGVManagerAPIPath + "GetWithRowPerPage");
            ViewBag.Page = index;
            ViewBag.MaxPage = (count / 10) - (count % 10 == 0 ? 1 : 0);
            return View(cGVManagers);
        }

        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {
            var cGVManager = await _cGVManagerServices.FindAsync(id, StaticDetails.CGVManagerAPIPath + "GetCGVManager");
            return View(cGVManager);
        }

        [HttpGet]
        public async Task<IActionResult> Create(int thanhPhoId = 0)
        {
            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            var cumRaps = await _cumRapServices.GetAllAsync(null, thanhPhoId, StaticDetails.CumRapAPIPath +
                "GetAll");
            ViewBag.CumRap = new SelectList(cumRaps, "CumRapId", "TenRap");
            CreateCGVManagerViewModel cGVManagerViewModel = new CreateCGVManagerViewModel();
            return View(cGVManagerViewModel);
        }

        [HttpGet]
        public async Task<JsonResult> FilterCumRap(int thanhPhoId)
        {
            var cumRaps = await _cumRapServices.GetAllAsync(null, thanhPhoId, StaticDetails.CumRapAPIPath +
                "GetAll");
            var listCumRap = new SelectList(cumRaps, "CumRapId", "TenRap");
            return Json(new { list = cumRaps.ToList() });
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateCGVManagerViewModel request)
        {
            if (!ModelState.IsValid)
            {
                
                return View(request);
            }
                
            var response = await _cGVManagerServices.CreateAsync(request, StaticDetails.CGVManagerAPIPath + "CreateAsync");
            if (response == "Success")
            {
                TempData["Success"] = "Thêm mới thành công !";
                return RedirectToAction(nameof(Index));
            }
            TempData["Error"] = "Đã xảy ra lỗi ...!";
            TempData["ErrorMessage"] = response;
            var cumRaps = await _cumRapServices.GetAllAsync(null, 0, StaticDetails.CumRapAPIPath +
                "GetAll");
            ViewBag.CumRap = new SelectList(cumRaps, "CumRapId", "TenRap");
            //TempData["CumRapId"] = cGVManager.CumRapId;
            TempData["TenCumRap"] = cumRaps.Where(x => x.CumRapId == request.CumRapId).FirstOrDefault().TenRap;
            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            var thanhPhoId = cumRaps.Where(x => x.CumRapId == request.CumRapId).FirstOrDefault().ThanhPhoId;
            TempData["ThanhPhoId"] = thanhPhoId;
            TempData["TenThanhPho"] = thanhPhos.Where(x => x.ThanhPhoId == thanhPhoId).FirstOrDefault().TenThanhPho;
            return View(request);
        }

        [HttpGet]
        public async Task<IActionResult> Update(string id)
        {
            var cGVManager = await _cGVManagerServices.FindAsync(id, StaticDetails.CGVManagerAPIPath + "GetCGVManager");
            var cumRaps = await _cumRapServices.GetAllAsync(null, 0, StaticDetails.CumRapAPIPath +
                "GetAll");
            ViewBag.CumRap = new SelectList(cumRaps, "CumRapId", "TenRap");
            //TempData["CumRapId"] = cGVManager.CumRapId;
            TempData["TenCumRap"] = cumRaps.Where(x => x.CumRapId == cGVManager.CumRapId).FirstOrDefault().TenRap;
            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            var thanhPhoId = cumRaps.Where(x => x.CumRapId == cGVManager.CumRapId).FirstOrDefault().ThanhPhoId;
            TempData["ThanhPhoId"] = thanhPhoId;
            TempData["TenThanhPho"] = thanhPhos.Where(x => x.ThanhPhoId == thanhPhoId).FirstOrDefault().TenThanhPho;
            return View(cGVManager);
        }

        [HttpPost]
        public async Task<IActionResult> Update(CGVManagerDetailViewModel request)
        {
            var response = await _cGVManagerServices.UpdateAsync(request, StaticDetails.CGVManagerAPIPath + "UpdateAsync");
            if (response)
            {
                TempData["Success"] = "Cập nhật thành công !";
                return RedirectToAction(nameof(Index));
            }
            TempData["Success"] = "Đã xảy ra lỗi !";
            TempData["ErrorMessage"] = response;
            return View(request);
        }

        public async Task<IActionResult> Delete(string id)
        {
            var cGVManager = await _cGVManagerServices.FindAsync(id, StaticDetails.CGVManagerAPIPath + "GetCGVManager");
            return View(cGVManager);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var response = await _cGVManagerServices.DeleteAsync(id, StaticDetails.CGVManagerAPIPath + "DeleteAsync");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
                return RedirectToAction(nameof(Index));
            }
            TempData["Success"] = "Đã xảy ra lỗi !";
            return RedirectToAction(nameof(Index));
        }
    }
}

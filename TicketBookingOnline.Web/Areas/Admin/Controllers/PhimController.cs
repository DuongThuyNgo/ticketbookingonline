﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TicketBookingOnline.ViewModels.Phims;
using TicketBookingOnline.Web.IServices;



namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PhimController : BaseController
    {
        private readonly IPhimServices _phimServices;
        private readonly ITheLoaiServices _theLoaiServices;
        private readonly ICanhBaoServices _canhBaoServices;
        private readonly IPhimTheLoaiServices _phimTheLoaiServices;
        private readonly IWebHostEnvironment _webHostEnvironment;


        public PhimController(IPhimServices phimServices, ITheLoaiServices theLoaiServices,
            ICanhBaoServices canhBaoServices, IPhimTheLoaiServices phimTheLoaiServices,
            IWebHostEnvironment webHostEnvironment)
        {
            _phimServices = phimServices;
            _theLoaiServices = theLoaiServices;
            _canhBaoServices = canhBaoServices;
            _phimTheLoaiServices = phimTheLoaiServices;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task<IActionResult> Index(string ? timKiem, string ? trangThaiPhim, int index = 1)
        {
            dynamic phims="";
            int count = 0;
            if (!string.IsNullOrEmpty(timKiem))
            {
                count = await _phimServices.CountPhimByTen(timKiem, StaticDetails.PhimAPIPath + "CountPhimByTenPhim");
                phims = await _phimServices.GetPhimByTenWithRowperPage(index, timKiem, StaticDetails.PhimAPIPath + "GetPhimByTenPhimWithRowperPage");
            }
            else if (!string.IsNullOrEmpty(trangThaiPhim))
            {
                count = await _phimServices.CountPhimByTrangThaiWithRowperPage(trangThaiPhim, StaticDetails.PhimAPIPath + "CountPhimByTrangThai");
                phims = await _phimServices.GetPhimByTrangThaiWithRowperPage(index, trangThaiPhim, StaticDetails.PhimAPIPath + "GetPhimByTrangThaiWithRowperPage");
            }
            else
            {
                count = await _phimServices.CountAllPhimIsActiveAsync(StaticDetails.PhimAPIPath + "CountPhimIsActive");
                phims = await _phimServices.GetAllPhimIsActiveWithRowperPageAsync(index, StaticDetails.PhimAPIPath + "GetAllPhimIsActiveWithRowperPage");
            }
            
            ViewBag.Page = index; 
            ViewBag.MaxPage = (count / 8) - (count % 8 == 0 ? 1 : 0);
            ViewBag.TimKiem = timKiem;
            ViewBag.TrangThaiPhim = trangThaiPhim;
            return View(phims);
        }


        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var phim = new CreatePhimViewModel();
            var theloais = await _theLoaiServices.GetAllAsync(StaticDetails.TheLoaiAPIPath + "GetAll");

            ViewBag.TheLoais = new SelectList(theloais, "TheLoaiId", "TenTheLoai");
            var canhbaos = await _canhBaoServices.GetAllAsync(StaticDetails.CanhBaoAPIPath + "GetAll");
            ViewBag.CanhBaoId = new SelectList(canhbaos, "CanhBaoId", "TenCanhBao");
            return View(phim);
        }

        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Create(CreatePhimViewModel phim, IFormFile banner, IFormFile anh,
            IFormFile trailer)
        {
            if (banner != null)
            {
                string bannerFolder = Path.Combine(_webHostEnvironment.WebRootPath, "banners");
                string uniqueBanner = Guid.NewGuid().ToString().Substring(0, 8) + "_" + banner.FileName;
                string bannerPath = Path.Combine(bannerFolder, uniqueBanner);
                using (var fileStream = new FileStream(bannerPath, FileMode.Create))
                {
                    banner.CopyTo(fileStream);
                }
                phim.Banner = uniqueBanner;
            }
            if (anh != null)
            {
                string posterFolder = Path.Combine(_webHostEnvironment.WebRootPath, "posters");
                string uniquePoster = Guid.NewGuid().ToString().Substring(0, 8) + "_" + anh.FileName;
                string posterPath = Path.Combine(posterFolder, uniquePoster);
                using (var fileStream = new FileStream(posterPath, FileMode.Create))
                {
                    anh.CopyTo(fileStream);
                }
                phim.Anh = uniquePoster;
            }
            if (trailer != null)
            {
                string trailerFolder = Path.Combine(_webHostEnvironment.WebRootPath, "trailers");
                string uniqueTrailer = Guid.NewGuid().ToString().Substring(0, 8) + "_" + trailer.FileName;
                string trailerPath = Path.Combine(trailerFolder, uniqueTrailer);
                using (var fileStream =
                   new FileStream(trailerPath, FileMode.Create, FileAccess.Write))
                {
                    await trailer.CopyToAsync(fileStream);
                }
                phim.Trailer = uniqueTrailer;
            }

            if (ModelState.IsValid)
            {
                var result = await _phimServices.CreateAsync(phim, StaticDetails.PhimAPIPath + "Create");

                if (result != null)
                {
                    TempData["Success"] = "Thêm mới phim thành công !";
                }
                else
                {
                    TempData["Error"] = "Đã xảy ra lỗi !";
                }
                return RedirectToAction(nameof(Index));

            }
            else
            {
                var theloais = await _theLoaiServices.GetAllAsync(StaticDetails.TheLoaiAPIPath + "GetAll");
                ViewBag.TheLoais = new SelectList(theloais, "TheLoaiId", "TenTheLoai");
                var canhbaos = await _canhBaoServices.GetAllAsync(StaticDetails.CanhBaoAPIPath + "GetAll");
                ViewBag.CanhBaoId = new SelectList(canhbaos, "CanhBaoId", "TenCanhBao");
                return View(phim);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int phimId)
        {
            var phim = new CreatePhimViewModel();
            phim = await _phimServices.GetPhimUpdate(phimId, StaticDetails.PhimAPIPath + "GetPhimUpdate");
            var theloais = await _theLoaiServices.GetAllAsync(StaticDetails.TheLoaiAPIPath + "GetAll");
            ViewBag.TheLoais = new SelectList(theloais, "TheLoaiId", "TenTheLoai");
            var canhbaos = await _canhBaoServices.GetAllAsync(StaticDetails.CanhBaoAPIPath + "GetAll");
            ViewBag.CanhBaoId = new SelectList(canhbaos, "CanhBaoId", "TenCanhBao");
            return View(phim);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CreatePhimViewModel phim, IFormFile bannerfile, IFormFile anhfile,
            IFormFile trailerfile)
        {
            if (bannerfile != null)
            {
                string bannerFolder = Path.Combine(_webHostEnvironment.WebRootPath, "banners");
                string uniqueBanner = Guid.NewGuid().ToString().Substring(0, 8) + "_" + bannerfile.FileName;
                string bannerPath = Path.Combine(bannerFolder, uniqueBanner);
                using (var fileStream = new FileStream(bannerPath, FileMode.Create))
                {
                    bannerfile.CopyTo(fileStream);
                }
                phim.Banner = uniqueBanner;
            }
            if (anhfile != null)
            {
                string posterFolder = Path.Combine(_webHostEnvironment.WebRootPath, "posters");
                string uniquePoster = Guid.NewGuid().ToString().Substring(0, 8) + "_" + anhfile.FileName;
                string posterPath = Path.Combine(posterFolder, uniquePoster);
                using (var fileStream = new FileStream(posterPath, FileMode.Create))
                {
                    anhfile.CopyTo(fileStream);
                }
                phim.Anh = uniquePoster;
            }
            if (trailerfile != null)
            {
                string trailerFolder = Path.Combine(_webHostEnvironment.WebRootPath, "trailers");
                string uniqueTrailer = Guid.NewGuid().ToString().Substring(0, 8) + "_" + trailerfile.FileName;
                string trailerPath = Path.Combine(trailerFolder, uniqueTrailer);
                using (var fileStream =
                   new FileStream(trailerPath, FileMode.Create, FileAccess.Write))
                {
                    await trailerfile.CopyToAsync(fileStream);
                }
                phim.Trailer = uniqueTrailer;
            }
           
            var result = await _phimServices.UpdateAsync(phim, StaticDetails.PhimAPIPath + "Update");
            if (result != null)
            {
                TempData["Success"] = "Cập nhật thông tin phim thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction(nameof(Index));
            
        }


        [HttpPost]
        public async Task<IActionResult> Delete(int phimId)
        {
            var response= await _phimServices.DeleteAsync(phimId, StaticDetails.PhimAPIPath + "Delete");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction(nameof(Index));
        }
    }
}

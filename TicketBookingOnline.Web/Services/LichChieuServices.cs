﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.LichChieus;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class LichChieuServices :ILichChieuServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public LichChieuServices(IHttpClientFactory httpClientFactory)
        {
            _clientFactory = httpClientFactory;
        }

        public async Task<bool> CreateAsync(CreateLichChieuViewModel lichChieuVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(lichChieuVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;           
        }

        public async Task<bool> DeleteAsync(int lichChieuId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, url + "/" + lichChieuId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<LichChieuViewModel>> FilterAsync(int phimId, int? cumRapId, int? rapId, DateTime ? ngayChieu, string url)
        {
            url = url + "?phimId=" + phimId;
            if (cumRapId != null)
                url = url + "&cumRapId=" + cumRapId;
            if(rapId != null)
                url = url + "&rapId=" + rapId;
            if (ngayChieu != null)
                url = url + "&ngayChieu=" + ngayChieu;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<LichChieuViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<LichChieuViewModel>> GetLichChieuInCumRapAsync(int cumRapId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + cumRapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<LichChieuViewModel>>(jsonString);
            }
            return null;
        }

            public async Task<IEnumerable<LichChieuViewModel>> GetLichChieuOfPhimInCumRapAsync(int phimId, int cumRapId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<LichChieuViewModel>>(jsonString);
            }
            return null;
        }
    }
}

﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.Ves;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class VeServices : IVeServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public VeServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<bool> CancelVeAsync(DeleteVeViewModel veVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(veVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> ConfirmVeAsync(int veId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + veId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<int> CountTotalVe(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, string url)
        {
            if (cumRapId != null)
                url = url + "?cumRapId=" + cumRapId;
            if (rapId != null)
                url = url + "&rapId=" + rapId;
            if (ngayChieu != null)
                url = url + "&ngayChieu=" + ngayChieu;
            if (tenPhim != null)
                url = url + "&tenPhim=" + tenPhim;
            if (ngayChieu != null)
                url = url + "&userName=" + userName;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return -1;
        }

        public async Task<IEnumerable<VeViewModel>> Filter(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, string url)
        {
            if (cumRapId != null)
                url = url + "?cumRapId=" + cumRapId;
            if (rapId != null)
                url = url + "&rapId=" + rapId;
            if (ngayChieu != null)
                url = url + "&ngayChieu=" + ngayChieu;
            if (tenPhim != null)
                url = url + "&tenPhim=" + tenPhim;
            if (ngayChieu != null)
                url = url + "&userName=" + userName;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<VeViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<VeViewModel>> GetAllIsActiveAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<VeViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<VeDetailViewModel> GetByIdAsync(int veId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + veId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VeDetailViewModel>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<VeViewModel>> GetVeByKhachHangIdAsync(int khachHangId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + khachHangId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<VeViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<VeViewModel>> VeWithRowPerPage(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, string url, int index)
        {
            url = url + "/" + index;
            if (cumRapId != null)
                url = url + "?cumRapId=" + cumRapId;
            if (rapId != null)
                url = url + "&rapId=" + rapId;
            if (ngayChieu != null)
                url = url + "&ngayChieu=" + ngayChieu;
            if (tenPhim != null)
                url = url + "&tenPhim=" + tenPhim;
            if (ngayChieu != null)
                url = url + "&userName=" + userName;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<VeViewModel>>(jsonString);
            }
            return null;
        }
    }
}

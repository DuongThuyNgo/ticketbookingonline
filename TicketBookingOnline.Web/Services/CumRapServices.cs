﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.CumRaps;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class CumRapServices : ICumRapServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public CumRapServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<int> CountAllAsync(string tenCumRap, int thanhPhoId, string url)
        {
            url = url + "?";
            if (tenCumRap != null)
            {
                url = url + "&tenCumRap=" + tenCumRap;
            }
            if (thanhPhoId != null)
            {
                url = url + "&thanhPhoId=" + thanhPhoId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return -1;
        }

        public async Task<bool> CreateAsync(CreateCumRapViewModel cumRapVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(cumRapVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteAsync(int cumRapId, string url)
        {

            var request = new HttpRequestMessage(HttpMethod.Delete, url + "/" + cumRapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<CumRapViewModel>> GetAllAsync(string tenCumRap, int thanhPhoId, string url)
        {
            url = url + "?";
            if (tenCumRap != null)
            {
                url = url + "&tenCumRap=" + tenCumRap;
            }
            if(thanhPhoId != null)
            {
                url = url + "&thanhPhoId=" + thanhPhoId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<CumRapViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<CumRapViewModel> GetByIdAsync(int cumRapId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + cumRapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<CumRapViewModel>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<CumRapViewModel>> GetWithRowPerPageAsync(int index, string tenCumRap, int thanhPhoId, string url)
        {
            url = url + "/" + index + "?";
            if (tenCumRap != null)
            {
                url = url + "&tenCumRap=" + tenCumRap;
            }
            if (thanhPhoId != null)
            {
                url = url + "&thanhPhoId=" + thanhPhoId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<CumRapViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<bool> UpdateAsync(CreateCumRapViewModel cumRapVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(cumRapVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }
    }
}

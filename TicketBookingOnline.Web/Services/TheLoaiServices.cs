﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.TheLoais;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class TheLoaiServices : ITheLoaiServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public TheLoaiServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<bool> CreateAsync(TheLoaiViewModel theLoai, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(theLoai), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteAsync(int theLoaiId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, url + "/" + theLoaiId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }

        public async Task<List<TheLoaiViewModel>> GetAllAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<TheLoaiViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<TheLoaiViewModel> GetByIdAsync(int theLoaiId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + theLoaiId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TheLoaiViewModel>(jsonString);
            }
            return null;
        }

        public async Task<bool> UpdateAsync(TheLoaiViewModel theLoai, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(theLoai), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }
    }
}

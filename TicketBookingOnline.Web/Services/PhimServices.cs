﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.Phims;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class PhimServices : IPhimServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public PhimServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<int> CountAllPhimIsActiveAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return 0;
        }

        public async Task<int> CountPhimByTen(string tenPhim, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + tenPhim);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return 0;
        }

        public async Task<int> CountPhimByTheLoai(int maTheLoai, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + maTheLoai);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return 0;
        }

        public async Task<int> CountPhimByTheLoaisAsync(List<int> maTheLoai, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url );
            request.Content = new StringContent(JsonConvert.SerializeObject(maTheLoai), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return 0;
        }

        public async Task<int> CountPhimByTrangThaiWithRowperPage(string trangThai, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + trangThai);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return 0;
        }

        public async Task<CreatePhimViewModel> CreateAsync(CreatePhimViewModel phimVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);           
            request.Content = new StringContent(JsonConvert.SerializeObject(phimVM), Encoding.UTF8, "application/json");                    
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<CreatePhimViewModel>(jsonString);
            }
            return null;

        }

        public async Task<bool> DeleteAsync(int phimId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, url + "/" + phimId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<PhimViewModel>> GetAllAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetAllPhimIsActiveAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetAllPhimIsActiveWithRowperPageAsync(int index, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + index);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<PhimDetailViewModel> GetPhimById(int phimId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/"+ phimId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<PhimDetailViewModel>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetPhimByListTheLoai(List<int> theloais, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url );
            request.Content = new StringContent(JsonConvert.SerializeObject(theloais), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetPhimByListTheLoaiWithRowPerPageAsync(List<int> theloais, int index, int rowPerPage, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + index + "/" + rowPerPage);
            request.Content = new StringContent(JsonConvert.SerializeObject(theloais), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetPhimByTen(string tenPhim, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url+"/"+tenPhim);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetPhimByTenWithRowperPage(int index, string tenPhim, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + index +"/" + tenPhim);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetPhimByTheLoaiRowPerPage(int maTheLoai, int index, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + maTheLoai + "/" + index);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetPhimByTrangThaiWithRowperPage(int index, string trangThai, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + index + "/" + trangThai);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetPhimHotAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<PhimViewModel>> GetPhimLienQuanAsync(int phimId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + phimId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<PhimViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<CreatePhimViewModel> GetPhimUpdate(int phimId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + phimId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<CreatePhimViewModel>(jsonString);
            }
            return null;
        }

        public async Task<CreatePhimViewModel> UpdateAsync(CreatePhimViewModel phimVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(phimVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<CreatePhimViewModel>(jsonString);
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> UpdateTrangThaiPhimAsync(int phimId, string trangThai, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url + "/" + phimId + "/" + trangThai);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }
    }
}

﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.CGVManagers;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class AccountServices : IAccountServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public AccountServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<UpdateAccountViewModel> FindAccountAsync(string id, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + id);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UpdateAccountViewModel>(jsonString);
                //return JsonConvert.DeserializeObject<CreatePhimViewModel>(jsonString);
            }
            return null;
        }

        public async Task<ResponseResult<string>> ChangePassword(ChangePassword cGVManager, string url)
        {
            LoginViewModel login = new LoginViewModel();
            login.Email = cGVManager.Email;
            login.Password = cGVManager.CurrentPassword;
            var isLogin = await SignInAsync(login, StaticDetails.AccountAPIPath + "Login");
            if (isLogin == null )
            {
                return new ResponseResult<string>("Current Password is incorrect !");
            }           
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(cGVManager), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                 return new ResponseResult<string>();
                
            }
            return new ResponseResult<string>("Error changed password !");
        }

        public async Task<LoginViewModel> SignInAsync(LoginViewModel cGVManager, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(cGVManager), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<LoginViewModel>(jsonString);
            }
            return null;
        }

        public async Task<bool> SingOutAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<string> UpdateInforAsync(UpdateAccountViewModel cGVManager, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(cGVManager), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "Success";
            }
            var jsonString = await respone.Content.ReadAsStringAsync();
            var errorMessages = JsonConvert.DeserializeObject<Error>(jsonString);
            return errorMessages.Message;
        }
    }
}

﻿using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class CommentServices : ICommentServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public CommentServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<bool> ConfirmComment(int commentId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + commentId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }
    }
}

﻿using Newtonsoft.Json;
using TicketBookingOnline.ViewModels.Comments;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.ThongKes;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class ThongKeServices : IThongKeServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public ThongKeServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }
        
        public async Task<DashBoard> DashBoardAsync(int? cumRapId, string url)
        {
            if(cumRapId != null)
            {
                url = url + "?cumRapId=" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<DashBoard>(jsonString);
            }
            return null;
        }

        public async Task<DoanhThuTheoNam> DoanhThuNamAsync(int? cumRapId, int nam, string url)
        {
            url = url + "/" + nam;
            if (cumRapId != null)
            {
                url = url + "?&cumRapId" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<DoanhThuTheoNam>(jsonString);
            }
            return null;
        }

        public async Task<DoanhThuTheoNgay> DoanhThuNgayAsync(int? cumRapId, DateTime ngay, string url)
        {
            url = url + "/" + ngay.ToString("yyyy-MM-dd");
            if (cumRapId != null)
            {
                url = url + "?&cumRapId" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<DoanhThuTheoNgay>(jsonString);
            }
            return null;
        }

        public async Task<DoanhThuTheoThang> DoanhThuThangAsync(int? cumRapId, int thang, int nam, string url)
        {
            url = url + "/" + thang + "/" + nam;
            if (cumRapId != null)
            {
                url = url + "?&cumRapId" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<DoanhThuTheoThang>(jsonString);
            }
            return null;
        }

        public async Task<DoanhThuTheoTuan> DoanhThuTuanAsync(int? cumRapId, DateTime ngay, string url)
        {
            url = url + "/" + ngay.ToString("yyyy-MM-dd");
            if (cumRapId != null)
            {
                url = url + "?&cumRapId" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<DoanhThuTheoTuan>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<DoanhThuTheoNam>> GetAllDoanhThuNamAsync(int nam,  int? thanhPhoId, string url)
        {
            url = url + "/" + nam;
            if (thanhPhoId != null)
            {
                url = url + "?thanhPhoId=" + thanhPhoId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<DoanhThuTheoNam>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<DoanhThuTheoNgay>> GetAllDoanhThuNgayAsync(DateTime ngay, int? thanhPhoId, string url)
        {
            url = url + "/" + ngay.ToString("yyyy-MM-dd");
            if(thanhPhoId != null)
            {
                url = url + "?thanhPhoId=" + thanhPhoId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<DoanhThuTheoNgay>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<DoanhThuTheoNgay>> GetAllDoanhThuNgayOfThangInCumRap(int cumRapId, int thang, int nam, string url)
        {
            url = url + "/" + cumRapId + "/" + thang + "/" + nam;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<DoanhThuTheoNgay>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<DoanhThuTheoThang>> GetAllDoanhThuThangAsync(int thang, int nam, int? thanhPhoId, string url)
        {
            url = url + "/" + thang + "/" + nam;
            if (thanhPhoId != null)
            {
                url = url + "?thanhPhoId=" + thanhPhoId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<DoanhThuTheoThang>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<CommentViewModel>> GetBinhLuanMoiAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<CommentViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<KhachHangViewModel>> GetKhachHangMoiAsync(int? cumRapId, string url)
        {
            if (cumRapId != null)
            {
                url = url + "?cumRapId=" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<KhachHangViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<DoanhThuTheoThang>> ThongKeDoanhThuNamAsync(int? cumRapId, int nam, string url)
        {
            url = url + "/" + nam;
            if (cumRapId != null)
            {
                url = url + "?&cumRapId" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<DoanhThuTheoThang>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<DoanhThuTheoTuan>> ThongKeDoanhThuThangAsync(int? cumRapId, DateTime ngay, string url)
        {
            url = url + "/" + ngay.ToString("yyyy-MM-dd");
            if (cumRapId != null)
            {
                url = url + "?&cumRapId" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<DoanhThuTheoTuan>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<DoanhThuTheoNgay>> ThongKeDoanhThuTuanAsync(int? cumRapId, DateTime ngay, string url)
        {
            url = url + "/" + ngay.ToString("yyyy-MM-dd");
            if (cumRapId != null)
            {
                url = url + "?cumRapId=" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<DoanhThuTheoNgay>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<TrangThaiVe>> ThongKeTrangThaiVeAsync(int? cumRapId, DateTime ngay, string url)
        {
            url = url + "/" + ngay.ToString("yyyy-MM-dd");
            if (cumRapId != null)
            {
                url = url + "?cumRapId=" + cumRapId;
            }
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TrangThaiVe>>(jsonString);
            }
            return null;
        }
    }
}

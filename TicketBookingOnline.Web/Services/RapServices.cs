﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.Raps;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class RapServices : IRapServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public RapServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<bool> CreateAsync(RapViewModel rapVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(rapVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteAsync(int rapId, string url)
        {

            var request = new HttpRequestMessage(HttpMethod.Delete, url + "/" + rapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<RapViewModel>> GetRapByCumRapIdAsync(int cumRapId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + cumRapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<RapViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<RapViewModel> GetRapByIdAsync(int rapId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + rapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<RapViewModel>(jsonString);
            }
            return null;
        }

        public async Task<bool> UpdateAsync(RapViewModel rapVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(rapVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }
    }
}

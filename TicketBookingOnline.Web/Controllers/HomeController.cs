﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

using TicketBookingOnline.Web.IServices;
using TicketBookingOnline.Web.Models;
using System.Linq;

namespace TicketBookingOnline.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPhimServices _phimServices;

        private readonly ICumRapServices _cumRapServices;
        private readonly ILichChieuServices _lichChieuServices;
        private readonly ISliderServices _sliderServices;

        public HomeController(IPhimServices phimServices, ICumRapServices cumRapServices,
            ILogger<HomeController> logger, ILichChieuServices lichChieuServices, ISliderServices sliderServices)
        {
            _logger = logger;
            _phimServices = phimServices;
            _cumRapServices = cumRapServices;
            _lichChieuServices = lichChieuServices;
            _sliderServices = sliderServices;
        }

        public async Task<IActionResult> Index(string trangThai = "DangChieu", int index = 1)
        {
            ViewBag.Page = index;
            ViewBag.trangThai = trangThai;
            TempData["trangThai"] = trangThai;
            var phimByRowperPage = await _phimServices.GetPhimByTrangThaiWithRowperPage(index, trangThai, StaticDetails.PhimAPIPath + "GetPhimByTrangThaiWithRowperPage");
            var count = await _phimServices.CountPhimByTrangThaiWithRowperPage(trangThai, StaticDetails.PhimAPIPath + "CountPhimByTrangThai");
            ViewBag.MaxPage = (count / 8) - (count % 8 == 0 ? 1 : 0);

            if (count == 0)
            {
                TempData["ErrorMessages"] = "Không có kết quả nào!!!";
            }
            var sliders = await _sliderServices.GetAllAsync(StaticDetails.SliderAPIPath + "GetAll");
            ViewBag.Slider = sliders.ToList();
            return View(phimByRowperPage);
        }

       
        public async Task<IActionResult> TheLoaiPhim(int maTheLoai, string theLoais, int index = 1)
        {           
            ViewBag.Page = index;
            ViewBag.maTheLoai = maTheLoai;
            TempData["maTheLoai"] = maTheLoai;
            if (theLoais != null)
            {
                string[] theLoaiIds = theLoais.Split(";");
                var listTheLoai = new List<int>();
                for (int i = 0; i< theLoaiIds.Count()-1; i++)
                {
                    listTheLoai.Add(int.Parse(theLoaiIds[i]));
                }
                TempData["listTheLoai"] = listTheLoai;
                TempData["StringTheLoai"] = theLoais;
                var phims = await _phimServices.GetPhimByListTheLoaiWithRowPerPageAsync(listTheLoai,index, 9, StaticDetails.PhimAPIPath + "GetPhimByTheLoaisWithRowPerPage");
                var soluong = await _phimServices.CountPhimByTheLoaisAsync(listTheLoai, StaticDetails.PhimAPIPath + "CountPhimByTheLoais");
                ViewBag.MaxPage = (soluong / 9) - (soluong % 9 == 0 ? 1 : 0);

                if (soluong == 0)
                {
                    TempData["ErrorMessages"] = "Không có kết quả nào!!!";
                }
                return View(phims);
            }
            var phimByRowperPage = await _phimServices.GetPhimByTheLoaiRowPerPage(maTheLoai, index, StaticDetails.PhimAPIPath + "GetPhimByTheLoaiRowPerPage");
            var count = await _phimServices.CountPhimByTheLoai(maTheLoai, StaticDetails.PhimAPIPath + "CountPhimByTheLoai");


            ViewBag.MaxPage = (count / 9) - (count % 9 == 0 ? 1 : 0);

            if (count == 0)
            {
                TempData["ErrorMessages"] = "Không có kết quả nào!!!";
            }
            return View(phimByRowperPage);
        }

        public async Task<IActionResult> GetCumRapByThanhPhos(int thanhPhoId)
        {
            var cumRaps = await _cumRapServices.GetAllAsync(null, thanhPhoId, StaticDetails.CumRapAPIPath +
                "GetAll");
            return Json(cumRaps);
        }

        public async Task<IActionResult> GetLichChieuInCumRap(int cumRapId)
        {
            var xuatChieus = await _lichChieuServices.GetLichChieuInCumRapAsync(
                cumRapId, StaticDetails.LichChieuAPIPath + "GetLichChieuInCumRap");
            return Json(xuatChieus);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
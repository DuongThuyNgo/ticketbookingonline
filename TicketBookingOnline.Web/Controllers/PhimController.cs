﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Controllers
{
    public class PhimController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPhimServices _phimServices;

        private readonly ICumRapServices _cumRapServices;
        private readonly ILichChieuServices _lichChieuServices;
        public PhimController(IPhimServices phimServices, ICumRapServices cumRapServices,
            ILogger<HomeController> logger, ILichChieuServices lichChieuServices)
        {
            _logger = logger;
            _phimServices = phimServices;
            _cumRapServices = cumRapServices;
            _lichChieuServices = lichChieuServices;

        }

        public async Task<IActionResult> ChiTietPhim(int phimId)
        {
            
            var phim = await _phimServices.GetPhimById(phimId,StaticDetails.PhimAPIPath + "GetPhimById");
            var phimLienQuan = await _phimServices.GetPhimLienQuanAsync(phimId, StaticDetails.PhimAPIPath + "PhimLienQuan");
            ViewBag.PhimLienQuan = phimLienQuan.ToList();
            return View(phim);
        }

        public async Task<IActionResult> Phim(int phimId)
        {
           
            var phim = await _phimServices.GetPhimById(phimId, StaticDetails.PhimAPIPath + "GetPhimByTrangThaiRowPerPage");

            return View(phim);
        }
    }
}

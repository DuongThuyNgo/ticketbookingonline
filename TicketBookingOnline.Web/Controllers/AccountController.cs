﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using TicketBookingOnline.Helper;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IKhachHangServices _khachHangServices;
        private readonly IVeServices _veServices;

        public AccountController(IKhachHangServices khachHangServices, IVeServices veServices)
        {
            _khachHangServices = khachHangServices;
            _veServices = veServices;
        }
        
        [HttpGet]
        public IActionResult DangKy()
        {
            //ViewBag.ErrorMesages = null;
            return View();
        }

        
        public async Task<IActionResult> DangKy(CreateKhachHangViewModel khachHang)
        {
            if (ModelState.IsValid)
            {
                var listError = new List<string>();
                if (!khachHang.Password.Equals(khachHang.ConfirmPassword))
                {
                    listError.Add("Xác nhận mật khẩu không chính xác !");
                }
                if (!khachHang.SoDienThoai.PhoneNumberValidate())
                {
                    listError.Add("Số điện thoại là 10 chữ số !");
                }
                if (listError.Count() > 0)
                {
                    TempData["ErrorMessages"] = listError;
                    return View(khachHang);
                }
                var response = await _khachHangServices.DangKyAsync(khachHang, StaticDetails.KhachHangAPIPath + "Create");
                if (response != "Success")
                {
                    listError.Add(response);
                    TempData["ErrorMessages"] = listError;
                    return View(khachHang);                   
                }
                TempData["Success"] = "Đăng ký tài khoản thành công !";
                return RedirectToAction("DangNhap");        
            }
            return View(khachHang);
        }

        [HttpGet]
        public IActionResult DangNhap()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> DangNhap(KhachHangLoginViewModel khachHang)
        {
            var userLogin = await _khachHangServices.Login(khachHang, StaticDetails.KhachHangAPIPath + "Login");
            if(userLogin != null)
            {
                // Claim identity
                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                identity.AddClaim(new Claim(ClaimTypes.Name, userLogin.HoTen));
                
                var principal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                //HttpContext.Session.SetString("JWToken", atm.Token);
                HttpContext.Session.SetString("KhachHangId", userLogin.UserId.ToString());
                HttpContext.Session.SetString("HoTen", userLogin.HoTen);
                HttpContext.Session.SetString("Email", userLogin.Email);
                //HttpContext.Session.SetString("Role", userLogin.RoleName);
                return RedirectToAction("Index", "Home");
            }
            TempData["ErrorMessages"] = "Email hoặc mật khẩu không chính xác !";
            return View(khachHang);
        }

        [HttpGet]
        public async Task<IActionResult> TaiKhoan()
        {
            int khachHangId = int.Parse(HttpContext.Session.GetString("KhachHangId"));
            var khachHang = await _khachHangServices.GetByIdAsync(khachHangId, StaticDetails.KhachHangAPIPath + "GetById");
            return View(khachHang);
        }

        [HttpPost]
        public async Task<IActionResult> TaiKhoan(KhachHangViewModel khachHang)
        {
            var response = await _khachHangServices.UpdateAsync(khachHang, StaticDetails.KhachHangAPIPath + "Update");
            if (response == "Success")
            {
                TempData["Success"] = "Cập nhật thông tin tài khoản thành công !";
                return View(khachHang);
            }
            TempData["ErrorMesages"] = response;
            return View(khachHang);
        }

        [HttpGet]
        public async Task<IActionResult> DoiMatKhau()
        {
            ViewBag.Email = HttpContext.Session.GetString("Email");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> DoiMatKhau(ChangePassword user)
        {
            user.Email = HttpContext.Session.GetString("Email");
            var response = await _khachHangServices.ChangePassword(user, StaticDetails.KhachHangAPIPath + "ChangePassword");
            if (response == "Success")
            {
                TempData["Success"] = "Cập nhật mật khẩu thành công !";
                return RedirectToAction("Index", "Home");
            }
            TempData["ErrorMessages"] = response;
            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> LichSuGiaoDich()
        {
            int khachHangId = int.Parse(HttpContext.Session.GetString("KhachHangId"));
            var khachHang = await _khachHangServices.GetByIdAsync(khachHangId, StaticDetails.KhachHangAPIPath + "GetById");
            ViewBag.KhachHang = khachHang;
            var transactions = await _veServices.GetVeByKhachHangIdAsync(khachHangId, StaticDetails.VeAPIPath + "GetVeByKhachHangId");
            return View(transactions);
        }

        public async Task<IActionResult> ChiTietVe(int veId)
        {
            var ve = await _veServices.GetByIdAsync(veId, StaticDetails.VeAPIPath + "GetById");
            return View(ve);
        }

        public async Task<IActionResult> DangXuat()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

    }
}

using Microsoft.AspNetCore.Authentication.Cookies;
using TicketBookingOnline.Web.IServices;
using TicketBookingOnline.Web.Services;

var builder = WebApplication.CreateBuilder(args);

//var connectionString = builder.Configuration.GetConnectionString("TicketBooking");
//builder.Services.AddDbContext<TicketBookingOnlineDBContext>(options => options.UseSqlServer(connectionString));
//builder.Services.AddDatabaseDeveloperPageExceptionFilter();

// Add services to the container.
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Cookie.HttpOnly = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                    options.LoginPath = "/Account/Login";
                    // options.AccessDeniedPath = "/ATM/AccessDenied";
                    options.SlidingExpiration = true;
                });
// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddHttpClient();

//builder.Services.AddHostedService<QueueService>();
//builder.Services.AddSingleton<IBackgroundQueue, BackgroundQueue>();
//builder.Services.AddMvc();
//builder.Services.AddMvc(options =>
//{
//    options.AllowEmptyInputInBodyModelBinding = true;
//    foreach (var formatter in options.InputFormatters)
//    {
//        if (formatter.GetType() == typeof(SystemTextJsonInputFormatter))
//            ((SystemTextJsonInputFormatter)formatter).SupportedMediaTypes.Add(
//            Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse("text/plain"));
//    }
//}).AddJsonOptions(options =>
//{
//    options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
//});

builder.Services.AddScoped<IPhimServices, PhimServices>();
builder.Services.AddScoped<ITheLoaiServices, TheLoaiServices>();
builder.Services.AddScoped<IPhimTheLoaiServices, PhimTheLoaiServices>();
builder.Services.AddScoped<ICanhBaoServices, CanhBaoServices>();
builder.Services.AddScoped<ICGVManagerServices, CGVManagerServices>();
builder.Services.AddScoped<IAccountServices, AccountServices>();
builder.Services.AddScoped<ILichChieuServices, LichChieuServices>();
builder.Services.AddScoped<IVeServices, VeServices>();
builder.Services.AddScoped<ICumRapServices, CumRapServices>();
builder.Services.AddScoped<IRapServices, RapServices>();
builder.Services.AddScoped<IRapGheServices, RapGheServices>();
builder.Services.AddScoped<IKhachHangServices, KhachHangServices>();
builder.Services.AddScoped<IThongKeServices, ThongKeServices>();
builder.Services.AddScoped<ICommentServices, CommentServices>();
builder.Services.AddScoped<IThanhPhoServices, ThanhPhoServices>();
builder.Services.AddScoped<ISliderServices, SliderServices>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseSession();

app.UseAuthorization();


app.UseEndpoints(endpoints =>
{

    //endpoints.MapControllerRoute
    //           (name: "login",
    //            pattern: "/login",
    //            defaults: new { area = "Admin", controller = "Account", action = "Login" });
    //endpoints.MapControllerRoute
    //(
    //    name: "posts",
    //    pattern: "Post/{year}/{month}/{url}",
    //    defaults: new { controller = "Post", action = "Details" },
    //    constraints: new { year = @"\d{4}", month = @"\d{2}" }
    //);

    endpoints.MapControllerRoute
               (name: "DashBoard",
                pattern: "/DashBoard",
                defaults: new { area = "Base", controller = "Home", action = "Index" });


    endpoints.MapControllerRoute
               (name: "CGV",
                pattern: "/CGV/Home",
                defaults: new { area = "CGV", controller = "Home", action = "Index" });

    endpoints.MapControllerRoute
               (name: "phim",
                pattern: "/Admin/Phim",
                defaults: new { area = "Admin", controller = "Phim", action = "Index" });

    //endpoints.MapControllerRoute
    //            (name: "Denied",
    //             pattern: "/access-denied",
    //             defaults: new { area = "Admin", controller = "Account", action = "AccessDenied" });
    endpoints.MapControllerRoute(
      name: "areas",
      pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
    );
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}"

    );
});


//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

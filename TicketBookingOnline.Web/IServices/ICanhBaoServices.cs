﻿using TicketBookingOnline.ViewModels.CanhBaos;

namespace TicketBookingOnline.Web.IServices
{
    public interface ICanhBaoServices
    {
        Task<CanhBaoViewModel> GetByIdAsync(int canhBaoId, string url);
        Task<IEnumerable<CanhBaoViewModel>> GetAllAsync(string url);
        Task<bool> CreateAsync(CanhBaoViewModel canhBaoVM, string url);
        Task<bool> UpdateAsync(CanhBaoViewModel canhBaoVM, string url);
        Task<bool> DeleteAsync(int canhBaoId, string url);
    }
}

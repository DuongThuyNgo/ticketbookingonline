﻿using TicketBookingOnline.ViewModels.Ves;

namespace TicketBookingOnline.Web.IServices
{
    public interface IVeServices
    {
        Task<IEnumerable<VeViewModel>> GetAllIsActiveAsync(string url);
        Task<IEnumerable<VeViewModel>> GetVeByKhachHangIdAsync(int khachHangId, string url);
        Task<int> CountTotalVe(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, string url);
        Task<IEnumerable<VeViewModel>> Filter(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, string url);
        Task<IEnumerable<VeViewModel>> VeWithRowPerPage(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, string url, int index);
        Task<VeDetailViewModel> GetByIdAsync(int veId, string url);
        Task<bool> ConfirmVeAsync(int veId, string url);
        Task<bool> CancelVeAsync(DeleteVeViewModel veVM, string url);
    }
}

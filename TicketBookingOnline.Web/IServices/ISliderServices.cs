﻿using TicketBookingOnline.Core.Models;
using TicketBookingOnline.ViewModels.Sliders;

namespace TicketBookingOnline.Web.IServices
{
    public interface ISliderServices
    {
        Task<SliderViewModel> GetByIdAsync(int sliderId, string url);
        Task<IEnumerable<SliderViewModel>> GetAllAsync(string url);
        Task<bool> CreateAsync(SliderViewModel slider, string url);
        Task<bool> UpdateAsync(SliderViewModel slider, string url);
        Task<bool> DeleteAsync(int sliderId, string url);
    }
}

﻿using TicketBookingOnline.ViewModels.Phims;

namespace TicketBookingOnline.Web.IServices
{
    public interface IPhimServices
    {
        Task<PhimDetailViewModel> GetPhimById(int phimId, string url);
        Task<CreatePhimViewModel> GetPhimUpdate(int phimId, string url);
        Task<IEnumerable<PhimViewModel>> GetPhimHotAsync(string url);
        Task<IEnumerable<PhimViewModel>> GetPhimLienQuanAsync(int phimId, string url);
        Task<IEnumerable<PhimViewModel>> GetAllAsync(string url);
        Task<IEnumerable<PhimViewModel>> GetAllPhimIsActiveAsync(string url);
        Task<int> CountAllPhimIsActiveAsync(string url);
        Task<IEnumerable<PhimViewModel>> GetAllPhimIsActiveWithRowperPageAsync(int index, string url);
        Task<IEnumerable<PhimViewModel>> GetPhimByTen(string tenPhim, string url);
        Task<int> CountPhimByTen(string tenPhim, string url);
        Task<IEnumerable<PhimViewModel>> GetPhimByTenWithRowperPage(int index, string tenPhim, string url);
        Task<int> CountPhimByTrangThaiWithRowperPage(string trangThai, string url);
        Task<IEnumerable<PhimViewModel>> GetPhimByTrangThaiWithRowperPage(int index, string trangThai, string url);
        Task<IEnumerable<PhimViewModel>> GetPhimByTheLoaiRowPerPage(int maTheLoai, int index, string url);
        Task<int> CountPhimByTheLoai(int maTheLoai, string url);
        Task<IEnumerable<PhimViewModel>> GetPhimByListTheLoai(List<int> theloais, string url);
        Task<int> CountPhimByTheLoaisAsync(List<int> maTheLoai, string url);
        Task<IEnumerable<PhimViewModel>> GetPhimByListTheLoaiWithRowPerPageAsync(List<int> theloais, int index, int rowPerPage, string url);
        Task<CreatePhimViewModel> CreateAsync(CreatePhimViewModel phimVM, string url);
        Task<CreatePhimViewModel> UpdateAsync(CreatePhimViewModel phimVM, string url);
        Task<bool> DeleteAsync(int phimId, string url);
        Task<bool> UpdateTrangThaiPhimAsync(int phimId, string trangThai, string url);
    }
}

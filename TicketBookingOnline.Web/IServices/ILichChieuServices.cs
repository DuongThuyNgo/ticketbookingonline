﻿using TicketBookingOnline.ViewModels.LichChieus;

namespace TicketBookingOnline.Web.IServices
{
    public interface ILichChieuServices
    {
        Task<bool> CreateAsync(CreateLichChieuViewModel lichChieuVM, string url);
        Task<bool> DeleteAsync(int lichChieuId, string url);
        Task<IEnumerable<LichChieuViewModel>> GetLichChieuOfPhimInCumRapAsync(int phimId, int cumRapId, string url);
        Task<IEnumerable<LichChieuViewModel>> GetLichChieuInCumRapAsync(int cumRapId, string url);
        Task<IEnumerable<LichChieuViewModel>> FilterAsync(int phimId, int ? cumRapId, int ? rapId, 
            DateTime ? ngayChieu, string url);

    }
}

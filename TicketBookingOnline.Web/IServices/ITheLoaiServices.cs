﻿using TicketBookingOnline.ViewModels.TheLoais;

namespace TicketBookingOnline.Web.IServices
{
    public interface ITheLoaiServices
    {
        Task<List<TheLoaiViewModel>> GetAllAsync(string url);
        Task<TheLoaiViewModel> GetByIdAsync(int theLoaiId, string url);
        Task<bool> CreateAsync(TheLoaiViewModel theLoai, string url);
        Task<bool> UpdateAsync(TheLoaiViewModel theLoai, string url);
        Task<bool> DeleteAsync(int theLoaiId, string url);
    }
}

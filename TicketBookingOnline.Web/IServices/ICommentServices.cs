﻿namespace TicketBookingOnline.Web.IServices
{
    public interface ICommentServices
    {
        Task<bool> ConfirmComment(int commentId, string url);
    }
}

﻿using TicketBookingOnline.ViewModels.RapGhes;

namespace TicketBookingOnline.Web.IServices
{
    public interface IRapGheServices
    {
        Task<IEnumerable<RapGheViewModel>> GetRapGheByRapAsync(int RapId, string url);
        Task<bool> UpdateRange(List<RapGheViewModel> rapGhes, string url);
    }
}

﻿using TicketBookingOnline.ViewModels.ThanhPhos;

namespace TicketBookingOnline.Web.IServices
{
    public interface IThanhPhoServices
    {
        Task<IEnumerable<ThanhPhoViewModel>> GetAllAsync(string url);
    }
}

﻿using TicketBookingOnline.ViewModels.Raps;

namespace TicketBookingOnline.Web.IServices
{
    public interface IRapServices
    {
        Task<IEnumerable<RapViewModel>> GetRapByCumRapIdAsync(int cumRapId, string url);
        Task<RapViewModel> GetRapByIdAsync(int rapId, string url);
        Task<bool> CreateAsync(RapViewModel rapVM, string url);
        Task<bool> UpdateAsync(RapViewModel rapVM, string url);
        Task<bool> DeleteAsync(int rapId, string url);
    }
}

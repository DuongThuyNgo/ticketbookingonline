﻿using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.CGVManagers;

namespace TicketBookingOnline.Web.IServices
{
    public interface IAccountServices
    {
        Task<LoginViewModel> SignInAsync(LoginViewModel cGVManager, string url);
        Task<bool> SingOutAsync(string url);
        Task<string> UpdateInforAsync(UpdateAccountViewModel cGVManager, string url);
        Task<UpdateAccountViewModel> FindAccountAsync(string id, string url);
        Task<ResponseResult<string>> ChangePassword(ChangePassword cGVManager, string url);
    }
}

﻿using TicketBookingOnline.Core.Models;
using TicketBookingOnline.ViewModels.CGVManagers;

namespace TicketBookingOnline.Web.IServices
{
    public interface ICGVManagerServices
    {
        Task<string> CreateAsync(CreateCGVManagerViewModel cGVManagerVM, string url);
        Task<bool> UpdateAsync(CGVManagerDetailViewModel cGVManagerVM, string url);
        Task<bool> DeleteAsync(string id, string url);
        Task<int> CountAllAsync(string url);
        Task<IEnumerable<CGVManagerViewModel>> GetAllAsync(string url);
        Task<IEnumerable<CGVManagerViewModel>> GetWithRowPerPageAsync(int index,int thanhPhoId, int cumRapId, string url);
        Task<IEnumerable<CGVManagerViewModel>> GetNhanVienInCumRapWithRowPerPageAsync(int cumRapId, int index, string url);
        Task<int> CountAllNhanVienInCumRapAsync(int cumRapId, string url);
        Task<CGVManagerDetailViewModel> FindAsync(string id, string url);
        
    }
}
